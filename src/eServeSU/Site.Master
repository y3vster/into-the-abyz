﻿<%@ Master Language="C#" AutoEventWireup="true" CodeBehind="Site.Master.cs" Inherits="eServeSU.Site" %>

<!DOCTYPE html>

<html lang="en">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <title><%: Page.Title %> - eServe @ SU</title>

    <asp:PlaceHolder runat="server">
        <%: Styles.Render("~/Content/css") %>
        <%: Scripts.Render("~/bundles/modernizr") %>
        <%: @Scripts.Render("~/bundles/WebFormsJs") %>
        <%: @Scripts.Render("~/bundles/MsAjaxJs") %>
    </asp:PlaceHolder>


    <link rel="icon" type="image/png" href="~/Content/Images/SU_icon.png" />

</head>
<body>
    <form runat="server">
        <ajaxToolkit:ToolkitScriptManager runat="server">
            <Scripts>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="jquery" />
                <%--<asp:ScriptReference Name="jquery.ui.combined" />--%>
                <asp:ScriptReference Name="WebForms.js" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </ajaxToolkit:ToolkitScriptManager>




        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="/" class="navbar-brand">eServe</a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">

                        <% if (Context.User.IsInRole("Student"))
                            {%>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Student Menu<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="/Student/Profile">Profile</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="/StudentLegacy/StudentRegistered">My Opportunities</a></li>
                                <li><a href="/StudentLegacy/StudentRegistration">Sign Up</a></li>
                            </ul>
                        </li>
                        <%
                            }
                            if (Context.User.IsInRole("Partner"))
                            {
                        %>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Community Partner<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="/Partner/Organization">My Organization</a></li>
                                <li><a href="/Partner/Employee">Employee Profiles</a></li>
                                <li><a href="/Partner/Opportunity">My Opportunities</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="/Opportunity/OpportunityList">Opportunity List</a></li>
                                <li><a href="/CommunityPartnerContent/ReadEvaluation">Read Evaluation</a></li>
                                <li><a href="/CommunityPartnerContent/CommunityPartnerStudentView">Students</a></li>
                            </ul>
                        </li>
                        <%
                            }
                            if (Context.User.IsInRole("Faculty"))
                            {
                        %>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Faculty Menu<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="/Faculty/FacultyOpportunityListView">Opportunities</a></li>
                                <li><a href="/Faculty/FacultyCourseOpportunity">My Students</a></li>
                            </ul>
                        </li>

                        <%
                            }
                            if (Context.User.IsInRole("Admin"))
                            {
                        %>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Admin Menu<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="/AdminLegacy/AdminProfile">Admin Page</a></li>
                                <li><a href="/AdminLegacy/AdminCourseListReport">Report</a></li>
                            </ul>
                        </li>
                        <%}%>
                    </ul>

                    <!-- Login View -->

                    <asp:LoginView runat="server" ViewStateMode="Disabled" ID="lvLogin">
                        <AnonymousTemplate>
                            <ul class="nav navbar-nav navbar-right">
                                <li><a runat="server" href="~/Account/Login">Log in</a></li>
                            </ul>
                        </AnonymousTemplate>
                        <LoggedInTemplate>
                            <ul class="nav navbar-nav navbar-right">
                                <li><a runat="server" href="~/Account/Manage" title="Manage your account">Hello, <%: Context.User.Identity.GetUserName()  %>!</a></li>
                                <li>
                                    <asp:LoginStatus runat="server" LogoutAction="Redirect" LogoutText="Log off" LogoutPageUrl="~/" OnLoggingOut="Unnamed_LoggingOut" />
                                </li>
                            </ul>
                        </LoggedInTemplate>
                    </asp:LoginView>
                </div>
            </div>
        </div>
        <!-- Navbar -->


        <div class="container body-content">
            <asp:ContentPlaceHolder ID="MainContent" runat="server">
            </asp:ContentPlaceHolder>

            <hr />
            <footer>
                <p>&copy; <%: DateTime.Now.Year %> - eServe @ Seattleu</p>
            </footer>
        </div>
    </form>
    <%: @Scripts.Render("~/bundles/jquery") %>
    <%: @Scripts.Render("~/bundles/bootstrap") %>
</body>
</html>

