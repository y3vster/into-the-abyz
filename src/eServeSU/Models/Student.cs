namespace eServeSU.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Student
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Student()
        {
            Opportunity_Student_Records = new HashSet<Opportunity_Student_Records>();
            Opportunities = new HashSet<Opportunity>();
            Ethinicities = new HashSet<Ethinicity>();
            Opp_FocusAreas = new HashSet<Opp_FocusArea>();
            Opp_Types = new HashSet<Opp_Type>();
        }

        [Key]
        public string AspNetUser_ID { get; set; }

        [Required]
        [Column(TypeName = "date")]
        public DateTime? DateOfBirth { get; set; }

        [Required]
        [StringLength(15)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(15)]
        public string LastName { get; set; }

        [StringLength(10)]
        public string PreferedName { get; set; }

        [StringLength(25)]
        public string Gender { get; set; }

        public bool InternationalStudent { get; set; }

        [Column(TypeName = "date")]
        public DateTime? LastBackgroundCheck { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Opportunity_Student_Records> Opportunity_Student_Records { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Opportunity> Opportunities { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Ethinicity> Ethinicities { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Opp_FocusArea> Opp_FocusAreas { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Opp_Type> Opp_Types { get; set; }
    }
}
