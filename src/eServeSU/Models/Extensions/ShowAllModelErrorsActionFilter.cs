﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace eServeSU.Models
{
    public class ShowAllModelErrors : ActionFilterAttribute
    {
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            // Used for debugging obscure model errors
            // ----------------------------------------
            var modelState = filterContext.Controller.ViewData.ModelState;
            var validationErrors = from val in modelState.Values
                                   where val.Errors.Count > 0
                                   from ex in val.Errors
                                   select ex.ErrorMessage;
            var allErrMsgs = string.Join("\n", validationErrors);
            modelState.AddModelError("", allErrMsgs);

            base.OnActionExecuted(filterContext);
        }
    }
}