﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace System.Web.Mvc
{
    /*                                                                      *
     *      This extension was derived from Brad Christie's answer          *
     *      on StackOverflow.                                               *
     *                                                                      *
     *      The original code can be found at:                              *
     *      http://stackoverflow.com/a/18338264/998328                      *
     *                                                                      */

    public interface INotificationType
    {
        string Key { get; set; }
    }

    public class NotificationType : INotificationType
    {
        public string Key { get; set; }
        public static INotificationType ERROR = new NotificationType { Key = "Error" };
        public static INotificationType WARNING =  new NotificationType { Key = "Warning" };
        public static INotificationType SUCCESS = new NotificationType { Key = "Success" };
        public static INotificationType INFO = new NotificationType { Key = "Info" };
}

//public static class NotificationType
//{
//    public const string ERROR = "Error";
//    public const string WARNING = "Warning";
//    public const string SUCCESS = "Success";
//    public const string INFO = "Info";
//}

public static class NotificationExtensions
{
    private static IDictionary<String, String> NotificationKey = new Dictionary<String, String>
    {
            { "Error",      "App.Notifications.Error" },
            { "Warning",    "App.Notifications.Warning" },
            { "Success",    "App.Notifications.Success" },
            { "Info",       "App.Notifications.Info" }
        };

    public static void AddNotification(this ControllerBase controller, String message, INotificationType notificationType)
    {
        string NotificationKey = notificationType.Key;
        ICollection<String> messages = controller.TempData[NotificationKey] as ICollection<String>;

        if (messages == null)
        {
            controller.TempData[NotificationKey] = (messages = new HashSet<String>());
        }

        messages.Add(message);
    }

    public static IEnumerable<String> GetNotifications(this HtmlHelper htmlHelper, INotificationType notificationType)
    {
        string NotificationKey = notificationType.Key;
        return htmlHelper.ViewContext.Controller.TempData[NotificationKey] as ICollection<String> ?? null;
    }

    private static string getNotificationKeyByType(INotificationType notificationType)
    {
        try
        {
            return NotificationKey[notificationType.Key];
        }
        catch (IndexOutOfRangeException e)
        {
            ArgumentException exception = new ArgumentException("Key is invalid", "notificationType", e);
            throw exception;
        }
    }
}

}