namespace eServeSU.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Opportunity_Student_Records
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Opportunity_Student_Records()
        {
            Opportunity_Student_TimeEntries = new HashSet<Opportunity_Student_TimeEntries>();
        }

        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Opportunity_ID { get; set; }

        [Key]
        [Column(Order = 1)]
        public string Student_id { get; set; }

        [Required]
        [StringLength(10)]
        public string SignUpStatus { get; set; }

        public string StudentReflection { get; set; }

        public string PartnerEvaluation { get; set; }

        public string StudentEvaluation { get; set; }

        public virtual Opportunity Opportunity { get; set; }

        public virtual Student Student { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Opportunity_Student_TimeEntries> Opportunity_Student_TimeEntries { get; set; }
    }
}
