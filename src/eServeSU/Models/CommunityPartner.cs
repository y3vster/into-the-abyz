namespace eServeSU.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CommunityPartner
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CommunityPartner()
        {
            CommunityPartnerStaff = new HashSet<CommunityPartnerEmployee>();
            Opportunities = new HashSet<Opportunity>();
        }

        [Key]
        [MaxLength(128)]
        public string AspNetUser_ID { get; set; }

        [Required]
        [StringLength(150)]
        [Display(Name="Organization Name")]
        public string OrganizationName { get; set; }

        [Required]
        [StringLength(300)]
        [Display(Name="Address")]
        public string Address { get; set; }

        [StringLength(128)]
        [Display(Name="Website")]
        public string Website { get; set; }

        [Required]
        [StringLength(16)]
        [Display(Name = "Phone")]
        public string MainPhone { get; set; }

        [StringLength(50)]
        [Display(Name ="Mission Statement")]
        public string MissionStatement { get; set; }

        [StringLength(500)]
        [Display(Name ="Work Description")]
        public string WorkDescription { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CommunityPartnerEmployee> CommunityPartnerStaff { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Opportunity> Opportunities { get; set; }

        public virtual CommunityPartnerPendingApproval CommunityPartnerPendingApproval { get; set; }
    }
}
