namespace eServeSU.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Univ_CourseSections
    {
        public int ID { get; set; }

        public int Course_ID { get; set; }

        public int Professor_ID { get; set; }

        [Required]
        [StringLength(70)]
        public string RoomNumber { get; set; }

        [Required]
        [StringLength(20)]
        public string ClassHours { get; set; }

        public int NumberOfSlots { get; set; }

        [StringLength(50)]
        public string SectionName { get; set; }

        public virtual Univ_Courses Univ_Courses { get; set; }

        public virtual Univ_Professors Univ_Professors { get; set; }
    }
}
