namespace eServeSU.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Opportunity_Student_TimeEntries
    {
        [Key]
        [Column(Order = 0)]
        public int ID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Opportunity_ID { get; set; }

        [Key]
        [Column(Order = 2)]
        public string Student_ID { get; set; }

        [Column(TypeName = "date")]
        public DateTime Date { get; set; }

        public int? PartnerApprovedHours { get; set; }

        [Column(TypeName = "date")]
        public DateTime WorkDateEntry { get; set; }

        public int HoursVolunteered { get; set; }

        public virtual Opportunity_Student_Records Opportunity_Student_Records { get; set; }
    }
}
