namespace eServeSU.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Univ_Quarter
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Univ_Quarter()
        {
            Opportunities = new HashSet<Opportunity>();
            Univ_Courses = new HashSet<Univ_Courses>();
        }

        [Key]
        public int ID { get; set; }

        [Required]
        [StringLength(15)]
        [Display(Name = "Full Quarter Name")]
        public string QuarterName { get; set; }

        [Required]
        [StringLength(10)]
        [Display(Name = "Quarter Code")]
        [RegularExpression(@"^\d{2}[A-Z]{2}$", ErrorMessage = "2 uppercase chars followed by a two digit year code. e.g. FQ16")]
        public string ShortName { get; set; }

        [Column(TypeName = "date")]
        [DataType(DataType.Date)]
        [Display(Name = "Quarter Start Date")]
        public DateTime StartDate { get; set; }

        [Column(TypeName = "date")]
        [DataType(DataType.Date)]
        [Display(Name = "Quarter Finish Date")]
        public DateTime EndDate { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Opportunity> Opportunities { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Univ_Courses> Univ_Courses { get; set; }
    }
}
