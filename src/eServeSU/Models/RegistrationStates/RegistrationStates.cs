﻿using System;
using System.Diagnostics;
using eServeSU.Areas.Partner.Models;
using Microsoft.AspNet.Identity;

namespace eServeSU.Models.RegistrationStates
{
    // STATE: NEW
    // ============================================================
    public class RegistrationStateNew : RegistrationState
    {
        public RegistrationStateNew(ApplicationUserManager usrMgr, IeServeContext context, string userID) : base(usrMgr, context, userID) { }

        public override void StateNew() { throw new InvalidOperationException(); }

        public override void StatePending()
        {

            Log("***State transition: NEW => PENDING");

            // check to ensure that the user is not rejected
            var usrRoles = _usrMgr.GetRoles(_usrID);
            Debug.Assert(!usrRoles.Contains("PartnerRejected"));

            _usrMgr.AddToRole(_usrID, "PartnerPending");
            Log("Added role PartnerPending to " + _usrID);

            _context.CommunityPartnerPendingApprovals.Add(new CommunityPartnerPendingApproval { AspNetUser_ID = _usrID });
            _context.SaveChanges();
            Log("Added record to the table PendingApprovals: " + _usrID);

        }

        public override void StateRejected() { throw new InvalidOperationException(); }
        public override void StateApproved() { throw new InvalidOperationException(); }

    }
    // STATE: PENDING
    // ============================================================
    public class RegistrationStatePending : RegistrationState
    {
        public RegistrationStatePending(ApplicationUserManager usrMgr, IeServeContext context, string userID)
            : base(usrMgr, context, userID)
        {
        }

        public override void StateNew() { throw new InvalidOperationException(); }
        public override void StatePending()
        {
            Log("***State transition: PENDING => PENDING");
        }

        public override void StateRejected()
        {

            Log("***State transition: PENDING => REJECTED");

            _usrMgr.RemoveFromRole(_usrID, "PartnerPending");
            Log("Removed role PartnerPending from " + _usrID);

            _usrMgr.AddToRole(_usrID, "PartnerRejected");
            Log("Added role PartnerRejected to " + _usrID);

            var pendingRecord = new CommunityPartnerPendingApproval { AspNetUser_ID = _usrID };
            _context.CommunityPartnerPendingApprovals.Attach(pendingRecord);
            _context.CommunityPartnerPendingApprovals.Remove(pendingRecord);
            _context.SaveChanges();
            Log("Removed record from PendingApprovals table: " + _usrID);

        }

        public override void StateApproved()

        {
            Log("***State transition: PENDING => APPROVED");
            
            _usrMgr.RemoveFromRole(_usrID, "PartnerPending");
            Log("Removed role PartnerPending from " + _usrID);

            _usrMgr.AddToRole(_usrID, "PartnerJustApproved");
            Log("Added role PartnerJustApproved to: " + _usrID);

            _usrMgr.AddToRole(_usrID, "PartnerCanAddOpportunities");
            Log("Added role PartnerCanAddOpportunities to " + _usrID);

            var pendingRecord = new CommunityPartnerPendingApproval { AspNetUser_ID = _usrID };
            _context.CommunityPartnerPendingApprovals.Attach(pendingRecord);
            _context.CommunityPartnerPendingApprovals.Remove(pendingRecord);
            _context.SaveChanges();
            Log("Removed record from PendingApprovals table: " + _usrID);
        }

    }
    // STATE: APPROVED
    // ============================================================
    public class RegistrationStateApproved : RegistrationState
    {
        public RegistrationStateApproved(ApplicationUserManager usrMgr, IeServeContext context, string userID) : base(usrMgr, context, userID) { }

        public override void StateNew() { throw new InvalidOperationException(); }

        public override void StatePending()
        {
            Log("***State transition: APPROVED => APPROVED");
            Log("No action was taken");
        }
        public override void StateRejected() { throw new InvalidOperationException(); }

        public override void StateApproved()
        {
            Log("***State transition: (JUST)APPROVED => APPROVED");
            _usrMgr.RemoveFromRole(_usrID, "PartnerJustApproved");
            Log("Removed role PartnerJustApproved from " + _usrID);
        }

    }

    // STATE: REJECTED
    // ============================================================
    public class RegistrationStateRejected : RegistrationState
    {
        public RegistrationStateRejected(ApplicationUserManager usrMgr, IeServeContext context, string userID) : base(usrMgr, context, userID) { }

        public override void StateNew() { throw new InvalidOperationException(); }

        public override void StatePending()
        {
            Log("***State transition: REJECTED => PENDING");
            var usrRoles = _usrMgr.GetRoles(_usrID);

            _usrMgr.AddToRole(_usrID, "PartnerPending");
            Log("Added role PartnerPending to " + _usrID);


            if (usrRoles.Contains("PartnerRejected"))
            {
                _usrMgr.RemoveFromRole(_usrID, "PartnerRejected");
                Log("Partner was previously rejected, removed that role");
            }

            _context.CommunityPartnerPendingApprovals.Add(new CommunityPartnerPendingApproval { AspNetUser_ID = _usrID });
            _context.SaveChanges();
            Log("Added record to the table PendingApprovals: " + _usrID);

        }

        public override void StateRejected() { throw new InvalidOperationException(); }
        public override void StateApproved() { throw new InvalidOperationException(); }

    }
}