﻿using Microsoft.AspNet.Identity;

namespace eServeSU.Models.RegistrationStates
{
    public class RegistrationStateFactory
    {

        public static RegistrationState GetRegistrationState(ApplicationUserManager usrMgr, IeServeContext context, string usrID)
        {
            var roles = usrMgr.GetRoles(usrID);

            if (roles.Contains("PartnerRejected"))
                return new RegistrationStateRejected(usrMgr, context, usrID);

            if (roles.Contains("PartnerPending"))
                return new RegistrationStatePending(usrMgr, context, usrID);

            if (roles.Contains("PartnerCanAddOpportunities"))
                return new RegistrationStateApproved(usrMgr, context, usrID);

            return new RegistrationStateNew(usrMgr, context, usrID);
        }
    }
}