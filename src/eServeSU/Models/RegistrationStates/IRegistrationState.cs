﻿namespace eServeSU.Models.RegistrationStates
{
    interface IRegistrationState
    {
        void StateNew();
        void StatePending();
        void StateRejected();
        void StateApproved();
    }
}
