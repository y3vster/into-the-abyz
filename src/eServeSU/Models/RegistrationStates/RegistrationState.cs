﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace eServeSU.Models.RegistrationStates
{
    public abstract class RegistrationState
    {
        protected ApplicationUserManager _usrMgr;
        protected IeServeContext _context;
        protected string _usrID;
        public List<string> TraceInfo = new List<string>();

        protected RegistrationState(ApplicationUserManager usrMgr, IeServeContext context, string usrID)
        {
            _usrMgr = usrMgr;
            _context = context;
            _usrID = usrID;
        }

        /// <summary>
        /// This method is used for tracing execution paths.
        /// </summary>
        /// <param name="msg">Message to log</param>
        protected void Log(string msg)
        {
            Debug.WriteLine(msg);
            //TraceInfo.Add(msg);
        }

        public abstract void StateNew();
        public abstract void StatePending();
        public abstract void StateRejected();
        public abstract void StateApproved();

    }
}