namespace eServeSU.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CommunityPartnerStaff")]
    public partial class CommunityPartnerEmployee
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CommunityPartnerEmployee()
        {
            Opportunities = new HashSet<Opportunity>();
        }

        [Key]
        public int ID { get; set; }

        [Required]
        [ForeignKey("CommunityPartner")]
        public string CommunityPartner_ID { get; set; }

        [Required]
        [StringLength(15)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(15)]
        public string LastName { get; set; }

        [Required]
        [StringLength(15)]
        public string Title { get; set; }

        [Required]
        [StringLength(100)]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [StringLength(13)]
        public string Phone { get; set; }

        public bool IsOrganizationAdmin { get; set; }

        //[Required]
        public virtual CommunityPartner CommunityPartner { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Opportunity> Opportunities { get; set; }

        [NotMapped] // do not store in the database
        [Display(Name = "Name")]
        public virtual string TitleFirstLastName 
            => $"{Title} {FirstName} {LastName}";
    }
}
