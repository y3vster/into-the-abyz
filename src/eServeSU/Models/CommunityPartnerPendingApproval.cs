namespace eServeSU.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CommunityPartnerPendingApproval
    {
        public CommunityPartnerPendingApproval()
        {            
        }

        //System.Web.Providers.Entities.Session["numberPendingProfiles"] = ...;
        // can a function be added here to get the number of pending partners?

        [Key]     
        [MaxLength(128)]
        public string AspNetUser_ID { get; set; }

        [ForeignKey("AspNetUser_ID")]
        public virtual CommunityPartner CommunityPartner { get; set; }
    }
}
