namespace eServeSU.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class eServeContext : DbContext, IeServeContext
    {
        public eServeContext()
            : base("name=eServeConnection")
        {
        }

        public eServeContext(string connectionString) : base(connectionString) { }

        public virtual DbSet<CommunityPartner> CommunityPartners { get; set; }

        public virtual DbSet<CommunityPartnerPendingApproval> CommunityPartnerPendingApprovals { get; set; }
        public virtual DbSet<CommunityPartnerEmployee> CommunityPartnerStaff { get; set; }
        public virtual DbSet<Ethinicity> Ethinicities { get; set; }
        public virtual DbSet<Map_Opportunities_UnivCourseSections> Map_Opportunities_UnivCourseSections { get; set; }
        public virtual DbSet<Map_Students_UnivCourseSections> Map_Students_UnivCourseSections { get; set; }
        public virtual DbSet<Opp_FocusArea> Opp_FocusAreas { get; set; }
        public virtual DbSet<Opp_Type> Opp_Types { get; set; }
        public virtual DbSet<Opportunity> Opportunities { get; set; }
        public virtual DbSet<Opportunity_Student_Records> Opportunity_Student_Records { get; set; }
        public virtual DbSet<Opportunity_Student_TimeEntries> Opportunity_Student_TimeEntries { get; set; }
        public virtual DbSet<Student> Students { get; set; }
        public virtual DbSet<Univ_Courses> Univ_Courses { get; set; }
        public virtual DbSet<Univ_CourseSections> Univ_CourseSections { get; set; }
        public virtual DbSet<Univ_Professors> Univ_Professors { get; set; }
        public virtual DbSet<Univ_Quarter> Univ_Quarters { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("eserve");

            modelBuilder.Entity<CommunityPartner>()
                .HasMany(e => e.CommunityPartnerStaff)
                .WithRequired(e => e.CommunityPartner)
                .HasForeignKey(e => e.CommunityPartner_ID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CommunityPartner>()
                .HasOptional(e => e.CommunityPartnerPendingApproval)
                .WithRequired(e => e.CommunityPartner);

            modelBuilder.Entity<CommunityPartner>()
                .HasMany(e => e.Opportunities)
                .WithRequired(e => e.CommunityPartner)
                .HasForeignKey(e => e.CommunityPartner_ID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CommunityPartnerEmployee>()
                .HasMany(e => e.Opportunities)
                .WithRequired(e => e.CommunityPartnerStaff)
                .HasForeignKey(e => e.CommPartnerStaff_ID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Ethinicity>()
                .HasMany(e => e.Students)
                .WithMany(e => e.Ethinicities)
                .Map(m => m.ToTable("Map_Students_Ethinicities").MapRightKey("Student_ID"));

            modelBuilder.Entity<Opp_FocusArea>()
                .Property(e => e.AreaName)
                .IsUnicode(false);

            modelBuilder.Entity<Opp_FocusArea>()
                .HasMany(e => e.Opportunities)
                .WithMany(e => e.Opp_FocusAreas)
                .Map(m => m.ToTable("Map_Opportunities_FocusAreas").MapLeftKey("FocusArea_ID"));

            modelBuilder.Entity<Opp_FocusArea>()
                .HasMany(e => e.Students)
                .WithMany(e => e.Opp_FocusAreas)
                .Map(m => m.ToTable("Map_Students_OppFocusAreas").MapLeftKey("FocusArea_ID").MapRightKey("Student_ID"));

            modelBuilder.Entity<Opp_Type>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Opp_Type>()
                .HasMany(e => e.Opportunities)
                .WithRequired(e => e.Opp_Type)
                .HasForeignKey(e => e.Type_ID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Opp_Type>()
                .HasMany(e => e.Students)
                .WithMany(e => e.Opp_Types)
                .Map(m => m.ToTable("Map_Students_OppTypes").MapLeftKey("Opportunity_ID").MapRightKey("Student_id"));

            modelBuilder.Entity<Opportunity>()
                .Property(e => e.JobDescription)
                .IsUnicode(false);

            modelBuilder.Entity<Opportunity>()
                .Property(e => e.Requirements)
                .IsUnicode(false);

            modelBuilder.Entity<Opportunity>()
                .Property(e => e.TimeCommittment)
                .IsUnicode(false);

            modelBuilder.Entity<Opportunity>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<Opportunity>()
                .Property(e => e.LinkToOnlineApp)
                .IsUnicode(false);

            modelBuilder.Entity<Opportunity>()
                .HasMany(e => e.Opportunity_Student_Records)
                .WithRequired(e => e.Opportunity)
                .HasForeignKey(e => e.Opportunity_ID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Opportunity>()
                .HasMany(e => e.Students)
                .WithMany(e => e.Opportunities)
                .Map(m => m.ToTable("Map_Opportunities_Students").MapRightKey("Student_ID"));

            modelBuilder.Entity<Opportunity_Student_Records>()
                .HasMany(e => e.Opportunity_Student_TimeEntries)
                .WithRequired(e => e.Opportunity_Student_Records)
                .HasForeignKey(e => new { e.Opportunity_ID, e.Student_ID })
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Student>()
                .HasMany(e => e.Opportunity_Student_Records)
                .WithRequired(e => e.Student)
                .HasForeignKey(e => e.Student_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Univ_Courses>()
                .HasMany(e => e.Univ_CourseSections)
                .WithRequired(e => e.Univ_Courses)
                .HasForeignKey(e => e.Course_ID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Univ_CourseSections>()
                .Property(e => e.ClassHours)
                .IsUnicode(false);

            modelBuilder.Entity<Univ_CourseSections>()
                .Property(e => e.SectionName)
                .IsUnicode(false);

            modelBuilder.Entity<Univ_Professors>()
                .Property(e => e.FirstName)
                .IsUnicode(false);

            modelBuilder.Entity<Univ_Professors>()
                .Property(e => e.LastName)
                .IsUnicode(false);

            modelBuilder.Entity<Univ_Professors>()
                .Property(e => e.EmailID)
                .IsUnicode(false);

            modelBuilder.Entity<Univ_Professors>()
                .Property(e => e.Phone)
                .IsUnicode(false);

            modelBuilder.Entity<Univ_Professors>()
                .HasMany(e => e.Univ_CourseSections)
                .WithRequired(e => e.Univ_Professors)
                .HasForeignKey(e => e.Professor_ID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Univ_Quarter>()
                .HasMany(e => e.Opportunities)
                .WithRequired(e => e.Univ_Quarter)
                .HasForeignKey(e => e.Quarter_ID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Univ_Quarter>()
                .HasMany(e => e.Univ_Courses)
                .WithRequired(e => e.Univ_Quarter)
                .HasForeignKey(e => e.Quarter_ID)
                .WillCascadeOnDelete(false);
        }
    }
}
