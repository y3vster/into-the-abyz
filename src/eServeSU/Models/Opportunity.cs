using System.ComponentModel;

namespace eServeSU.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Opportunity
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Opportunity()
        {
            Opportunity_Student_Records = new HashSet<Opportunity_Student_Records>();
            Opp_FocusAreas = new HashSet<Opp_FocusArea>();
            Students = new HashSet<Student>();
            
            // default values when created
            DateOfCreation = DateTime.Now;
            Status = "Pending";
        }

        // used when doing a custom bind
        public Opportunity(string organizationID):this()
        {
            CommunityPartner_ID = organizationID;
        }


        public int ID { get; set; }

        [Required]
        [Display(Name = "Opportunity Type")]
        public int? Type_ID { get; set; }


        [Required]
        [StringLength(128)]
        [Display(Name = "Community Partner")]
        public string CommunityPartner_ID { get; set; }


        [Required]
        [Display(Name = "Opportunity Supervisor")]
        public int? CommPartnerStaff_ID { get; set; }


        [Column(TypeName = "date")]
        [Display(Name = "Date Created")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime DateOfCreation { get; set; }


        [Required]
        [Display(Name = "Academic Quarter")]
        public int Quarter_ID { get; set; }


        [Required]
        [StringLength(50)]
        [Display(Name = "Opportunity Name/Title")]
        public string Name { get; set; }


        [Required]
        [StringLength(50)]
        [Display(Name = "Location")]
        public string Location { get; set; }


        [Required]
        [DataType(DataType.MultilineText)]
        [StringLength(1000)]
        [Display(Name = "Opportunity Description")]
        public string JobDescription { get; set; }

        [Required]
        [DataType(DataType.MultilineText)]
        [StringLength(1000)]
        [Display(Name = "Requirements")]
        public string Requirements { get; set; }

        [StringLength(50)]
        [Display(Name = "Minimum Time Commitment")]
        public string TimeCommittment { get; set; }

        [Display(Name = "Number of Slots")]
        public int? TotalNumberOfSlots { get; set; }

        [Column(TypeName = "date")]
        [Display(Name = "Orientation Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? OrientationDate { get; set; }

        [Required]
        [Display(Name = "Status")]
        [StringLength(50)]
        public string Status { get; set; }

        [Display(Name = "Resume Required")]
        public bool ResumeRequired { get; set; }

        [Required]
        [DefaultValue(18)]
        [Range(12, 99)]
        [Display(Name = "Minimum Age")]
        public int? MinimumAge { get; set; }

        [Display(Name = "Criminal Record Check Required By Partner")]
        public bool? CRCRequiredByPartner { get; set; }

        [Display(Name = "Criminal Record Check Required by SU")]
        public bool? CRCRequiredBySU { get; set; }

        [StringLength(200)]
        [Display(Name = "Link to Online Application")]
        public string LinkToOnlineApp { get; set; }


        [Required]
        [Range(1, 1000)]
        [Display(Name = "Available # of Hours")]
        public int? JobHours { get; set; }

        [Required]
        [Range(0.01, 50.0)]
        [Display(Name = "Distance From SU (miles)")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:F1}")]
        public double? DistanceFromSU { get; set; }

        [Display(Name = "CommunityPartner_ID")]
        public virtual CommunityPartner CommunityPartner { get; set; }

        [Display(Name = "Supervisor")]
        public virtual CommunityPartnerEmployee CommunityPartnerStaff { get; set; }

        [Display(Name = "Opportunity Type")]
        public virtual Opp_Type Opp_Type { get; set; }

        [Display(Name = "Academic Quarter")]
        public virtual Univ_Quarter Univ_Quarter { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Opportunity_Student_Records> Opportunity_Student_Records { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        [Display(Name = "Opportunity Focus Areas")]
        public virtual ICollection<Opp_FocusArea> Opp_FocusAreas { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Student> Students { get; set; }
    }
}
