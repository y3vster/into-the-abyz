namespace eServeSU.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Univ_Courses
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Univ_Courses()
        {
            Univ_CourseSections = new HashSet<Univ_CourseSections>();
        }

        public int ID { get; set; }

        public int Quarter_ID { get; set; }

        public int ShortName { get; set; }

        [StringLength(50)]
        public string CourseName { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Univ_CourseSections> Univ_CourseSections { get; set; }

        public virtual Univ_Quarter Univ_Quarter { get; set; }
    }
}
