using System.Data.Entity.Infrastructure;
using System.Threading;
using System.Threading.Tasks;

namespace eServeSU.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public interface IeServeContext : IDisposable
    {
        DbSet<CommunityPartner> CommunityPartners { get; set; }

        DbSet<CommunityPartnerPendingApproval> CommunityPartnerPendingApprovals { get; set; }
        DbSet<CommunityPartnerEmployee> CommunityPartnerStaff { get; set; }
        DbSet<Ethinicity> Ethinicities { get; set; }
        DbSet<Map_Opportunities_UnivCourseSections> Map_Opportunities_UnivCourseSections { get; set; }
        DbSet<Map_Students_UnivCourseSections> Map_Students_UnivCourseSections { get; set; }
        DbSet<Opp_FocusArea> Opp_FocusAreas { get; set; }
        DbSet<Opp_Type> Opp_Types { get; set; }
        DbSet<Opportunity> Opportunities { get; set; }
        DbSet<Opportunity_Student_Records> Opportunity_Student_Records { get; set; }
        DbSet<Opportunity_Student_TimeEntries> Opportunity_Student_TimeEntries { get; set; }
        DbSet<Student> Students { get; set; }
        DbSet<Univ_Courses> Univ_Courses { get; set; }
        DbSet<Univ_CourseSections> Univ_CourseSections { get; set; }
        DbSet<Univ_Professors> Univ_Professors { get; set; }
        DbSet<Univ_Quarter> Univ_Quarters { get; set; }


        // functionality used
        int SaveChanges();
        Task<int> SaveChangesAsync();
        DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;
    }
}
