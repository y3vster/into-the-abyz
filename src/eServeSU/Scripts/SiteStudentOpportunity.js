﻿

// Signed up ----------------------------------------
// attach click handlers to table rows

var modal_loading_html = $('#signedup_loading_template').html();
var modal_error_html = $('#signedup_error_template').html();

$('#signedup_tbody tr').click(
    function () {
        var opp_id = $(this).find('td:first').text().trim();

        $.ajax({
            type: "GET",
            url: signedup_detail_url + "/" + opp_id,
            beforeSend: function () {
                $('#signedup_modal_content').empty().append(modal_loading_html);
                $('#signedup_modal_box').modal('show');
            },
        complete: function () {
            // complete
        },
        error: function () {
            $('#signedup_modal_content').empty().append(modal_error_html);
        },
        success: function (data) {
            $('#signedup_modal_content').empty().html(data);
        } // success fn
    }); // anon fn
}); // click


// Available ----------------------------------------
// attach click handlers to table rows

var modal_loading_html = $('#loading_template').html();
var modal_error_html = $('#error_template').html();

$('tbody tr').click(
    function () {
        var opp_id = $(this).find('td:first').text().trim();

        $.ajax({
            type: "GET",
            url: "@Url.Action("DetailsModal")/" + opp_id,
            beforeSend: function () {
                $('#modal_content').empty().append(modal_loading_html);
                $('#modal_box').modal('show');
            },
        complete: function () {
            // complete
        },
        error: function () {
            $('#modal_content').empty().append(modal_error_html);
        },
        success: function (data) {
            $('#modal_content').empty().html(data);
        } // success fn
    }); // anon fn
}); // click
