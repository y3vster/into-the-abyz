﻿// #create_modal    modal box
// data_rows        global variable with row json data

var j_create_modal = $('#create_modal');
var j_modal_form = $('#modal_form');
var j_modal_delete_btn = $('#empl_delete_btn');
var j_table_rows = $('#empl_table tbody');

// clears out the modal form
clear_modal_form = function () {
    j_modal_form.find('input[data-clear-item], textarea').val("");
}

// user clicks on the table row
set_row_click_events = function () {
    j_table_rows.find('tr').click(
    function () {
        clear_modal_form();
        var clicked_row = $(this).index();
        var row = data_rows[clicked_row];

        // fill out modal form elements
        for (var k in row) {
            if (row.hasOwnProperty(k)) {
                var form_element = j_modal_form.find('#' + k);
                if (form_element.length) {
                    form_element.val(row[k]);
                }
            }
        }

        j_create_modal.modal('show');
    });
}
;

// populate the table when the page first loads

refresh_empl_table = function () {
    html = Mustache.to_html($('#rows_template').html(), {
        rows: data_rows
    });
    $('#empl_table tbody').empty().append(html);
    set_row_click_events();
}


$(function () {
    refresh_empl_table();
});

// creating a new record
$(function () {
    j_modal_form.submit(function (event) {
        event.preventDefault();
        var form = $(this);
        if (!form.valid()) {
            return;
        }

        // form is valid at this point
        $.ajax({
            type: "POST",
            url: form.attr("action"),
            data: form.serialize(),
            beforeSend: function () {
                $("#create_form_error_well").hide();
                $("#create_submit_ajax_loader").show();
            },
            complete: function () {
                $("#create_submit_ajax_loader").hide();
            },
            error: function () {
                $("#create_form_error_well").show().text("There was an error trying to reach the server");
            },
            success: function (data) {
                if (data.record_inserted || data.record_updated) {
                    var added_alert;

                    if (data.record_inserted) {
                        added_alert = $("#alert_empl_added").children().clone();
                    } else {
                        added_alert = $("#alert_empl_changed").children().clone();
                    }

                    added_alert.id = "alert_" + (new Date().getTime());

                    $("#employee_alerts").append(added_alert);
                    added_alert.delay(2000).slideUp(200, function () {
                        $(this).alert('close');
                    });

                    $("#create_modal").modal('hide');
                    data_rows = data.data_rows;
                    refresh_empl_table();
                } else {
                    var html = Mustache.to_html(
                    $("#error_template").html(),
                    {
                        errors: data
                    });
                    $("#create_form_error_well").empty().append(html).show();
                }
            }
        });
    });
});

// deleting a new record
j_modal_delete_btn.click(function (event) {
    event.preventDefault();
    var link_element = $(this);

    // form is valid at this point
    $.ajax({
        type: "POST",
        url: link_element.attr("data-delete-link"),
        data: j_modal_form.serialize(),
        beforeSend: function () {
            $("#create_form_error_well").hide();
            $("#delete_submit_ajax_loader").show();
        },
        complete: function () {
            $("#delete_submit_ajax_loader").hide();
        },
        error: function () {
            $("#create_form_error_well").show().text("There was an error trying to reach the server");
        },
        success: function (data) {
            if (data.record_deleted) {
                var added_alert = $("#alert_empl_removed").children().clone();
                added_alert.id = "alert_" + (new Date().getTime());

                $("#employee_alerts").append(added_alert);
                added_alert.delay(2000).slideUp(200, function () {
                    $(this).alert('close');
                });
                j_create_modal.modal('hide');
                data_rows = data.data_rows;
                refresh_empl_table();
            } else {
                var html = Mustache.to_html(
                $("#error_template").html(),
                {
                    errors: data
                });
                $("#create_form_error_well").empty().append(html).show();
            }
        }
    });
});



// make the modal box validatable
$.validator.unobtrusive.parse("#create_empl_form");

// make popconfirm work
// Make sure that delete record event is attached first
$('#empl_delete').popConfirm();

