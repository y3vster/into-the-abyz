﻿$('body').on('show.bs.modal', '.modal', function () {
    $(this).html("<div class=\"modal-content\"> \
            <div class=\"modal-header\"> \
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button> \
            </div> \
            <div class=\"modal-body\"> \
                <img class=\"padded-content center-block\" src=\"~/Content/Images/ajax-loader.gif\"> \
            </div> \
            <div class=\"modal-footer\"> \
                <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button> \
            </div> \
        </div>") });