﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using eServeSU.Areas.Partner.Controllers;
using eServeSU.Models;
using eServeSU.Models.RegistrationStates;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace eServeSU.Controllers
{
    public class HomeController : Controller
    {
        private readonly eServeContext _db = new eServeContext();
        private string _usrID;
        private ApplicationUserManager _usrMgr;

        public string UserID
        {
            get
            {
                if (_usrID == null)
                {
                    _usrID = User.Identity.GetUserId();
                }
                return _usrID;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                if (_usrMgr == null)
                {
                    _usrMgr = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
                }
                return _usrMgr;
            }
        }

        // GET /home/index
        [ImportModelStateFromTempData]
        public ActionResult Index()
        {
            if (User.IsInRole("PartnerPending"))
            {
                this.AddNotification("Admin is in the process of approving your profile.", NotificationType.WARNING);
            }
            if (User.IsInRole("PartnerRejected"))
            {
                this.AddNotification("Admin has rejected your profile. Please update your profile and resubmit.",
                    NotificationType.ERROR);
            }
            if (User.IsInRole("PartnerJustApproved"))
            {
                this.AddNotification("Admin has approved your profile. You may now add opportunities.",
                    NotificationType.INFO);
                // Partners should only get this notification on the first login after Admin approval
                var currentState = RegistrationStateFactory.GetRegistrationState(UserManager, _db, UserID);
                currentState.StateApproved();
            }

            return View();
        }

        // GET /home/about
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        [HttpPost]
        public ActionResult Contact(string message)
        {
            ViewBag.TheMessage = "Thanks, we've got your message.";
            //TempData["Message"] = 
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.TheMessage = "Having trouble? Send us a message.";
            //TempData["Message"] = 
            return View();
        }
    }
}