using eServeSU.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;

namespace eServeSU.Migrations
{

    internal sealed class Configuration : DbMigrationsConfiguration<eServeSU.Models.eServeContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        //  This method will be called after migrating to the latest version.
        protected override void Seed(eServeSU.Models.eServeContext db)
        {
            var appContext = new ApplicationDbContext();
            var userStore = new UserStore<ApplicationUser>(appContext);
            var userManager = new UserManager<ApplicationUser>(userStore);

            // ROLES
            appContext.Roles.AddOrUpdate(r => r.Name, new IdentityRole { Name = "Admin" });
            appContext.Roles.AddOrUpdate(r => r.Name, new IdentityRole { Name = "Student" });
            appContext.Roles.AddOrUpdate(r => r.Name, new IdentityRole { Name = "Partner" });
            appContext.Roles.AddOrUpdate(r => r.Name, new IdentityRole { Name = "PartnerPending" });
            appContext.Roles.AddOrUpdate(r => r.Name, new IdentityRole { Name = "PartnerRejected" });
            appContext.Roles.AddOrUpdate(r => r.Name, new IdentityRole { Name = "PartnerJustApproved" });
            appContext.Roles.AddOrUpdate(r => r.Name, new IdentityRole { Name = "PartnerCanAddOpportunities" });
            appContext.Roles.AddOrUpdate(r => r.Name, new IdentityRole { Name = "Faculty" });
            appContext.Roles.AddOrUpdate(r => r.Name, new IdentityRole { Name = "Alumni" });
            appContext.SaveChanges();

            // ADMIN ACCOUNT
            if (!appContext.Users.Any(t => t.UserName == "admin"))
            {
                var user = new ApplicationUser { UserName = "admin" };

                userManager.PasswordValidator = new PasswordValidator {
                    RequiredLength = 0,
                    RequireNonLetterOrDigit = false,
                    RequireDigit = false,
                    RequireLowercase = false,
                    RequireUppercase = false,
                };

                userManager.Create(user);
                var result = userManager.AddPassword(user.Id, "password");

                userManager.AddToRole(user.Id, "Admin");
                appContext.SaveChanges();
            }

            db.Opp_FocusAreas.AddOrUpdate(
                x => x.AreaName,
                new Opp_FocusArea { AreaName = "Children and Youth" },
                new Opp_FocusArea { AreaName = "Intercultural Connections" },
                new Opp_FocusArea { AreaName = "Working with Families" },
                new Opp_FocusArea { AreaName = "Hunger and Homelessness" },
                new Opp_FocusArea { AreaName = "Sustainability" },
                new Opp_FocusArea { AreaName = "Aging and Disabilities" },
                new Opp_FocusArea { AreaName = "Seattle University Youth Initiative" });

            db.Opp_Types.AddOrUpdate(
                x => x.Name,
                new Opp_Type { Name = "Community Service" },
                new Opp_Type { Name = "Internship" },
                new Opp_Type { Name = "Other" });

            db.Ethinicities.AddOrUpdate(
                x => x.Description,
                new Ethinicity { Description = "American Indian or Alaska Native" },
                new Ethinicity { Description = "Asian" },
                new Ethinicity { Description = "Black or African American" },
                new Ethinicity { Description = "Native Hawaiian or Other Pacific Islander" },
                new Ethinicity { Description = "Hispanic or Latino" },
                new Ethinicity { Description = "White" });

            db.Univ_Quarters.AddOrUpdate(
                x => x.ShortName,
                new Univ_Quarter {
                    ShortName = "16WQ",
                    QuarterName = "Winter Quarter",
                    StartDate = new DateTime(2016, 1, 1),
                    EndDate = new DateTime(2016, 3, 10)
                },
                new Univ_Quarter {
                    ShortName = "16SQ",
                    QuarterName = "Spring Quarter",
                    StartDate = new DateTime(2016, 3, 20),
                    EndDate = new DateTime(2016, 6, 10)
                });

            db.SaveChanges();
        }
    }
}
