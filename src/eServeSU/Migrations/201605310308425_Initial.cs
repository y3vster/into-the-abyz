namespace eServeSU.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AspNetRoles",
                c => new {
                    Id = c.String(nullable: false, maxLength: 128),
                    Name = c.String(nullable: false, maxLength: 256),
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");

            CreateTable(
                "dbo.AspNetUserRoles",
                c => new {
                    UserId = c.String(nullable: false, maxLength: 128),
                    RoleId = c.String(nullable: false, maxLength: 128),
                })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);

            CreateTable(
                "dbo.AspNetUsers",
                c => new {
                    Id = c.String(nullable: false, maxLength: 128),
                    Email = c.String(maxLength: 256),
                    EmailConfirmed = c.Boolean(nullable: false),
                    PasswordHash = c.String(),
                    SecurityStamp = c.String(),
                    PhoneNumber = c.String(),
                    PhoneNumberConfirmed = c.Boolean(nullable: false),
                    TwoFactorEnabled = c.Boolean(nullable: false),
                    LockoutEndDateUtc = c.DateTime(),
                    LockoutEnabled = c.Boolean(nullable: false),
                    AccessFailedCount = c.Int(nullable: false),
                    UserName = c.String(nullable: false, maxLength: 256),
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");

            CreateTable(
                "dbo.AspNetUserClaims",
                c => new {
                    Id = c.Int(nullable: false, identity: true),
                    UserId = c.String(nullable: false, maxLength: 128),
                    ClaimType = c.String(),
                    ClaimValue = c.String(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);

            CreateTable(
                "dbo.AspNetUserLogins",
                c => new {
                    LoginProvider = c.String(nullable: false, maxLength: 128),
                    ProviderKey = c.String(nullable: false, maxLength: 128),
                    UserId = c.String(nullable: false, maxLength: 128),
                })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);

            CreateTable(
                "eserve.CommunityPartnerPendingApprovals",
                c => new
                    {
                        AspNetUser_ID = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.AspNetUser_ID)
                .ForeignKey("eserve.CommunityPartners", t => t.AspNetUser_ID)
                .Index(t => t.AspNetUser_ID);
            
            CreateTable(
                "eserve.CommunityPartners",
                c => new
                    {
                        AspNetUser_ID = c.String(nullable: false, maxLength: 128),
                        OrganizationName = c.String(nullable: false, maxLength: 150),
                        Address = c.String(nullable: false, maxLength: 300),
                        Website = c.String(maxLength: 128),
                        MainPhone = c.String(nullable: false, maxLength: 16),
                        MissionStatement = c.String(maxLength: 50),
                        WorkDescription = c.String(maxLength: 500),
                    })
                .PrimaryKey(t => t.AspNetUser_ID);
            
            CreateTable(
                "eserve.CommunityPartnerStaff",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CommunityPartner_ID = c.String(nullable: false, maxLength: 128),
                        FirstName = c.String(nullable: false, maxLength: 15),
                        LastName = c.String(nullable: false, maxLength: 15),
                        Title = c.String(nullable: false, maxLength: 15),
                        Email = c.String(nullable: false, maxLength: 100),
                        Phone = c.String(maxLength: 13),
                        IsOrganizationAdmin = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("eserve.CommunityPartners", t => t.CommunityPartner_ID)
                .Index(t => t.CommunityPartner_ID);
            
            CreateTable(
                "eserve.Opportunities",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Type_ID = c.Int(nullable: false),
                        CommunityPartner_ID = c.String(nullable: false, maxLength: 128),
                        CommPartnerStaff_ID = c.Int(nullable: false),
                        DateOfCreation = c.DateTime(nullable: false, storeType: "date"),
                        Quarter_ID = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 50),
                        Location = c.String(nullable: false, maxLength: 50),
                        JobDescription = c.String(nullable: false, maxLength: 1000, unicode: false),
                        Requirements = c.String(nullable: false, maxLength: 1000, unicode: false),
                        TimeCommittment = c.String(maxLength: 50, unicode: false),
                        TotalNumberOfSlots = c.Int(),
                        OrientationDate = c.DateTime(storeType: "date"),
                        Status = c.String(nullable: false, maxLength: 50, unicode: false),
                        ResumeRequired = c.Boolean(nullable: false),
                        MinimumAge = c.Int(nullable: false),
                        CRCRequiredByPartner = c.Boolean(),
                        CRCRequiredBySU = c.Boolean(),
                        LinkToOnlineApp = c.String(maxLength: 200, unicode: false),
                        JobHours = c.Int(nullable: false),
                        DistanceFromSU = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("eserve.Opp_Type", t => t.Type_ID)
                .ForeignKey("eserve.Univ_Quarter", t => t.Quarter_ID)
                .ForeignKey("eserve.CommunityPartnerStaff", t => t.CommPartnerStaff_ID)
                .ForeignKey("eserve.CommunityPartners", t => t.CommunityPartner_ID)
                .Index(t => t.Type_ID)
                .Index(t => t.CommunityPartner_ID)
                .Index(t => t.CommPartnerStaff_ID)
                .Index(t => t.Quarter_ID);
            
            CreateTable(
                "eserve.Opp_FocusArea",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        AreaName = c.String(nullable: false, maxLength: 200, unicode: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "eserve.Students",
                c => new
                    {
                        AspNetUser_ID = c.String(nullable: false, maxLength: 128),
                        DateOfBirth = c.DateTime(nullable: false, storeType: "date"),
                        FirstName = c.String(nullable: false, maxLength: 15),
                        LastName = c.String(nullable: false, maxLength: 15),
                        PreferedName = c.String(maxLength: 10),
                        Gender = c.String(maxLength: 25),
                        InternationalStudent = c.Boolean(nullable: false),
                        LastBackgroundCheck = c.DateTime(storeType: "date"),
                    })
                .PrimaryKey(t => t.AspNetUser_ID);
            
            CreateTable(
                "eserve.Ethinicities",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "eserve.Opp_Type",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50, unicode: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "eserve.Opportunity_Student_Records",
                c => new
                    {
                        Opportunity_ID = c.Int(nullable: false),
                        Student_id = c.String(nullable: false, maxLength: 128),
                        SignUpStatus = c.String(nullable: false, maxLength: 10),
                        StudentReflection = c.String(),
                        PartnerEvaluation = c.String(),
                        StudentEvaluation = c.String(),
                    })
                .PrimaryKey(t => new { t.Opportunity_ID, t.Student_id })
                .ForeignKey("eserve.Students", t => t.Student_id)
                .ForeignKey("eserve.Opportunities", t => t.Opportunity_ID)
                .Index(t => t.Opportunity_ID)
                .Index(t => t.Student_id);
            
            CreateTable(
                "eserve.Opportunity_Student_TimeEntries",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        Opportunity_ID = c.Int(nullable: false),
                        Student_ID = c.String(nullable: false, maxLength: 128),
                        Date = c.DateTime(nullable: false, storeType: "date"),
                        PartnerApprovedHours = c.Int(),
                        WorkDateEntry = c.DateTime(nullable: false, storeType: "date"),
                        HoursVolunteered = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ID, t.Opportunity_ID, t.Student_ID })
                .ForeignKey("eserve.Opportunity_Student_Records", t => new { t.Opportunity_ID, t.Student_ID })
                .Index(t => new { t.Opportunity_ID, t.Student_ID });
            
            CreateTable(
                "eserve.Univ_Quarter",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        QuarterName = c.String(nullable: false, maxLength: 15),
                        ShortName = c.String(nullable: false, maxLength: 10),
                        StartDate = c.DateTime(nullable: false, storeType: "date"),
                        EndDate = c.DateTime(nullable: false, storeType: "date"),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "eserve.Univ_Courses",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Quarter_ID = c.Int(nullable: false),
                        ShortName = c.Int(nullable: false),
                        CourseName = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("eserve.Univ_Quarter", t => t.Quarter_ID)
                .Index(t => t.Quarter_ID);
            
            CreateTable(
                "eserve.Univ_CourseSections",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Course_ID = c.Int(nullable: false),
                        Professor_ID = c.Int(nullable: false),
                        RoomNumber = c.String(nullable: false, maxLength: 70),
                        ClassHours = c.String(nullable: false, maxLength: 20, unicode: false),
                        NumberOfSlots = c.Int(nullable: false),
                        SectionName = c.String(maxLength: 50, unicode: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("eserve.Univ_Professors", t => t.Professor_ID)
                .ForeignKey("eserve.Univ_Courses", t => t.Course_ID)
                .Index(t => t.Course_ID)
                .Index(t => t.Professor_ID);
            
            CreateTable(
                "eserve.Univ_Professors",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false, maxLength: 15, unicode: false),
                        LastName = c.String(nullable: false, maxLength: 15, unicode: false),
                        EmailID = c.String(nullable: false, maxLength: 50, unicode: false),
                        Phone = c.String(nullable: false, maxLength: 12, unicode: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "eserve.Map_Opportunities_UnivCourseSections",
                c => new
                    {
                        OpportunityID = c.Int(nullable: false),
                        SectionID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.OpportunityID, t.SectionID });
            
            CreateTable(
                "eserve.Map_Students_UnivCourseSections",
                c => new
                    {
                        StudentID = c.Int(nullable: false),
                        SectionID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.StudentID, t.SectionID });
            
            CreateTable(
                "eserve.Map_Opportunities_FocusAreas",
                c => new
                    {
                        FocusArea_ID = c.Int(nullable: false),
                        Opportunity_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.FocusArea_ID, t.Opportunity_ID })
                .ForeignKey("eserve.Opp_FocusArea", t => t.FocusArea_ID, cascadeDelete: true)
                .ForeignKey("eserve.Opportunities", t => t.Opportunity_ID, cascadeDelete: true)
                .Index(t => t.FocusArea_ID)
                .Index(t => t.Opportunity_ID);
            
            CreateTable(
                "eserve.Map_Students_Ethinicities",
                c => new
                    {
                        Ethinicity_ID = c.Int(nullable: false),
                        Student_ID = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.Ethinicity_ID, t.Student_ID })
                .ForeignKey("eserve.Ethinicities", t => t.Ethinicity_ID, cascadeDelete: true)
                .ForeignKey("eserve.Students", t => t.Student_ID, cascadeDelete: true)
                .Index(t => t.Ethinicity_ID)
                .Index(t => t.Student_ID);
            
            CreateTable(
                "eserve.Map_Students_OppTypes",
                c => new
                    {
                        Opportunity_ID = c.Int(nullable: false),
                        Student_id = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.Opportunity_ID, t.Student_id })
                .ForeignKey("eserve.Opp_Type", t => t.Opportunity_ID, cascadeDelete: true)
                .ForeignKey("eserve.Students", t => t.Student_id, cascadeDelete: true)
                .Index(t => t.Opportunity_ID)
                .Index(t => t.Student_id);
            
            CreateTable(
                "eserve.Map_Students_OppFocusAreas",
                c => new
                    {
                        FocusArea_ID = c.Int(nullable: false),
                        Student_ID = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.FocusArea_ID, t.Student_ID })
                .ForeignKey("eserve.Opp_FocusArea", t => t.FocusArea_ID, cascadeDelete: true)
                .ForeignKey("eserve.Students", t => t.Student_ID, cascadeDelete: true)
                .Index(t => t.FocusArea_ID)
                .Index(t => t.Student_ID);
            
            CreateTable(
                "eserve.Map_Opportunities_Students",
                c => new
                    {
                        Opportunity_ID = c.Int(nullable: false),
                        Student_ID = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.Opportunity_ID, t.Student_ID })
                .ForeignKey("eserve.Opportunities", t => t.Opportunity_ID, cascadeDelete: true)
                .ForeignKey("eserve.Students", t => t.Student_ID, cascadeDelete: true)
                .Index(t => t.Opportunity_ID)
                .Index(t => t.Student_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
            DropForeignKey("eserve.Opportunities", "CommunityPartner_ID", "eserve.CommunityPartners");
            DropForeignKey("eserve.CommunityPartnerStaff", "CommunityPartner_ID", "eserve.CommunityPartners");
            DropForeignKey("eserve.Opportunities", "CommPartnerStaff_ID", "eserve.CommunityPartnerStaff");
            DropForeignKey("eserve.Univ_Courses", "Quarter_ID", "eserve.Univ_Quarter");
            DropForeignKey("eserve.Univ_CourseSections", "Course_ID", "eserve.Univ_Courses");
            DropForeignKey("eserve.Univ_CourseSections", "Professor_ID", "eserve.Univ_Professors");
            DropForeignKey("eserve.Opportunities", "Quarter_ID", "eserve.Univ_Quarter");
            DropForeignKey("eserve.Map_Opportunities_Students", "Student_ID", "eserve.Students");
            DropForeignKey("eserve.Map_Opportunities_Students", "Opportunity_ID", "eserve.Opportunities");
            DropForeignKey("eserve.Opportunity_Student_Records", "Opportunity_ID", "eserve.Opportunities");
            DropForeignKey("eserve.Map_Students_OppFocusAreas", "Student_ID", "eserve.Students");
            DropForeignKey("eserve.Map_Students_OppFocusAreas", "FocusArea_ID", "eserve.Opp_FocusArea");
            DropForeignKey("eserve.Opportunity_Student_Records", "Student_id", "eserve.Students");
            DropForeignKey("eserve.Opportunity_Student_TimeEntries", new[] { "Opportunity_ID", "Student_ID" }, "eserve.Opportunity_Student_Records");
            DropForeignKey("eserve.Map_Students_OppTypes", "Student_id", "eserve.Students");
            DropForeignKey("eserve.Map_Students_OppTypes", "Opportunity_ID", "eserve.Opp_Type");
            DropForeignKey("eserve.Opportunities", "Type_ID", "eserve.Opp_Type");
            DropForeignKey("eserve.Map_Students_Ethinicities", "Student_ID", "eserve.Students");
            DropForeignKey("eserve.Map_Students_Ethinicities", "Ethinicity_ID", "eserve.Ethinicities");
            DropForeignKey("eserve.Map_Opportunities_FocusAreas", "Opportunity_ID", "eserve.Opportunities");
            DropForeignKey("eserve.Map_Opportunities_FocusAreas", "FocusArea_ID", "eserve.Opp_FocusArea");
            DropForeignKey("eserve.CommunityPartnerPendingApprovals", "AspNetUser_ID", "eserve.CommunityPartners");
            DropIndex("eserve.Map_Opportunities_Students", new[] { "Student_ID" });
            DropIndex("eserve.Map_Opportunities_Students", new[] { "Opportunity_ID" });
            DropIndex("eserve.Map_Students_OppFocusAreas", new[] { "Student_ID" });
            DropIndex("eserve.Map_Students_OppFocusAreas", new[] { "FocusArea_ID" });
            DropIndex("eserve.Map_Students_OppTypes", new[] { "Student_id" });
            DropIndex("eserve.Map_Students_OppTypes", new[] { "Opportunity_ID" });
            DropIndex("eserve.Map_Students_Ethinicities", new[] { "Student_ID" });
            DropIndex("eserve.Map_Students_Ethinicities", new[] { "Ethinicity_ID" });
            DropIndex("eserve.Map_Opportunities_FocusAreas", new[] { "Opportunity_ID" });
            DropIndex("eserve.Map_Opportunities_FocusAreas", new[] { "FocusArea_ID" });
            DropIndex("eserve.Univ_CourseSections", new[] { "Professor_ID" });
            DropIndex("eserve.Univ_CourseSections", new[] { "Course_ID" });
            DropIndex("eserve.Univ_Courses", new[] { "Quarter_ID" });
            DropIndex("eserve.Opportunity_Student_TimeEntries", new[] { "Opportunity_ID", "Student_ID" });
            DropIndex("eserve.Opportunity_Student_Records", new[] { "Student_id" });
            DropIndex("eserve.Opportunity_Student_Records", new[] { "Opportunity_ID" });
            DropIndex("eserve.Opportunities", new[] { "Quarter_ID" });
            DropIndex("eserve.Opportunities", new[] { "CommPartnerStaff_ID" });
            DropIndex("eserve.Opportunities", new[] { "CommunityPartner_ID" });
            DropIndex("eserve.Opportunities", new[] { "Type_ID" });
            DropIndex("eserve.CommunityPartnerStaff", new[] { "CommunityPartner_ID" });
            DropIndex("eserve.CommunityPartnerPendingApprovals", new[] { "AspNetUser_ID" });
            DropTable("eserve.Map_Opportunities_Students");
            DropTable("eserve.Map_Students_OppFocusAreas");
            DropTable("eserve.Map_Students_OppTypes");
            DropTable("eserve.Map_Students_Ethinicities");
            DropTable("eserve.Map_Opportunities_FocusAreas");
            DropTable("eserve.Map_Students_UnivCourseSections");
            DropTable("eserve.Map_Opportunities_UnivCourseSections");
            DropTable("eserve.Univ_Professors");
            DropTable("eserve.Univ_CourseSections");
            DropTable("eserve.Univ_Courses");
            DropTable("eserve.Univ_Quarter");
            DropTable("eserve.Opportunity_Student_TimeEntries");
            DropTable("eserve.Opportunity_Student_Records");
            DropTable("eserve.Opp_Type");
            DropTable("eserve.Ethinicities");
            DropTable("eserve.Students");
            DropTable("eserve.Opp_FocusArea");
            DropTable("eserve.Opportunities");
            DropTable("eserve.CommunityPartnerStaff");
            DropTable("eserve.CommunityPartners");
            DropTable("eserve.CommunityPartnerPendingApprovals");
        }
    }
}
