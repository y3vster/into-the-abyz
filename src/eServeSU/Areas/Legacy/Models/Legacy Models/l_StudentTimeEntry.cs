﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Web;

namespace eServeSU
{
    /// <summary>
    /// Summary description for l_OpportunityRegistered
    /// </summary>
    public class l_StudentTimeEntry
    {
        public l_StudentTimeEntry()
        {
            //
            // TODO: Add constructor logic here
            //
            dbHelper = new DatabaseHelper();
            dbHelper.DbConnection = ConfigurationManager.ConnectionStrings["eServeConnection"].ConnectionString;
        }

        public string  WorkDate { get; set; }
        public int OpportunityID { get; set; }
        public int StudentID { get; set; }
        public int CPPID { get; set; }
        public int PartnerApprovedHours { get; set; }
        public string TimeEntryDate { get; set; }
        public int HoursVolunteered { get; set; }

        private DatabaseHelper dbHelper;

        public void SubmitStudentTimeEntry(l_StudentTimeEntry lStudentTimeEntry)
        {
            dbHelper.SubmitStudentTimeEntry(Constant.SP_AddTimeEntries, lStudentTimeEntry.WorkDate, lStudentTimeEntry.OpportunityID, lStudentTimeEntry.StudentID,
                                                        lStudentTimeEntry.CPPID, lStudentTimeEntry.PartnerApprovedHours, lStudentTimeEntry.TimeEntryDate, lStudentTimeEntry.HoursVolunteered);
        }

        public List<l_StudentTimeEntry> GetStudentTimeEntriesByOpportunityId(int studentId, int opportunityId)
        {
            var reader = dbHelper.GetStudentTimeEntriesByOpportunityID(Constant.SP_GetStudentTimeEntriesByOpportunityID, studentId, opportunityId);

            List<l_StudentTimeEntry> studentTimeEntryList = new List<l_StudentTimeEntry>();
            l_StudentTimeEntry lStudentTimeEntry = null;

            while (reader.Read())
            {
                lStudentTimeEntry = new l_StudentTimeEntry();

                lStudentTimeEntry.WorkDate = reader["WorkDate"].ToString();
                lStudentTimeEntry.HoursVolunteered = Convert.ToInt32(reader["HoursVolunteered"]);
                //l_StudentTimeEntry.PartnerApprovedHours = Convert.ToInt32(reader["PartnerApprovedHours"]);
                studentTimeEntryList.Add(lStudentTimeEntry);
            }

            return studentTimeEntryList;
        }

        public List<l_StudentTimeEntry> GetTimeEntries(int studentId, int opportunityId)
        {
            var reader = dbHelper.GetTimeEntries(Constant.SP_GetTimeEntries, studentId, opportunityId);

            List<l_StudentTimeEntry> studentTimeEntryList = new List<l_StudentTimeEntry>();
            l_StudentTimeEntry lStudentTimeEntry = null;

            while (reader.Read())
            {
                lStudentTimeEntry = new l_StudentTimeEntry();

                lStudentTimeEntry.WorkDate = reader["WorkDate"].ToString();
                lStudentTimeEntry.HoursVolunteered = Convert.ToInt32(reader["HoursVolunteered"]);
                lStudentTimeEntry.PartnerApprovedHours = Convert.ToInt32(reader["PartnerApprovedHours"]);
                studentTimeEntryList.Add(lStudentTimeEntry);
            }

            return studentTimeEntryList;
        }

        public void UpdatePartnerHours()
        {
            dbHelper.UpdateTimeEntries(Constant.SP_UpdateTimeEntries, this.OpportunityID, this.StudentID,Convert.ToDateTime (this.WorkDate),this.PartnerApprovedHours);
        }
    }
}
