﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Web;

namespace eServeSU
{
    /// <summary>
    /// Summary description for l_OpportunityRegistered
    /// </summary>
    public class l_OpportunityRegistration
    {
        public l_OpportunityRegistration()
        {
            //
            // TODO: Add constructor logic here
            //
            dbHelper = new DatabaseHelper();
            dbHelper.DbConnection = ConfigurationManager.ConnectionStrings["eServeConnection"].ConnectionString;
        }

        public int OpportunityID { get; set; }
        public string Position { get; set; }
        public string Organization { get; set; }
        public string Location { get; set; }
        public string SlotsAvailable { get; set; }
        public string DistanceFromSU { get; set; }
        public string MinimumAge { get; set; }
        public string CRCRequiredByPartner { get; set; }
        public string TimeCommittment { get; set; }
        public string JobDescription { get; set; }

        private DatabaseHelper dbHelper;

        public List<l_OpportunityRegistration> GetOpportunityListByStudentId(int studentId)
        {
            var reader = dbHelper.GetOpportunityRegistrationByStudentId(Constant.SP_GetOpportunityListByStudentId, studentId);

            List<l_OpportunityRegistration> regirationList = new List<l_OpportunityRegistration>();
            l_OpportunityRegistration lOpportunityRegistration = null;

            while (reader.Read())
            {
                lOpportunityRegistration = new l_OpportunityRegistration();

                lOpportunityRegistration.OpportunityID = Convert.ToInt32(reader["OpportunityID"]);
                lOpportunityRegistration.Position = reader["Position"].ToString();
                lOpportunityRegistration.Organization = reader["Organization"].ToString();
                lOpportunityRegistration.Location = reader["Location"].ToString();
                lOpportunityRegistration.SlotsAvailable = reader["SlotsAvailable"].ToString();
                lOpportunityRegistration.DistanceFromSU = reader["DistanceFromSU"].ToString();
                lOpportunityRegistration.MinimumAge = reader["MinimumAge"].ToString();
                lOpportunityRegistration.CRCRequiredByPartner = reader["CRCRequired"].ToString();
                lOpportunityRegistration.TimeCommittment = reader["TimeCommittment"].ToString();
                lOpportunityRegistration.JobDescription = reader["JobDescription"].ToString();

                regirationList.Add(lOpportunityRegistration);
            }

            return regirationList;
        }
    }
}
