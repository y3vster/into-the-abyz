﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Web;

namespace eServeSU
{
    /// <summary>
    /// Summary description for 
    /// </summary>
    public class l_StudentHours
    {
        public l_StudentHours()
        {
            dbHelper = new DatabaseHelper();
            dbHelper.DbConnection = ConfigurationManager.ConnectionStrings["eServeConnection"].ConnectionString;
        }

        private DatabaseHelper dbHelper;
        public string HoursVolunteered { get; set; }
        public string PartnerApprovedHours { get; set; }

        public l_StudentHours GetVolunteeredAndPartnerApprovedHours(int studentId, int opportunityID)
        {
            var reader = dbHelper.GetVolunteeredAndPartnerApprovedHours(Constant.SP_GetStudentVolunteeredPartnerApprovedHoursByOpportunityId, studentId, opportunityID);
            
            l_StudentHours lStudentHours = new l_StudentHours();
            reader.Read();
            lStudentHours.HoursVolunteered = reader["HoursVolunteered"].ToString();
            lStudentHours.PartnerApprovedHours = reader["PartnerApprovedHours"].ToString();

            return lStudentHours;
        }
    }
}
