﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Web;

namespace eServeSU
{
    /// <summary>
    /// Summary description for l_OpportunityRegistered
    /// </summary>
    public class l_OpportunityRegistered
    {
        public l_OpportunityRegistered()
        {
            //
            // TODO: Add constructor logic here
            //
            dbHelper = new DatabaseHelper();
            dbHelper.DbConnection = ConfigurationManager.ConnectionStrings["eServeConnection"].ConnectionString;
        }

        public int OpportunityID { get; set; }
        public int StudentId { get; set; }
        public string Opportunity { get; set; }
        public string Course { get; set; }
        public string Quarter { get; set; }
        public string Organization { get; set; }
        public string EmailId { get; set; }
        public string Status { get; set; }
        public string HoursVolunteered { get; set; }
        public string ParternEvaluation { get; set; }
        public string StudentEvaluation { get; set; }
        public string StudentReflection { get; set; }

        private DatabaseHelper dbHelper;

        public List<l_OpportunityRegistered> GetOpportunityRegisteredByStudentId(int studentId)
        {
            var reader = dbHelper.GetRegisteredOpportunityByStudentId(Constant.SP_GetOpportunityRegisteredByStudentId, studentId);

            List<l_OpportunityRegistered> registeredList = new List<l_OpportunityRegistered>();
            l_OpportunityRegistered lOpportunityRegistered = null;

            while (reader.Read())
            {
                lOpportunityRegistered = new l_OpportunityRegistered();

                lOpportunityRegistered.OpportunityID = Convert.ToInt32(reader["OpportunityID"]);
                lOpportunityRegistered.StudentId = Convert.ToInt32(reader["StudentId"]);
                lOpportunityRegistered.Opportunity = reader["l_Opportunity"].ToString();
                lOpportunityRegistered.Course = reader["CourseName"].ToString();
                lOpportunityRegistered.Quarter = reader["l_Quarter"].ToString();
                lOpportunityRegistered.Organization = reader["Organization"].ToString();
                lOpportunityRegistered.EmailId = reader["EmailId"].ToString();
                lOpportunityRegistered.Status = reader["Status"].ToString();
                lOpportunityRegistered.HoursVolunteered = reader["HoursVolunteered"].ToString();
                lOpportunityRegistered.ParternEvaluation = reader["PartnerEvaluation"].ToString();
                lOpportunityRegistered.StudentEvaluation = reader["StudentEvaluation"].ToString();
                lOpportunityRegistered.StudentReflection = reader["StudentReflection"].ToString();

                registeredList.Add(lOpportunityRegistered);
            }

            return registeredList;
        }

        public bool RegisterOpportunity(int studentId, int opportunityId)
        {
            bool registrationStatus = true;
            var reader = dbHelper.RegisteredOpportunityByStudentAndOpportunityId(Constant.SP_RegisterStudentOpportunity, studentId, opportunityId);


            return registrationStatus;
        }
    }
}
