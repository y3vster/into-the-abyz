﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Web;

namespace eServeSU
{
    public class l_OpportunityDetail
    {
        public l_OpportunityDetail()
        {
            //
            // TODO: Add constructor logic here
            //
            dbHelper = new DatabaseHelper();
            dbHelper.DbConnection = ConfigurationManager.ConnectionStrings["eServeConnection"].ConnectionString;
        }
        
        public int opportunityId { get; set; }
        public string OrganizationName { get; set; }
        public string OrganizationDesc { get; set; }
        public string OpportunityName { get; set; }
        public string OpportunityDesc { get; set; }
        public string SlotsAvailable { get; set; }
        public string DistanceFromSU { get; set; }       
        public string Location { get; set; }
        public string TimeCommittment { get; set; }
        public string SiteSupervisorName { get; set; }
        public string SiteSupervisorEmail { get; set; }
        public string BackgroundCheck { get; set; }
        public string MinimumAge { get; set; }
        public string Link { get; set; }
        public string OtherRequirements { get; set; }

        private DatabaseHelper dbHelper;

        public l_OpportunityDetail GetOpportunityDetailById(int opportunityId)
        {
            var reader = dbHelper.GetOpportunityDetailByOpportunityId(Constant.SP_GetOpportunityDetailByOpportunityId, opportunityId);
            List<l_OpportunityDetail> detailList = new List<l_OpportunityDetail>();
            l_OpportunityDetail lOpportunityDetail = null;
            
            while (reader.Read())
            {
                lOpportunityDetail = new l_OpportunityDetail();

                lOpportunityDetail.OrganizationName = reader["OrganizationName"].ToString();
                lOpportunityDetail.OrganizationDesc = reader["OrganizationDesc"].ToString();
                lOpportunityDetail.OpportunityName = reader["OpportunityName"].ToString();
                lOpportunityDetail.OpportunityDesc = reader["OpportunityDesc"].ToString();
                //l_OpportunityDetail.SlotsAvailable = reader["SlotsAvailable"].ToString();
                //l_OpportunityDetail.DistanceFromSU = reader["DistanceFromSU"].ToString();
                lOpportunityDetail.Location = reader["Location"].ToString();
                lOpportunityDetail.TimeCommittment = reader["TimeCommittment"].ToString();
                lOpportunityDetail.SiteSupervisorName = reader["SiteSupervisorName"].ToString();
                lOpportunityDetail.SiteSupervisorEmail = reader["SupervisorEmail"].ToString();
                lOpportunityDetail.BackgroundCheck = reader["MinimumAge"].ToString();
                lOpportunityDetail.MinimumAge = reader["MinimumAge"].ToString();
                lOpportunityDetail.Link = reader["Link"].ToString();
                lOpportunityDetail.OtherRequirements = "Resume";

                detailList.Add(lOpportunityDetail);
            }

            return lOpportunityDetail;
        }
    }
}
