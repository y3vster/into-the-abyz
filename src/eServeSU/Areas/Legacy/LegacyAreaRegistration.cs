﻿using System.Web.Mvc;
using System.Web.Routing;
using Microsoft.AspNet.FriendlyUrls;

namespace eServeSU.Areas.Legacy
{
    public class LegacyAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Legacy";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {

            var routes = context.Routes;

            var settings = new FriendlyUrlSettings();
            settings.AutoRedirectMode = RedirectMode.Temporary;
            routes.EnableFriendlyUrls(settings);

            routes.IgnoreRoute("{resource}.aspx/{*pathInfo}");
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            // opportunity routes
            routes.MapPageRoute("opp_opp",
                "Opportunity/Opportunity",
                "~/Areas/Legacy/Opportunity/Opportunity.aspx", 
                false, null, new RouteValueDictionary(new { controller = new IncomingRequestConstraint()}));

            routes.MapPageRoute("opp_opplist",
                "Opportunity/OpportunityList",
                "~/Areas/Legacy/Opportunity/OpportunityList.aspx",
                false, null, new RouteValueDictionary(new { controller = new IncomingRequestConstraint()}));


            // community partner routes
            routes.MapPageRoute("cp_readeval",
                "CommunityPartnerContent/ReadEvaluation",
                "~/Areas/Legacy/CommunityPartnerContent/ReadEvaluation.aspx",
                false, null, new RouteValueDictionary(new { controller = new IncomingRequestConstraint()}));

            routes.MapPageRoute("cp_cpv",
                "CommunityPartnerContent/CommunityPartnerView",
                "~/Areas/Legacy/CommunityPartnerContent/CommunityPartnerView.aspx",
                false, null, new RouteValueDictionary(new { controller = new IncomingRequestConstraint()}));

            routes.MapPageRoute("cp_cpsv",
                "CommunityPartnerContent/CommunityPartnerStudentView",
                "~/Areas/Legacy/CommunityPartnerContent/CommunityPartnerStudentView.aspx",
                false, null, new RouteValueDictionary(new { controller = new IncomingRequestConstraint()}));

            // student routes
            routes.MapPageRoute("st_stprof",
                "StudentLegacy/StudentProfile",
                "~/Areas/Legacy/Student/StudentProfile.aspx",
                false, null, new RouteValueDictionary(new { controller = new IncomingRequestConstraint()}));

            routes.MapPageRoute("st_streg",
                "StudentLegacy/StudentRegistered",
                "~/Areas/Legacy/Student/StudentRegistered.aspx",
                false, null, new RouteValueDictionary(new { controller = new IncomingRequestConstraint()}));

            routes.MapPageRoute("st_stregistr",
                "StudentLegacy/StudentRegistration",
                "~/Areas/Legacy/Student/StudentRegistration.aspx",
                false, null, new RouteValueDictionary(new { controller = new IncomingRequestConstraint()}));

            // admin routes
            routes.MapPageRoute("adm_admprof",
                "AdminLegacy/AdminProfile",
                "~/Areas/Legacy/Admin/AdminProfile.aspx",
                false, null, new RouteValueDictionary(new { controller = new IncomingRequestConstraint()}));

            routes.MapPageRoute("adm_admcourselist",
                "AdminLegacy/AdminCourseListReport",
                "~/Areas/Legacy/Admin/AdminCourseListReport.aspx",
                false, null, new RouteValueDictionary(new { controller = new IncomingRequestConstraint()}));

            // faculty routes
            routes.MapPageRoute("fac_facopplist",
                "Faculty/FacultyOpportunityListView",
                "~/Areas/Legacy/Faculty/FacultyOpportunityListView.aspx",
                false, null, new RouteValueDictionary(new { controller = new IncomingRequestConstraint()}));

            routes.MapPageRoute("fac_faccourseopp",
                "Faculty/FacultyCourseOpportunity",
                "~/Areas/Legacy/Faculty/FacultyCourseOpportunity.aspx",
                false, null, new RouteValueDictionary(new { controller = new IncomingRequestConstraint()}));

            //context.MapRoute(
            //    "Legacy_default",
            //    "Legacy/{controller}/{action}/{id}",
            //    new { action = "Index", id = UrlParameter.Optional }
            //);
        }
    }
}