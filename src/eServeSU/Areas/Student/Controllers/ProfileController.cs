﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using eServeSU.Areas.StudentArea.Models;
using eServeSU.Models;
using Microsoft.AspNet.Identity;

namespace eServeSU.Areas.StudentArea.Controllers
{
    public class ProfileController : Controller
    {
        private eServeContext _db = new eServeContext();

        // GET: Student/Profile
        public async Task<ActionResult> Index()
        {
            //return View(await _db.Students.ToListAsync());
            return RedirectToAction("Update");
        }

        // GET: Student/Profile/Update
        [Authorize(Roles = "Student")]
        [ImportModelStateFromTempData]
        public async Task<ActionResult> Update()
        {
            string aspId = User.Identity.GetUserId();
            Student st = await _db.Students.FindAsync(aspId) ?? new Student();

            var focusAreas = _db.Opp_FocusAreas.ToList();
            var oppTypes = _db.Opp_Types.ToList();
            var ethnRaces = _db.Ethinicities.ToList();
            StudentViewModel svm = new StudentViewModel(st, focusAreas, oppTypes, ethnRaces);

            return View(svm);
        }

        // POST: Student/Profile/Update
        [Authorize(Roles = "Student")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        //public async Task<ActionResult> Update(StudentViewModel svm, int[] interest_areas)
        public async Task<ActionResult> Update(StudentViewModel svm)
        {
            if (svm == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            if (ModelState.IsValid)
            {
                var focusAreas = _db.Opp_FocusAreas.ToList();
                var oppTypes = _db.Opp_Types.ToList();
                var ethnRaces = _db.Ethinicities.ToList();
                svm.InjectDependencies(focusAreas, oppTypes, ethnRaces);

                string aspId = User.Identity.GetUserId();

                Student st = await _db.Students.FindAsync(aspId);

                bool isNew = st == null;

                if (isNew)
                    st = new Student { AspNetUser_ID = aspId };

                st.SaveStudentInfo(svm);

                if (isNew)
                    _db.Students.Add(st);
                else
                    _db.Entry(st).State = EntityState.Modified;

                _db.SaveChanges();
            }

            ViewBag.success = true;
            return View(svm);
        }


        ///////////////////////////////////////////
        // Save these methods for when we implement the admin functionality to edit profiles 

        //// GET: Student/Profile/Details/5
        //public async Task<ActionResult> Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Student student = await _db.Students.FindAsync(id);
        //    if (student == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(student);
        //}

        //// GET: Student/Profile/Create
        //public ActionResult Create()
        //{
        //    StudentViewModel svm = new StudentViewModel(new Student(), _db.FocusAreas.ToList());
        //    return View(svm);
        //}

        //// POST: Student/Profile/Create
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Create([Bind(Include = "AspNetUser_ID,DateOfBirth,FirstName,LastName,PreferedName,Gender,InternationalStudent,LastBackgroundCheck")] Student student)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        _db.Students.Add(student);
        //        await _db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }

        //    return View(student);
        //}



        // GET: Student/Profile/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Student student = await _db.Students.FindAsync(id);
            if (student == null)
            {
                return HttpNotFound();
            }
            return View(student);
        }

        // POST: Student/Profile/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "AspNetUser_ID,DateOfBirth,FirstName,LastName,PreferedName,Gender,InternationalStudent,LastBackgroundCheck")] Student student)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(student).State = EntityState.Modified;
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(student);
        }

        // GET: Student/Profile/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Student student = await _db.Students.FindAsync(id);
            if (student == null)
            {
                return HttpNotFound();
            }
            return View(student);
        }

        // POST: Student/Profile/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Student student = await _db.Students.FindAsync(id);
            _db.Students.Remove(student);
            await _db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
