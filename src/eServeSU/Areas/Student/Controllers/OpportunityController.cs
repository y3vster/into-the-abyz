﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using eServeSU.Areas.Partner.Controllers;
using eServeSU.Models;
using eServeSU.Areas.StudentArea.Models;
using Microsoft.AspNet.Identity;

namespace eServeSU.Areas.StudentArea.Controllers
{
    [Authorize(Roles = "Student")]
    public class OpportunityController : Controller
    {
        private eServeContext _db = new eServeContext();
        private string _usrID;


        public string UserID
        {
            get
            {
                if (_usrID == null) { _usrID = User.Identity.GetUserId(); }
                return _usrID;
            }
            set { _usrID = value; }
        }

        // GET: Student/Opportunity
        [ExportModelStateToTempData]
        public async Task<ActionResult> Index()
        {
            // student requires a profile to register for opportunities
            if (!_db.Students.Any(st => st.AspNetUser_ID == UserID))
            {
                ModelState.AddModelError("", "A student profile is required to register for opportunities");
                return RedirectToAction("Update", "Profile");
            }

            return View();
        }

        // GET: Student/Opportunity/DetailsModal/5
        public async Task<ActionResult> DetailsModal(int? id)
        {

            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Opportunity opp = await _db.Opportunities.FindAsync(id);
            if (opp == null)
                return new HttpNotFoundResult();

            return View("DetailsModalPartial", opp);
        }

        // AVAILABLE Opportunities
        [ChildActionOnly]
        public ActionResult AvailablePartial()
        {
            var opportunities = _db.Opportunities
                .Where(opp => opp.Students.All(st => st.AspNetUser_ID != UserID))
                .Include(o => o.CommunityPartner)
                .Include(o => o.CommunityPartnerStaff)
                .Include(o => o.Opp_Type)
                .Include(o => o.Univ_Quarter)
                .ToList();

            var oppAvailViewModels = opportunities.Select(opp => new OpportunityRegistrationViewModel(opp));

            return View("AvailableOpportunities", oppAvailViewModels);

        }

        // REGISTERED Opportunities
        [ChildActionOnly]
        public ActionResult RegisteredPartial()
        {
            // TODO: oh dear god, do a performance trace on this monster
            var opportunities = _db.Opportunities
                .Where(opp => opp.Students.Any(st => st.AspNetUser_ID == UserID))
                .Include(o => o.CommunityPartner)
                .Include(o => o.CommunityPartnerStaff)
                .Include(o => o.Opp_Type)
                .Include(o => o.Univ_Quarter)
                .ToList();

            var oppRegViewModels = opportunities.Select(opp => new RegisteredOpportunityViewModel(opp));

            return View("RegisteredOpportunities", oppRegViewModels);
        }


        // POST: Student/Opportunity/SignUp
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SignUp(
            [Bind(Include = "id")]
            int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Opportunity opp = await _db.Opportunities.FindAsync(id);
            if (opp == null)
                return new HttpNotFoundResult();

            // insert logic

            Student me = new Student {AspNetUser_ID = UserID};
            _db.Students.Attach(me);
            opp.Students.Add(me);
            _db.SaveChanges();

            return View("ConfirmationModalPartial", opp);
        }



        // GET: Student/Opportunity/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Student/Opportunity/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

    }
}
