﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using eServeSU.Areas.StudentArea.Models;

namespace eServeSU.Models
{
    public partial class Student
    {
        public void SaveStudentInfo(StudentViewModel svm)
        {
            if (svm.DateOfBirth.HasValue)
                this.DateOfBirth = svm.DateOfBirth.Value;
            this.FirstName = svm.FirstName;
            this.LastName = svm.LastName;
            this.Gender = svm.Gender;
            if (svm.InternationalStudent != null)
                this.InternationalStudent = svm.InternationalStudent.Value;
            this.PreferedName = svm.PreferedName;

            // update focus areas
            var updatedFocusAreas = GetValidSelectedIDs(svm.SelectedFocusAreaIDs);
            var currentFocusAreas = new HashSet<Opp_FocusArea>(this.Opp_FocusAreas);
            foreach (Opp_FocusArea focusArea in svm.AllFocusAreas)
            {
                if (updatedFocusAreas.Contains(focusArea.ID))
                {
                    if (!currentFocusAreas.Contains(focusArea))
                    {
                        this.Opp_FocusAreas.Add(focusArea);
                    }
                }
                else if (currentFocusAreas.Contains(focusArea))
                {
                    this.Opp_FocusAreas.Remove(focusArea);
                }
            }

            // opportunity types
            var updatedOppTypes = GetValidSelectedIDs(svm.SelectedOppTypeIDs);
            var currentOppTypes = new HashSet<Opp_Type>(this.Opp_Types);
            foreach (Opp_Type opp in svm.AllOpportunityTypes)
            {
                if (updatedOppTypes.Contains(opp.ID))
                {
                    if (!currentOppTypes.Contains(opp))
                    {
                        this.Opp_Types.Add(opp);
                    }
                }
                else if (currentOppTypes.Contains(opp))
                {
                    this.Opp_Types.Remove(opp);
                }
            }

            // update focus areas
            var updatedEthnRaces = GetValidSelectedIDs(svm.SelectedRaceEthnIDs);
            var currentEthnRaces = new HashSet<Ethinicity>(this.Ethinicities);
            foreach (Ethinicity ethn in svm.AllEthinicities)
            {
                if (updatedEthnRaces.Contains(ethn.ID))
                {
                    if (!currentEthnRaces.Contains(ethn))
                    {
                        this.Ethinicities.Add(ethn);
                    }
                }
                else if (currentEthnRaces.Contains(ethn))
                {
                    this.Ethinicities.Remove(ethn);
                }
            }
        }

        /// <summary>
        /// Convert list of strings into hashset of ints
        /// </summary>
        private HashSet<int> GetValidSelectedIDs(IList<string> list)
        {
            var hs = new HashSet<int>();
            foreach (string str in list)
            {
                int val;
                if (int.TryParse(str, out val))
                {
                    hs.Add(val);
                }
            }
            return hs;
        }
    }
}