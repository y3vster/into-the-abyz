﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using eServeSU.Models;

namespace eServeSU.Areas.StudentArea.Models
{
    public class OpportunityRegistrationViewModel
    {
        public OpportunityRegistrationViewModel(Opportunity opp)
        {
            ID = opp.ID;
            OpportunityType = opp.Opp_Type.Name;
            Name = opp.Name;
            Organization = opp.CommunityPartner.OrganizationName;
            SlotsAvailable = opp.TotalNumberOfSlots;
            DistanceFromSU = opp.DistanceFromSU;
            Location = opp.Location;
            TimeCommitment = opp.TimeCommittment;
            DateCreated = opp.DateOfCreation;
            Status = opp.Status;
        }

        public int ID { get; set; }

        public string Name { get; set; }

        [Display(Name = "Organization Name")]
        public string Organization { get; set; }

        [Display(Name = "# of Slots")]
        public int? SlotsAvailable { get; set; }

        [Display(Name = "Distance from Su")]
        public double? DistanceFromSU { get; set; }

        [Display(Name = "Location")]
        public string Location { get; set; }

        [Display(Name = "Minimum Time Commitment")]
        public string TimeCommitment { get; set; }

        [Display(Name = "Type")]
        public string OpportunityType { get; set; }


        [Display(Name = "Added")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime DateCreated { get; set; }

        [Display(Name = "Status")]
        public string Status { get; set; }
    }
}