﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using eServeSU.Models;

namespace eServeSU.Areas.StudentArea.Models
{
    public class RegisteredOpportunityViewModel
    {

        public RegisteredOpportunityViewModel(Opportunity opp)
        {
            ID = opp.ID;
            AcademicTerm = opp.Univ_Quarter.QuarterName;
            Organization = opp.CommunityPartner.OrganizationName;
            OpportunityName = opp.Name;
            Hours = opp.JobHours;
            Status = opp.Status;

        }
        public int ID { get; set; }
        
        [Display(Name = "Academic Term")]
        public string AcademicTerm { get; set; }

        [Display(Name = "Organization Name")]
        public string Organization { get; set; }

        [Display(Name = "Opportunity Name")]
        public string OpportunityName { get; set; }

        [Display(Name = "Available Hours")]
        public int? Hours { get; set; }

        [Display(Name = "Status")]
        public string Status { get; set; }
    }

}