﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using eServeSU.Areas.StudentArea.Controllers;
using eServeSU.Areas.StudentArea.Models;
using eServeSU.Models;

namespace eServeSU.Areas.StudentArea.Models
{
    /// <summary>
    /// Helps with binding the studentviewmodel when student profile is updated with post data
    /// </summary>
    public class StudentViewModelBinder : DefaultModelBinder
    {
        protected override void BindProperty(ControllerContext controllerContext, ModelBindingContext bindingContext,
            PropertyDescriptor propertyDescriptor)
        {
            base.BindProperty(controllerContext, bindingContext, propertyDescriptor);
        }

        protected override object CreateModel(ControllerContext controllerContext, ModelBindingContext bindingContext, Type modelType)
        {
            //if (modelType == typeof(StudentViewModel))
            //{
            //    var db = new Entities();
            //    var focusAreaList = db.FocusAreas.AsNoTracking().ToList();
            //    return new StudentViewModel(focusAreaList);
            //}
            return base.CreateModel(controllerContext, bindingContext, modelType);
        }
    }
}