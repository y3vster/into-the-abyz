﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using System;
using System.Collections.Generic;
using eServeSU.Models;

namespace eServeSU.Areas.StudentArea.Models
{
    public class CheckBoxInfo
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public bool IsSelected { get; set; }
    }

    //[ModelBinder(typeof(StudentViewModelBinder))]
    public partial class StudentViewModel
    {
        public IList<Opp_FocusArea> AllFocusAreas { get; set; }
        public IList<Opp_Type> AllOpportunityTypes { get; set; }
        public IList<Ethinicity> AllEthinicities { get; set; }


        /// <summary>
        /// MUST BE CALLED IF THE VIEW MODEL IS BOUND
        /// </summary>
        public void InjectDependencies(IList<Opp_FocusArea> focusAreas, IList<Opp_Type> oppTypes, IList<Ethinicity> ethnicities)
        {
            AllFocusAreas = focusAreas;
            AllOpportunityTypes = oppTypes;
            AllEthinicities = ethnicities;
        }

        /// <summary>
        /// Accepts an existing student record, used when a profile already exists
        /// </summary>
        /// <param name="s"></param>
        /// <param name="allFocusAreas"></param>
        public StudentViewModel(Student s, IList<Opp_FocusArea> allFocusAreas, IList<Opp_Type> oppTypes, IList<Ethinicity> ethnicities)
        {
            FirstName = s.FirstName;
            LastName = s.LastName;
            PreferedName = s.PreferedName;
            DateOfBirth = s.DateOfBirth;
            Gender = s.Gender;
            InternationalStudent = s.InternationalStudent;
            LastBackgroundCheck = s.LastBackgroundCheck;
            SelectedFocusAreaIDs = s.Opp_FocusAreas.Select(fa => fa.ID.ToString()).ToList();
            SelectedOppTypeIDs = s.Opp_Types.Select(fa => fa.ID.ToString()).ToList();
            SelectedRaceEthnIDs = s.Ethinicities.Select(fa => fa.ID.ToString()).ToList();

            AllFocusAreas = allFocusAreas;
            AllOpportunityTypes = oppTypes;
            AllEthinicities = ethnicities;
        }

        public StudentViewModel() { }

        [Required]
        [Display(Name = "First Name")]
        [StringLength(15)]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        [StringLength(15)]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "Preferred Name")]
        [StringLength(10)]
        public string PreferedName { get; set; }

        [Required(ErrorMessage = "Please enter a valid birth date.")]
        [DataType(DataType.Date)]
        [Display(Name = "Date of Birth")]
        public DateTime? DateOfBirth { get; set; }

        [StringLength(25)]
        public string Gender { get; set; }

        [Required]
        [DataType("bool")]
        [Display(Name = "Are you an international student?")]
        public bool? InternationalStudent { get; set; }

        [Display(Name = "Last background check date (can be left blank):")]
        public DateTime? LastBackgroundCheck { get; set; }

        [Display(Name = "(Optional) Select your focus areas")]
        public IList<string> SelectedFocusAreaIDs { get; set; } = new List<string>();

        [Display(Name = "(Optional) Select oppotunities that interest you")]
        public IList<string> SelectedOppTypeIDs { get; set; } = new List<string>();

        [Display(Name = "(Optional) Select any that apply")]
        public IList<string> SelectedRaceEthnIDs { get; set; } = new List<string>();



        /// <summary>
        /// Generates checkbox info list from some collection of fields, like interest areas
        /// </summary>
        private IList<CheckBoxInfo> GenerateBoxesFor<TField>(
            IList<TField> propertyCollection,
            IList<string> selectedList,
            Func<TField, int> idSelector,
            Func<TField, string> nameSelector)
        {
            var currentSelections = new HashSet<string>(selectedList);
            var boxes = from item in propertyCollection
                        select new CheckBoxInfo {
                            Id = idSelector(item).ToString(),
                            Name = nameSelector(item),
                            IsSelected = currentSelections.Contains(idSelector(item).ToString())
                        };
            return boxes.ToList();
        }

        private IList<CheckBoxInfo> _focusAreaBoxes;
        public IList<CheckBoxInfo> FocusAreasBoxes
        {
            get { return _focusAreaBoxes ?? (_focusAreaBoxes = GenerateBoxesFor(AllFocusAreas, SelectedFocusAreaIDs, fa => fa.ID, fa => fa.AreaName)); }
        }

        private IList<CheckBoxInfo> _oppTypeBoxes;
        public IList<CheckBoxInfo> OpportunityTypeBoxes
        {
            get { return _oppTypeBoxes ?? (_oppTypeBoxes = GenerateBoxesFor(AllOpportunityTypes, SelectedOppTypeIDs, fa => fa.ID, fa => fa.Name)); }
        }

        private IList<CheckBoxInfo> _raceEthnBoxes;
        public IList<CheckBoxInfo> EthnicitiesBoxes
        {
            get { return _raceEthnBoxes ?? (_raceEthnBoxes = GenerateBoxesFor(AllEthinicities, SelectedRaceEthnIDs, fa => fa.ID, fa => fa.Description)); }
        }


    }
}
