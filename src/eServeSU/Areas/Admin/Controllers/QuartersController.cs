﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using eServeSU.Models;

namespace eServeSU.Areas.Admin.Controllers
{
    public class QuartersController : Controller
    {
        private eServeContext db = new eServeContext();

        // GET: Admin/Quarters
        public async Task<ActionResult> Index()
        {
            return View(await db.Univ_Quarters.ToListAsync());
        }

        // GET: Admin/Quarters/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Univ_Quarter quarter = await db.Univ_Quarters.FindAsync(id);
            if (quarter == null)
            {
                return HttpNotFound();
            }
            return View(quarter);
        }

        // GET: Admin/Quarters/Create
        public ActionResult Create()
        {
            Univ_Quarter qvm = new Univ_Quarter();
            return View(qvm);
        }

        // POST: Admin/Quarters/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,QuarterName,ShortName,StartDate,EndDate")] Univ_Quarter quarter)
        {
            if (ModelState.IsValid)
            {
                db.Univ_Quarters.Add(quarter);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(quarter);
        }

        // GET: Admin/Quarters/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Univ_Quarter quarter = await db.Univ_Quarters.FindAsync(id);
            if (quarter == null)
            {
                return HttpNotFound();
            }
            return View(quarter);
        }

        // POST: Admin/Quarters/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "QuarterID,QuarterName,ShortName,StartDate,EndDate")] Univ_Quarter quarter)
        {
            if (ModelState.IsValid)
            {
                db.Entry(quarter).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(quarter);
        }

        // GET: Admin/Quarters/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Univ_Quarter quarter = await db.Univ_Quarters.FindAsync(id);
            if (quarter == null)
            {
                return HttpNotFound();
            }
            return View(quarter);
        }

        // POST: Admin/Quarters/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Univ_Quarter quarter = await db.Univ_Quarters.FindAsync(id);
            db.Univ_Quarters.Remove(quarter);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
