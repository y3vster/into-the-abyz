﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using eServeSU.Models;

namespace eServeSU.Areas.Admin.Controllers
{
    public class ProfessorsController : Controller
    {
        private eServeContext db = new eServeContext();

        // GET: Admin/Professors
        public ActionResult Index()
        {
            return View(db.Univ_Professors.ToList());
        }

        // GET: Admin/Professors/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Univ_Professors univ_Professors = db.Univ_Professors.Find(id);
            if (univ_Professors == null)
            {
                return HttpNotFound();
            }
            return View(univ_Professors);
        }

        // GET: Admin/Professors/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Professors/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,FirstName,LastName,EmailID,Phone")] Univ_Professors univ_Professors)
        {
            if (ModelState.IsValid)
            {
                db.Univ_Professors.Add(univ_Professors);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(univ_Professors);
        }

        // GET: Admin/Professors/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Univ_Professors univ_Professors = db.Univ_Professors.Find(id);
            if (univ_Professors == null)
            {
                return HttpNotFound();
            }
            return View(univ_Professors);
        }

        // POST: Admin/Professors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,FirstName,LastName,EmailID,Phone")] Univ_Professors univ_Professors)
        {
            if (ModelState.IsValid)
            {
                db.Entry(univ_Professors).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(univ_Professors);
        }

        // GET: Admin/Professors/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Univ_Professors univ_Professors = db.Univ_Professors.Find(id);
            if (univ_Professors == null)
            {
                return HttpNotFound();
            }
            return View(univ_Professors);
        }

        // POST: Admin/Professors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Univ_Professors univ_Professors = db.Univ_Professors.Find(id);
            db.Univ_Professors.Remove(univ_Professors);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
