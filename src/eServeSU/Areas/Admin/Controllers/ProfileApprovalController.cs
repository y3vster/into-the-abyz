﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using eServeSU.Models;
using eServeSU.Areas.Partner.Models;
using eServeSU.Models.RegistrationStates;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace eServeSU.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ProfileApprovalController : Controller
    {
        private IeServeContext _db;
        private ApplicationUserManager _usrMgr;

        public ProfileApprovalController()
        {
            _db = new eServeContext();
            _usrMgr = null;
        }

        public ProfileApprovalController(IeServeContext context, ApplicationUserManager usrMgr)
        {
            _db = context;
            _usrMgr = usrMgr;
        }

        public ProfileApprovalController(IeServeContext context)
        {
            _db = context;
            _usrMgr = null;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                if (_usrMgr == null)
                {
                    _usrMgr = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
                }
                return _usrMgr;
            }
        }

        // Shows all unapproved pending profiles
        // GET: Admin/ProfileApproval
        public ActionResult Index()
        {
            List<CommunityPartnerPendingApproval> pendingOrganizations = _db.CommunityPartnerPendingApprovals
                .Include(cpa => cpa.CommunityPartner)
                .ToList();

            List<CommunityPartner> pendingPartners = pendingOrganizations
                .Select(cpa => cpa.CommunityPartner)
                .ToList();

            return View(pendingPartners);
        }

        // Returns details of the organization 
        // GET: Admin/ProfileApproval/Details/5
        public ActionResult DetailsModal(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CommunityPartner cp = _db.CommunityPartners.SingleOrDefault(c => c.AspNetUser_ID == id);

            if (cp == null)
            {
                return HttpNotFound();
            }
            ViewBag.PartnerLoginUserName = UserManager.FindById(cp.AspNetUser_ID).UserName;

            return View("DetailsModal",cp);
        }

        // GET: Admin/ProfileApproval/Approve/{id}
        public ActionResult Approve(string id)
        {
            // place profile in approved state
            var currentState = RegistrationStateFactory.GetRegistrationState(UserManager, _db, id);
            currentState.StateApproved();

            this.AddNotification("Profile approved: "+ UserManager.FindById(id).UserName, NotificationType.SUCCESS);
            return RedirectToAction("Index");
        }

        // GET: Admin/ProfileApproval/Reject/{id}
        public ActionResult Reject(string id)
        {
            // place profile in rejected state
            var currentState = RegistrationStateFactory.GetRegistrationState(UserManager, _db, id);
            currentState.StateRejected();

            this.AddNotification("Profile rejected: "+ UserManager.FindById(id).UserName, NotificationType.SUCCESS);
            return RedirectToAction("Index");
        }

    }
}
