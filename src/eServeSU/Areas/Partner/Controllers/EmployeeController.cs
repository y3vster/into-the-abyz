using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using eServeSU.Models;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Web;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

namespace eServeSU.Areas.Partner.Controllers
{
    public class EmployeeController : Controller
    {
        private IeServeContext _db;
        private ApplicationUserManager _usrMgr;
        private IAuthenticationManager _authMgr;
        private string _usrID;

        public EmployeeController()
        {
            _db = new eServeContext();
            _usrID = null;
        }


        public EmployeeController(IeServeContext context, string userID)
        {
            _db = context;
            _usrID = userID;
        }

        public string GetUserID
        {
            get
            {
                if (_usrID == null)
                {
                    _usrID = User.Identity.GetUserId();
                }
                return _usrID;
            }
        }
        public ApplicationUserManager UserManager
        {
            get
            {
                if (_usrMgr == null) { _usrMgr = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>(); }
                return _usrMgr;
            }
        }
        public IAuthenticationManager AuthenticationManager
        {
            get
            {
                if (_authMgr == null) { _authMgr = HttpContext.GetOwinContext().Authentication; }
                return _authMgr;
            }
        }

        ////this is for testing as a workaround for Mocking extension methods
        ////the Mock failed return null for GetUserId() (see test commented code)
        //public Func<string> GetUserId; //For testing
        //public EmployeeController()
        //{
        //    GetUserId = () => User.Identity.GetUserId();
        //}

        // return true if the current partner has a valid org profile
        private bool OrganizationProfileExists(string aspNetUser_ID)
        {
            return _db.CommunityPartners.Any(cp => cp.AspNetUser_ID == aspNetUser_ID);
        }


        // GET: Partner/Employee
        [Authorize(Roles = "Partner")]
        public async Task<ActionResult> Index()
        {
            //string myID = User.Identity.GetUserId();
            string myID = GetUserID;

            if (!OrganizationProfileExists(myID))
            {
                this.AddNotification("Please fill out your organization profile before managing employees.",
                    NotificationType.ERROR);
                return RedirectToAction("Profile", "Organization");
            }

            var allRows = await GetAllRows();
            ViewData["row_data"] = JsonConvert.SerializeObject(allRows.Data);
            return View();
        }


        private async Task<JsonResult> GetAllRows()
        {
            //string myID = User.Identity.GetUserId();
            string myID = GetUserID;

            var employees = (await _db.CommunityPartnerStaff
                .Where(cps => cps.CommunityPartner_ID == myID)
                .ToListAsync())
                .Select(e => new {
                    ID = e.ID,
                    TitleFirstLastName = e.TitleFirstLastName,
                    Title = e.Title,
                    FirstName = e.FirstName,
                    LastName = e.LastName,
                    Email = e.Email,
                    Phone = e.Phone
                });
            return Json(employees);
        }

        [Authorize(Roles = "Partner")]
        [OutputCache(Duration = 0, NoStore = true)]
        public async Task<ActionResult> Rows()
        {
            return await GetAllRows();
        }


        // CREATE / EDIT
        // --------------------------------------------------
        // POST: Partner/Employee/CreateUpdate
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Partner")]
        public async Task<ActionResult> CreateUpdate(
            [Bind(Include = "ID, FirstName, LastName, Title, Email, Phone")]
            CommunityPartnerEmployee empl)
        {
            //string partnerID = User.Identity.GetUserId();
            string partnerID = GetUserID;
            empl.CommunityPartner_ID = partnerID;

            // TODO: get the model binder for this to work
            ModelState.Clear();
            TryValidateModel(empl);

            if (!ModelState.IsValid)
            {
                // return errors
                var allErrors = ModelState.Values.SelectMany(v => v.Errors);
                return Json(allErrors);
            }

            // model is valid at this point

            bool is_new_record = empl.ID == 0;

            if (is_new_record)
                _db.CommunityPartnerStaff.Add(empl);
            else
                _db.Entry(empl).State = EntityState.Modified;

            await _db.SaveChangesAsync();

            JsonResult allRows = await GetAllRows();

            return Json(new
            {
                record_inserted = is_new_record,
                record_updated = !is_new_record,
                data_rows = allRows.Data
            });
        }


        // Delete
        // --------------------------------------------------
        // POST: Partner/Employee/Delete
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Partner")]
        public async Task<ActionResult> Delete([Bind(Include = "ID")] int ID)
        {
            //string partnerID = User.Identity.GetUserId();
            string partnerID = GetUserID;

            var objToRemove = _db.CommunityPartnerStaff
                .Where(cp =>
                    cp.ID == ID &&
                    cp.CommunityPartner_ID == partnerID);

            var result = _db.CommunityPartnerStaff.RemoveRange(objToRemove);

            await _db.SaveChangesAsync();

            JsonResult allRows = await GetAllRows();

            return Json(new {
                record_deleted = true,
                data_rows = allRows.Data
            });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
