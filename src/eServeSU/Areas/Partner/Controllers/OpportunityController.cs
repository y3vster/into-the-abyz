﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DocumentFormat.OpenXml.Office2010.CustomUI;
using eServeSU.Areas.Partner.Models;
using eServeSU.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace eServeSU.Areas.Partner.Controllers
{
    public class OpportunityController : Controller
    {
        private readonly eServeContext _db = new eServeContext();
        private string _usrID;

        public string UserID
        {
            get
            {
                if (_usrID == null)
                {
                    _usrID = User.Identity.GetUserId();
                }
                return _usrID;
            }
        }

        private IEnumerable<SelectListItem> GetAllSupervisors(int? selected = null)
        {
            string myID = User.Identity.GetUserId();
            IEnumerable<SelectListItem> supervisors =
                _db.CommunityPartnerStaff
                .Where(sup => sup.CommunityPartner_ID == myID)
                .ToList()
                .Select(sup => new SelectListItem {
                    Text = sup.TitleFirstLastName,
                    Value = sup.ID.ToString(),
                    Selected = sup.ID == selected
                });
            return supervisors;
        }

        // GET: Partner/Opportunities
        [ExportModelStateToTempData]
        public async Task<ActionResult> Index()
        {
            if (User.IsInRole("Partner"))
                return await IndexPartner();

            if (User.IsInRole("Admin"))
                return await IndexAdmin();

            return RedirectToAction("Index", "Home", new { area = "" });
        }


        // TODO: "PartnerJustApproved"
        private async Task<ActionResult> IndexPartner()
        {
            if (!User.IsInRole("PartnerCanAddOpportunities"))
            {
                if (!_db.CommunityPartners.Any(cp => cp.AspNetUser_ID == UserID))
                {
                    //ModelState.AddModelError("", "Before adding an opportunity, please update your organization's profile.");
                    this.AddNotification("Before adding an opportunity, please update your organization's profile.", NotificationType.ERROR);
                    return RedirectToAction("Profile", "Organization");
                }

                if (User.IsInRole("PartnerPending"))
                {
                    //ModelState.AddModelError("", "Please wait for Admin to approve your profile before adding an opportunity.");
                    this.AddNotification("Please wait for Admin to approve your profile before adding an opportunity.", NotificationType.INFO);
                    return RedirectToAction("Index", "Home", new { area = "" });
                }

                if (User.IsInRole("PartnerRejected"))
                {
                    //ModelState.AddModelError("", "Admin has rejected your profile. Please update your profile and resubmit.");
                    this.AddNotification("Admin has rejected your profile. Please update your profile and resubmit.", NotificationType.ERROR);
                    return RedirectToAction("Profile", "Organization");
                }
            }

            if (!_db.CommunityPartnerStaff.Any(empl => empl.CommunityPartner_ID == UserID))
            {
                //ModelState.AddModelError("", "Before adding an opportunity, please create a profile for at least one employee.");
                this.AddNotification("Before adding an opportunity, please create a profile for at least one employee.", NotificationType.ERROR);
                return RedirectToAction("Index", "Employee");
            }

            var opportunities = (await _db.Opportunities
                .Where(opp => opp.CommunityPartner_ID == UserID)
                .Include(o => o.CommunityPartner)
                .Include(o => o.CommunityPartnerStaff)
                .Include(o => o.Opp_Type)
                .Include(o => o.Univ_Quarter)
                .ToListAsync())
                .Select(opp => new OpportunityViewModel(opp));

            return View("IndexPartner", opportunities);
        }



        private async Task<ActionResult> IndexAdmin()
        {
            IQueryable<Opportunity> opportunities = _db.Opportunities.Include(o => o.CommunityPartner).Include(o => o.CommunityPartnerStaff).Include(o => o.Opp_Type).Include(o => o.Univ_Quarter);

            return View("IndexAdmin", await opportunities.ToListAsync());
        }



        // GET: Partner/Opportunities/Create
        [Authorize(Roles = "PartnerCanAddOpportunities")]
        [ExportModelStateToTempData]
        public ActionResult Create()
        {
            string AspNetUser_ID = User.Identity.GetUserId();

            var userMgr = Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var myRoles = userMgr.GetRoles(UserID);
            var cppa = new CommunityPartnerPendingApproval { AspNetUser_ID = UserID };



            Opportunity opp = new Opportunity();

            ViewBag.Type_ID = new SelectList(_db.Opp_Types, "ID", "Name");
            ViewBag.Quarter_ID = new SelectList(_db.Univ_Quarters, "ID", "QuarterName");
            ViewBag.Supervisors = GetAllSupervisors(selected: opp.CommPartnerStaff_ID);
            return View(opp);
        }



        // POST: Partner/Opportunities/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize(Roles = "Partner")]
        [ExportModelStateToTempData]
        [ValidateAntiForgeryToken]
        //[ShowAllModelErrors]      // shows all model errors, use for debugging
        public async Task<ActionResult> Create(
            [Bind(Include = "Type_ID, CommPartnerStaff_ID,  Quarter_ID, Name, Location, JobDescription, Requirements, TimeCommittment, TotalNumberOfSlots, OrientationDate, ResumeRequired, MinimumAge, CRCRequiredByPartner, CRCRequiredBySU, LinkToOnlineApp, JobHours, DistanceFromSU")] Opportunity opp)
        {
            opp.CommunityPartner_ID = UserID;

            ModelState.Clear();
            TryValidateModel(opp);

            // INSERT
            if (ModelState.IsValid)
            {
                _db.Opportunities.Add(opp);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");

                // TODO: redirect to record view
            }

            ViewBag.Type_ID = new SelectList(_db.Opp_Types, "ID", "Name", opp.Type_ID);
            ViewBag.Quarter_ID = new SelectList(_db.Univ_Quarters, "ID", "QuarterName", opp.Quarter_ID);
            ViewBag.Supervisors = GetAllSupervisors(selected: opp.CommPartnerStaff_ID);
            return View(opp);
        }

        // GET: Partner/Opportunities/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Opportunity opportunity = await _db.Opportunities.FindAsync(id);
            if (opportunity == null)
            {
                return HttpNotFound();
            }
            return View(opportunity);
        }

        // Get: Partner/Opportunity/DetailsModal/5
        public async Task<ActionResult> DetailsModal(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Opportunity opportunity = await _db.Opportunities.FindAsync(id);
            if (opportunity == null)
            {
                return HttpNotFound();
            }
            return View("DetailsModalPartial", opportunity);
        }

        // GET: Partner/Opportunities/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Opportunity opportunity = await _db.Opportunities.FindAsync(id);
            if (opportunity == null || opportunity.CommunityPartner_ID != User.Identity.GetUserId())
            {
                return HttpNotFound();
            }
            ViewBag.Type_ID = new SelectList(_db.Opp_Types, "ID", "Name", opportunity.Type_ID);
            ViewBag.Quarters = new SelectList(_db.Univ_Quarters, "ID", "QuarterName", opportunity.Quarter_ID);
            ViewBag.Supervisors = GetAllSupervisors(selected: opportunity.CommPartnerStaff_ID);
            return View(opportunity);
        }

        // POST: Partner/Opportunities/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //[ShowAllModelErrors]      // shows all model errors, use for debugging
        public async Task<ActionResult> Edit(
            [Bind(Include = "ID, Type_ID, CommunityPartner_ID, CommPartnerStaff_ID, Quarter_ID, Name, Location, DateOfCreation, JobDescription, Requirements, TimeCommittment, TotalNumberOfSlots, OrientationDate, Status, ResumeRequired, MinimumAge, CRCRequiredByPartner, CRCRequiredBySU, LinkToOnlineApp, SupervisorEmail, JobHours, DistanceFromSU")]
            [ModelBinder(typeof(UserIdPropertyBinder))]
            Opportunity opportunity)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(opportunity).State = EntityState.Modified;
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.Type_ID = new SelectList(_db.Opp_Types, "ID", "Name", opportunity.Type_ID);
            ViewBag.Quarters = new SelectList(_db.Univ_Quarters, "ID", "QuarterName", opportunity.Quarter_ID);
            ViewBag.Supervisors = GetAllSupervisors(selected: opportunity.CommPartnerStaff_ID);
            return View(opportunity);
        }

        // GET: Partner/Opportunities/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Opportunity opportunity = await _db.Opportunities.FindAsync(id);
            if (opportunity == null)
            {
                return HttpNotFound();
            }
            return View(opportunity);
        }

        // POST: Partner/Opportunities/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Opportunity opportunity = await _db.Opportunities.FindAsync(id);
            _db.Opportunities.Remove(opportunity);
            await _db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
