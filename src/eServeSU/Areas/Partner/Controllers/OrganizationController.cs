﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using eServeSU.Areas.Partner.Models;
using eServeSU.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using eServeSU.Controllers;
using eServeSU.Models.RegistrationStates;
using Microsoft.Owin;
using Microsoft.Owin.Security;

namespace eServeSU.Areas.Partner.Controllers
{
    public class OrganizationController : Controller
    {
        private IeServeContext _db;
        private ApplicationUserManager _usrMgr;
        private IAuthenticationManager _authMgr;
        private string _usrID;

        public OrganizationController()
        {
            _db = new eServeContext();
        }

        /// <summary>
        /// Ctor used for unit testing - dependency injection
        /// </summary>
        public OrganizationController(IeServeContext context, ApplicationUserManager userManager, string userID)
        {
            _db = context;
            _usrMgr = userManager;
            _usrID = userID;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                if (_usrMgr == null) { _usrMgr = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>(); }
                return _usrMgr;
            }
        }
        
	public IAuthenticationManager AuthenticationManager
        {
            get
            {
                if (_authMgr == null) { _authMgr = HttpContext.GetOwinContext().Authentication; }
                return _authMgr;
            }
        }

        public string GetUserID
        {
            get
            {
                if (_usrID == null) { _usrID = User.Identity.GetUserId(); }
                return _usrID;
            }
        }




        // GET: Partner/Organization
        [Authorize(Roles = "Admin, Partner")]
        public async Task<ActionResult> Index()
        {
            // Admin gets to view everyone's profiles in a list
            if (User.IsInRole("Admin"))
                return View(await _db.CommunityPartners.ToListAsync());

            // But the partner only gets to edit own
            return RedirectToAction("Profile");
        }

        // Get: /Partner/Organization/Profile
        [Authorize(Roles = "Partner")]
        [ImportModelStateFromTempData]
        public async Task<ActionResult> Profile()
        {
            string AspNetUser_ID = User.Identity.GetUserId();
            CommunityPartner communityPartner = await _db.CommunityPartners.FindAsync(AspNetUser_ID) ?? new CommunityPartner();
            return View(communityPartner);
        }

        // POST: Partner/Organization/Profile
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Partner")]
        public async Task<ActionResult> Profile(
            [Bind(Include = "OrganizationName, Address, Website, MainPhone, MissionStatement, WorkDescription")]
            CommunityPartner communityPartner)
        {

            communityPartner.AspNetUser_ID = GetUserID;
            if (ModelState.IsValid)
            {
                if (_db.CommunityPartners.Any(cp => cp.AspNetUser_ID == GetUserID))
                {
                    this.AddNotification("Profile was updated. ", NotificationType.SUCCESS);
                    _db.Entry(communityPartner).State = EntityState.Modified;
                }
                else
                {
                    this.AddNotification("New profile was added. ", NotificationType.SUCCESS);
                    _db.CommunityPartners.Add(communityPartner);
                }
                await _db.SaveChangesAsync();

                // registration state logic model
                var currentState = RegistrationStateFactory.GetRegistrationState(UserManager, _db, GetUserID);
                currentState.StatePending();

                RefreshIdentityToken();

                //ViewBag.DebugMessage = string.Join("\n", currentState.TraceInfo.Select(line => "<p>" + line));

            }
            return View(communityPartner);
        }


        // fixes a bug where the roles would not update for the user
        private void RefreshIdentityToken()
        {
            var updatedUser =  UserManager.FindByName(User.Identity.Name);
            var newIdentity =  updatedUser.GenerateUserIdentity(UserManager);
            AuthenticationManager.SignOut();
            AuthenticationManager.SignIn(newIdentity);
        }




        // GET: Partner/Organization/Details/5
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> DetailsPartial(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CommunityPartner communityPartner = await _db.CommunityPartners.FindAsync(id);
            if (communityPartner == null)
            {
                return HttpNotFound();
            }
            return View(communityPartner);
        }

        // GET: Partner/Organization/Create
        [Authorize(Roles = "Admin")] // only admin can create new profiles
        public ActionResult Create()
        {
            return View();
        }

        // POST: Partner/Organization/Create
        [HttpPost]
        [Authorize(Roles = "Admin")] // only admin can create a new community partner profile, all others can only edit/update their own
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "AspNetUser_ID, OrganizationName, Address, Website, MainPhone, MissionStatement, WorkDescription")] CommunityPartner communityPartner)
        {
            if (ModelState.IsValid)
            {
                _db.CommunityPartners.Add(communityPartner);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(communityPartner);
        }


        // GET: Partner/Organization/Edit/5
        [Authorize(Roles = "Admin")] // only admin can edit any profile
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            CommunityPartner communityPartner = await _db.CommunityPartners.FindAsync(id);
            if (communityPartner == null)
            {
                return HttpNotFound();
            }
            return View(communityPartner);
        }

        // POST: Partner/Organization/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")] // only admin can save changes to any profile by id
        public async Task<ActionResult> Edit([Bind(Include = "AspNetUser_ID, OrganizationName, Address, Website, MainPhone, MissionStatement, WorkDescription")] CommunityPartner communityPartner)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(communityPartner).State = EntityState.Modified;
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(communityPartner);
        }

        // GET: Partner/Organization/Delete/5
        [Authorize(Roles = "Admin")] // only admin can delete profiles
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CommunityPartner communityPartner = await _db.CommunityPartners.FindAsync(id);
            if (communityPartner == null)
            {
                return HttpNotFound();
            }
            return View(communityPartner);
        }

        // POST: Partner/Organization/Delete/5
        [HttpPost, ActionName("Delete")]
        [Authorize(Roles = "Admin")] // only admin can delete profiles
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            CommunityPartner communityPartner = await _db.CommunityPartners.FindAsync(id);
            _db.CommunityPartners.Remove(communityPartner);
            await _db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
