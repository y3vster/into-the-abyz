﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using eServeSU.Models;
using Microsoft.AspNet.Identity;

namespace eServeSU.Areas.Partner.Models
{
    public class UserIdPropertyBinder : DefaultModelBinder
    {
        protected override object CreateModel(ControllerContext controllerContext, ModelBindingContext bindingContext, Type modelType)
        {
            if (modelType == typeof(Opportunity))
            {
                var userId = controllerContext.HttpContext.User.Identity.GetUserId();
                return new Opportunity(userId);
            }
            return base.CreateModel(controllerContext, bindingContext, modelType);
        }
    }
}