﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using eServeSU.Models;

namespace eServeSU.Areas.Partner.Models
{
    public class OpportunityViewModel
    {
        public OpportunityViewModel(Opportunity opp)
        {
            ID = opp.ID;
            Name = opp.Name;
            Location = opp.Location;
            OpportunityType = opp.Opp_Type.Name;
            DateCreated = opp.DateOfCreation;
            Status = opp.Status;
            Supervisor = opp.CommunityPartnerStaff.TitleFirstLastName;
        }

        public int ID { get; set; }

        public string Name { get; set; }

        public string Location { get; set; }

        [Display(Name = "Opportunity Type")]
        public string OpportunityType { get; set; }

        [Display(Name = "Date Created")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime DateCreated { get; set; }


        public string Status { get; set; }

        public string Supervisor { get; set; }
    }
}