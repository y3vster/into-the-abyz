﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using eServeSU.Areas.Partner.Controllers;
using eServeSU.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace eServeSU.Tests
{
    [TestClass]
    public class CommunityPartnerControllerTests
    {
        [TestMethod]
        public async Task Test_IndexViewProfileExists()
        {     
            // ARRANGE          
            ////mock the database
            var repositoryMock = new Mock<IeServeContext>();          
            
            ////mock the table 'CommunityPartner' for the db
            var setCPMock = new Mock<DbSet<CommunityPartner>>(); //.SetupData(data);
            ////mock a community partner to 'seed' the table for the db
            var dataCP = new List<CommunityPartner>
            { ////create in-memory data
                new CommunityPartner
                { 
                    Address = "123 4th St",
                    AspNetUser_ID = "test",
                    MainPhone = "1234567890",
                    MissionStatement = "test mission statement",
                    OrganizationName = "TEST",
                    Website = "test.test",
                    WorkDescription = "tester"
                }
            };
            ////mock the query data, setup implementation of IQueryable
            var queryableCP = dataCP.AsQueryable();
            setCPMock.As<IQueryable<CommunityPartner>>().Setup(m => m.Provider).Returns(queryableCP.Provider);
            setCPMock.As<IQueryable<CommunityPartner>>().Setup(m => m.Expression).Returns(queryableCP.Expression);
            setCPMock.As<IQueryable<CommunityPartner>>().Setup(m => m.ElementType).Returns(queryableCP.ElementType);
            setCPMock.As<IQueryable<CommunityPartner>>().Setup(m => m.GetEnumerator()).Returns(queryableCP.GetEnumerator());
            setCPMock.CallBase = true;
            /* create a context and DBSet<T>
             * wire up the IQueryable implementation for the DbSet 'CommunityPartner'
             * delegating to the LINQ to Objects provider that works with List<T>*/
            repositoryMock.Setup(x => x.CommunityPartners).Returns(setCPMock.Object); //fake db returns fake CP

            ////mock the table 'CommunityPartnerEmployee' for the db
            var setCPEMock = new Mock<DbSet<CommunityPartnerEmployee>>();
            ////mock a community partner employee to 'seed' the table for the db
            var dataCPE = new List<CommunityPartnerEmployee>
            {
                new CommunityPartnerEmployee
                { //create in-memory data
                    ID = 1,
                    CommunityPartner_ID = "test",
                    FirstName = "firstTest",
                    LastName = "lastTest",
                    Title = "Employee",
                    Email = "test@test.c",
                    Phone = "8675309",
                    IsOrganizationAdmin = true
                }
            };
            ////mock the query data, setup implementation of IQueryable
            var queryableCPE = dataCPE.AsQueryable();
            ////setup implementation of async IQuearyable
            setCPEMock.As<IDbAsyncEnumerable<CommunityPartnerEmployee>>()
                .Setup(m => m.GetAsyncEnumerator())
                .Returns(new TestDbAsyncEnumerator<CommunityPartnerEmployee>(queryableCPE.GetEnumerator()));
            setCPEMock.As<IQueryable<CommunityPartnerEmployee>>()
                .Setup(m => m.Provider)
                .Returns(new TestDbAsyncQueryProvider<CommunityPartnerEmployee>(queryableCPE.Provider)); 
            setCPEMock.As<IQueryable<CommunityPartnerEmployee>>().Setup(m => m.Expression).Returns(queryableCPE.Expression);
            setCPEMock.As<IQueryable<CommunityPartnerEmployee>>().Setup(m => m.ElementType).Returns(queryableCPE.ElementType);
            setCPEMock.As<IQueryable<CommunityPartnerEmployee>>().Setup(m => m.GetEnumerator()).Returns(queryableCPE.GetEnumerator());
            setCPEMock.CallBase = true;
            /* create a context and DBSet<T> for 'CommunityPartnerEmployee'
            * wire up the IQueryable implementation for the DbSet
            * delegating to the LINQ to Objects provider that works with List<T>*/
            repositoryMock.Setup(x => x.CommunityPartnerStaff).Returns(setCPEMock.Object);

            ////mock the controller with a mocked db and mocked CP
            var controller = new EmployeeController(repositoryMock.Object, "test");
  
          
            // ACT
            var result = await controller.Index() as ViewResult;


            // ASSERT
            Assert.AreEqual("", result.ViewName);

     
        }

        [TestMethod]
        public async Task Test_IndexViewProfileDoesNOTExist()
        {
            // ARRANGE
            ////mock the database
            var repositoryMock = new Mock<IeServeContext>(); //fake db
            
            ////mock the table 'CommunityPartner' for the db
            var setCPMock = new Mock<DbSet<CommunityPartner>>(); //.SetupData(data);
            ////mock a community partner to 'seed' the table for the db
            var dataCP = new List<CommunityPartner>
            {//create in-memory data
                new CommunityPartner
                { //CP does NOT have a profile, so does not exist in this table
                    Address = null,
                    AspNetUser_ID = null,
                    MainPhone = null,
                    MissionStatement = null,
                    OrganizationName = null,
                    Website = null,
                    WorkDescription = null
                }
            };
            ////mock the query data, setup implementation of IQueryable
            var queryableCP = dataCP.AsQueryable();
            setCPMock.As<IQueryable<CommunityPartner>>().Setup(m => m.Provider).Returns(queryableCP.Provider);
            setCPMock.As<IQueryable<CommunityPartner>>().Setup(m => m.Expression).Returns(queryableCP.Expression);
            setCPMock.As<IQueryable<CommunityPartner>>().Setup(m => m.ElementType).Returns(queryableCP.ElementType);
            setCPMock.As<IQueryable<CommunityPartner>>().Setup(m => m.GetEnumerator()).Returns(queryableCP.GetEnumerator());
            setCPMock.CallBase = true;
            /* create a context and DBSet<T> for 'CommunityPartner'
            * wire up the IQueryable implementation for the DbSet
            * delegating to the LINQ to Objects provider that works with List<T>*/
            repositoryMock.Setup(x => x.CommunityPartners).Returns(setCPMock.Object); //fake db returns fake CP
  
            ////mock the controller with a mocked db and mocked CP
            var controller = new EmployeeController(repositoryMock.Object, "test");


            //ACT
            var result = await controller.Index() as RedirectToRouteResult;
            

            //ASSERT
            Assert.AreEqual("Profile", result.RouteValues["action"]);
            Assert.AreEqual("Organization", result.RouteValues["controller"]);

        }
    }
}
