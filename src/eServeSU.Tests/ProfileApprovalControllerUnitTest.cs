﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using eServeSU;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data.SqlClient;
using eServeSU.Areas.Partner.Controllers;
using System.Web.Mvc;
using System.Threading.Tasks;
using System.Security.Principal;
using System.Security.Claims;

using Moq;
using AspnetIdentity20;
using System.Web;
using eServeSU.Models;

using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Globalization;
using System.Threading;
using eServeSU.Areas.Admin.Controllers;
using NSubstitute;
using Microsoft.Owin;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace eServeSU.Tests
{



    [TestClass]
    public class ProfileApprovalControllerUnitTest
    {
        #region Static data used for the tests

        // Sample mock data
        // ============================================================
        IEnumerable<CommunityPartner> DummyCommunityPartners = new[] {
            new CommunityPartner
            {
                Address = "123 4th St",
                AspNetUser_ID = "test",
                MainPhone = "1234567890",
                MissionStatement = "test mission statement",
                OrganizationName = "TEST",
                Website = "test.test",
                WorkDescription = "tester"
            },
            new CommunityPartner
            {
                Address = "567 8th St",
                AspNetUser_ID = "test2",
                MainPhone = "0987654321",
                MissionStatement = "mission statement test",
                OrganizationName = "TESTtest",
                Website = "testTEST.test",
                WorkDescription = "tester"
            },
            new CommunityPartner
            {
                Address = "90th St",
                AspNetUser_ID = "test3",
                MainPhone = "1029384756",
                MissionStatement = "mission statement test",
                OrganizationName = "TESTtest",
                Website = "testTEST.test",
                WorkDescription = "tester"
            }
        };

        /// <summary>
        /// Returns dummy community partners needing approval
        /// </summary>
        IEnumerable<CommunityPartnerPendingApproval> DummyCommunityPartnerPendingApprovals
        {
            get
            {
                return from cp in DummyCommunityPartners
                       select new CommunityPartnerPendingApproval {
                           AspNetUser_ID = cp.AspNetUser_ID,
                           CommunityPartner = cp
                       };

            }

        }
        #endregion

        /// <summary>
        /// Returns a substitute for a DbSet
        /// </summary>
        /// <typeparam name="TTable">Type of Table object</typeparam>
        /// <param name="dummyData">dummy data for the table</param>
        /// <returns>NSubstitute object replacing the DbSet</returns>
        private DbSet<TTable> GetDbSetSubstitute<TTable>(IEnumerable<TTable> dummyData) where TTable : class
        {

            // dummy data list
            List<TTable> dummyDataList = dummyData.ToList();

            ////mock a TTable to 'seed' the table for the db
            IQueryable<TTable> queryableCollection = dummyDataList.AsQueryable();

            ////mock the table for the db

            // YK:
            // not sure if this line is needed?

            //Mock<DbSet<TTable>> dbSetMock = new Mock<DbSet<TTable>>(); //.SetupData(data);

            DbSet<TTable> dbSetMock = Substitute.For<DbSet<TTable>, IQueryable<TTable>>();

            ((IQueryable<TTable>)dbSetMock).Provider.Returns(queryableCollection.Provider);
            ((IQueryable<TTable>)dbSetMock).Expression.Returns(queryableCollection.Expression);
            ((IQueryable<TTable>)dbSetMock).ElementType.Returns(queryableCollection.ElementType);
            ((IQueryable<TTable>)dbSetMock).GetEnumerator().Returns(queryableCollection.GetEnumerator());

            return dbSetMock;
        }


        /// <summary>
        /// Fake eServeContext
        /// </summary>
        /// <returns></returns>
        private Mock<IeServeContext> GetMockedContext()
        {
            ////mock the database
            Mock<IeServeContext> repositoryMock = new Mock<IeServeContext>();

            DbSet<CommunityPartner> setCPMock = GetDbSetSubstitute(DummyCommunityPartners);
            DbSet<CommunityPartnerPendingApproval> setCPPAMock = GetDbSetSubstitute(DummyCommunityPartnerPendingApprovals);

            // include statements with any expression are bypassed
            setCPPAMock.Include(x => x.CommunityPartner).ReturnsForAnyArgs(setCPPAMock);

            /* create a context and DBSet<T>
             * wire up the IQueryable implementation for the DbSet 'CommunityPartner'
             * delegating to the LINQ to Objects provider that works with List<T>*/

            repositoryMock.Setup(x => x.CommunityPartners).Returns(setCPMock); 
            repositoryMock.Setup(x => x.CommunityPartnerPendingApprovals).Returns(setCPPAMock); 

            return repositoryMock;
        }


        [TestMethod]
        public void Test_Index_View_Pending_Profiles_Exist()
        {

            // Note: How to ignore the include statements
            // http://stackoverflow.com/a/25551596/6318478

            // ARRANGE          

            Mock<IeServeContext> context = GetMockedContext();
            ProfileApprovalController controller = new ProfileApprovalController(context.Object);

            // ACT
            ViewResult result = controller.Index() as ViewResult;

            // ASSERT

            // view model should be a list of community partners
            IList<CommunityPartner> resultModel = result.Model as IList<CommunityPartner>;
            Assert.IsNotNull(resultModel);

            // view should have 3 items in the table
            Assert.AreEqual(resultModel.Count, 3);

            // view item #3 should be "test3"
            Assert.AreEqual(resultModel.Skip(2).First().AspNetUser_ID, "test3");
        }

        [TestMethod]
        public void Test_DetailsModal_param_isNull()
        {

            // ARRANGE              

            Mock<IeServeContext> repositoryMock = new Mock<IeServeContext>();
            DbSet<CommunityPartner> setCPMock = GetDbSetSubstitute(DummyCommunityPartners);
            repositoryMock.Setup(x => x.CommunityPartners).Returns(setCPMock);

            ProfileApprovalController controller = new ProfileApprovalController(repositoryMock.Object);

            //ACT

            var actionResult = controller.DetailsModal(null) as HttpStatusCodeResult;


            //ASSERT

            Assert.AreEqual(400, actionResult.StatusCode);
        }
        
        [TestMethod]
        public void Test_DetailsModal_CommunityPartner_isNull()
        {

            // ARRANGE              
            // - mock database
            Mock<IeServeContext> repositoryMock = new Mock<IeServeContext>();
            DbSet<CommunityPartner> setCPMock = GetDbSetSubstitute(DummyCommunityPartners);
            repositoryMock.Setup(x => x.CommunityPartners).Returns(setCPMock);

            // ARRANGE - create controller
            ProfileApprovalController controller = new ProfileApprovalController(repositoryMock.Object);

            //ACT
            var actionResult = controller.DetailsModal("NoTest") as HttpNotFoundResult;
            

            //ASSERT
            //view returns error 404   
            Assert.AreEqual(404, actionResult.StatusCode);
        }

        [TestMethod]
        public void Test_DetailsModal_ViewBag_UserName_Value()
        {


            // ARRANGE                        
            // - mock database
            Mock<IeServeContext> repositoryMock = new Mock<IeServeContext>();
            DbSet<CommunityPartner> setCPMock = GetDbSetSubstitute(DummyCommunityPartners);
            repositoryMock.Setup(x => x.CommunityPartners).Returns(setCPMock);             
            string paramID = "test2";

            // ARRANGE = mock UserManager
            var dummyUser = new ApplicationUser() { UserName = "test2", Email = "foo@bar.baz" };
            var mockStore = new Mock<UserStore<ApplicationUser>>();
            var userManager = new ApplicationUserManager(mockStore.Object);
            mockStore.Setup(x => x.CreateAsync(dummyUser))
                        .Returns(Task.FromResult(IdentityResult.Success));
            mockStore.Setup(x => x.FindByIdAsync(dummyUser.UserName))
                        .Returns(Task.FromResult(dummyUser));

            // ARRANGE - create controller
            ProfileApprovalController controller = new ProfileApprovalController(repositoryMock.Object, userManager);


            //ACT
            ViewResult actionResult = controller.DetailsModal(paramID) as ViewResult;


            //ASSERT

            // view ViewBag.PartnerLoginUserName should return "test2"
            var viewBagResult = actionResult.ViewBag.PartnerLoginUserName;
            Assert.AreEqual(paramID, viewBagResult);
        }
    }
}
