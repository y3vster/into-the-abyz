﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using eServeSU.Models;
using eServeSU.Models.RegistrationStates;

using Microsoft.AspNet.Identity;

using NSubstitute;
using Moq;

namespace eServeSU.Tests
{
    /// <summary>
    /// Summary description for RegistrationStateTests
    /// </summary>
    [TestClass]
    public class RegistrationStateTests
    {
        public RegistrationStateTests()
        {
            //
            // TODO: Add constructor logic here
            //
        }


        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        private IeServeContext _context;
        private DbSet<CommunityPartnerPendingApproval> _pendingApprovalTable;
        private IUserStore<ApplicationUser> _userStore; 
        private ApplicationUserManager _userManager;

        private void SetUp()
        {
            // db context
            _context = Substitute.For<IeServeContext>();
            _pendingApprovalTable = Substitute.For<DbSet<CommunityPartnerPendingApproval>>();
            _context.CommunityPartnerPendingApprovals.Returns(_pendingApprovalTable);

            // Identity management objects
            _userStore = Substitute.For<IUserStore<ApplicationUser>>();
            _userManager = Substitute.For<ApplicationUserManager>(_userStore);

        }
        #region State_New Unit Tests
        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Registration_State_New_StateNew_isThrown()
        {
            // ARRANGE
            // ------------------------------------------------------------
            SetUp();

            // create new state object
            RegistrationStateNew newState = new RegistrationStateNew(_userManager, _context, "dummy");

            // ACT
            // ------------------------------------------------------------
            
            // ASSERT
            // ------------------------------------------------------------
            newState.StateNew();
        }
        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Registration_State_New_StateRejected_isThrown()
        {
            // ARRANGE
            // ------------------------------------------------------------
            SetUp();

            // create new state object
            RegistrationStateNew newState = new RegistrationStateNew(_userManager, _context, "dummy");

            // ACT
            // ------------------------------------------------------------

            // ASSERT
            // ------------------------------------------------------------
            newState.StateRejected();
        }
        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Registration_State_New_StateApproved_isThrown()
        {
            // ARRANGE
            // ------------------------------------------------------------
            SetUp();

            // create new state object
            RegistrationStateNew newState = new RegistrationStateNew(_userManager, _context, "dummy");

            // ACT
            // ------------------------------------------------------------

            // ASSERT
            // ------------------------------------------------------------
            newState.StateApproved();
        }
        [TestMethod]
        public void Registration_State_New_State_Adds_Record_To_Pending_Table()
        {
            // ARRANGE
            // ------------------------------------------------------------
            SetUp();

            // create new state object
            RegistrationStateNew newState = new RegistrationStateNew(_userManager, _context, "unique_id_12");

            // ACT
            // ------------------------------------------------------------

            newState.StatePending();

            // ASSERT
            // ------------------------------------------------------------
            _pendingApprovalTable.Received()
                .Add(Arg.Is<CommunityPartnerPendingApproval>(cppa => cppa.AspNetUser_ID == "unique_id_12"));
        }

        [TestMethod]
        public void Registration_State_New_State_Pending_Adds_Role_To_User()
        {
            // ARRANGE
            // ------------------------------------------------------------

            // dummy user
            string userID = "test_user_id1";

            SetUp();

            // create new state object
            RegistrationStateNew newState = new RegistrationStateNew(_userManager, _context, userID);

            // ACT
            // ------------------------------------------------------------
            newState.StatePending();

            // ASSERT
            // ------------------------------------------------------------
            _userManager.Received().AddToRole(userID, "PartnerPending");
        }
        #endregion

        #region State_Pending Unit Tests
        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Registration_State_Pending_StateNew_isThrown()
        {
            // ARRANGE
            // ------------------------------------------------------------
            SetUp();

            // create new state object
            RegistrationStatePending PendingState = new RegistrationStatePending(_userManager, _context, "dummy");

            // ACT
            // ------------------------------------------------------------

            // ASSERT
            // ------------------------------------------------------------
            PendingState.StateNew();
        }
        [TestMethod]
        public void Registration_State_Pending_State_Approved_Removes_Record_from_Pending_Table()
        {
            // ARRANGE
            // ------------------------------------------------------------
            SetUp();

            // create new state object
            RegistrationStatePending PendingState = new RegistrationStatePending(_userManager, _context, "I'm getting approved");

            // ACT
            // ------------------------------------------------------------

            PendingState.StatePending();

            // ASSERT
            // ------------------------------------------------------------
            _pendingApprovalTable.DidNotReceive()
                .Add(Arg.Is<CommunityPartnerPendingApproval>(cppa => cppa.AspNetUser_ID == "I'm getting approved"));
        }
        [TestMethod]
        public void Registration_State_Pending_State_Rejected_Removes_Record_from_Pending_Table()
        {
            // ARRANGE
            // ------------------------------------------------------------
            SetUp();

            // create new state object
            RegistrationStatePending PendingState = new RegistrationStatePending(_userManager, _context, "I'm not getting approved");

            // ACT
            // ------------------------------------------------------------

            PendingState.StatePending();

            // ASSERT
            // ------------------------------------------------------------
            _pendingApprovalTable.DidNotReceive()
                .Add(Arg.Is<CommunityPartnerPendingApproval>(cppa => cppa.AspNetUser_ID == "I'm not getting approved"));
        }
        #endregion

        #region State_Approved
        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Registration_State_Approved_StateNew_isThrown()
        {
            // ARRANGE
            // ------------------------------------------------------------
            SetUp();

            // create new state object
            RegistrationStateApproved ApprovedState = new RegistrationStateApproved(_userManager, _context, "dummy");

            // ACT
            // ------------------------------------------------------------

            // ASSERT
            // ------------------------------------------------------------
            ApprovedState.StateNew();
        }
        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Registration_State_Approved_StateRejected_isThrown()
        {
            // ARRANGE
            // ------------------------------------------------------------
            SetUp();

            // create new state object
            RegistrationStateApproved ApprovedState = new RegistrationStateApproved(_userManager, _context, "dummy");

            // ACT
            // ------------------------------------------------------------

            // ASSERT
            // ------------------------------------------------------------
            ApprovedState.StateRejected();
        }
        #endregion

        #region State_Rejected
        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Registration_State_Rejected_StateNew_isThrown()
        {
            // ARRANGE
            // ------------------------------------------------------------
            SetUp();

            // create new state object
            RegistrationStateRejected RejectedState = new RegistrationStateRejected(_userManager, _context, "dummy");

            // ACT
            // ------------------------------------------------------------

            // ASSERT
            // ------------------------------------------------------------
            RejectedState.StateNew();
        }
        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Registration_State_Rejected_StateApproved_isThrown()
        {
            // ARRANGE
            // ------------------------------------------------------------
            SetUp();

            // create new state object
            RegistrationStateRejected RejectedState = new RegistrationStateRejected(_userManager, _context, "dummy");

            // ACT
            // ------------------------------------------------------------

            // ASSERT
            // ------------------------------------------------------------
            RejectedState.StateApproved();
        }
        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Registration_State_Rejected_StateRejected_isThrown()
        {
            // ARRANGE
            // ------------------------------------------------------------
            SetUp();

            // create new state object
            RegistrationStateRejected RejectedState = new RegistrationStateRejected(_userManager, _context, "dummy");

            // ACT
            // ------------------------------------------------------------

            // ASSERT
            // ------------------------------------------------------------
            RejectedState.StateRejected();
        }
        [TestMethod]
        public void Registration_State_Rejected_State_Adds_Record_To_Pending_Table()
        {
            // ARRANGE
            // ------------------------------------------------------------
            SetUp();

            // create new state object
            RegistrationStateRejected newState = new RegistrationStateRejected(_userManager, _context, "Fancy McNoPants");

            // ACT
            // ------------------------------------------------------------

            newState.StatePending();

            // ASSERT
            // ------------------------------------------------------------
            _pendingApprovalTable.Received()
                .Add(Arg.Is<CommunityPartnerPendingApproval>(cppa => cppa.AspNetUser_ID == "Fancy McNoPants"));
        }
        #endregion

    }
}
