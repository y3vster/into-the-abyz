using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using eServeSU;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data.SqlClient;
using eServeSU.Areas.Partner.Controllers;
using System.Web.Mvc;
using System.Threading.Tasks;
using Moq;
using eServeSU.Models;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;


namespace eServeSU.Tests
{
    [TestClass]
    public class CommunityPartnerUnitTest
    {
        public TestContext TestContext { get; set; }
        public string ConnectionString = ConfigurationManager.ConnectionStrings["eServeConnection"].ConnectionString;

        [TestMethod]
        [TestCategory("CommunityPartner")]
        [Description("")]
        public void Test_GetAllCommunityPartner()
        {
            //Initialize SqlQueryHelper object
            var sqlConnection = new SqlConnection(ConnectionString);
            sqlConnection.Open();
          
            var command = new SqlCommand("select count(*) from CommunityPartners");
            command.Connection = sqlConnection;
            var cpCount = Convert.ToInt32(command.ExecuteScalar());

            //CommunityPartner
            
            l_CommunityPartner cp = new l_CommunityPartner();

            List<l_CommunityPartner> cpList = cp.GetAllCommunityPartner();
            
            Assert.AreEqual(cpCount, cpList.Count);
        }


    }
}
