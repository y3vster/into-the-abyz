﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eServeSU;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace eServeSU.Tests
{
    [TestClass]
    public class StudentUnitTest
    {
        public TestContext TestContext { get; set; }
        public string ConnectionString = ConfigurationManager.ConnectionStrings["eServeConnection"].ConnectionString;

        [TestMethod]
        [TestCategory("Student")]
        [Description("")]
        public void Test_GetAllRegisteredOpportunity()
        {
            //Initialize SqlQueryHelper object
            var sqlConnection = new SqlConnection(ConnectionString);
            sqlConnection.Open();

            var command = new SqlCommand("select count(distinct [OpportunityID]) from [dbo].[SignUpFor] where studentid = 106288");
            command.Connection = sqlConnection;
            var oppRegisteredCount = Convert.ToInt32(command.ExecuteScalar());

            //l_Opportunity

            l_OpportunityRegistered oppRegistered = new l_OpportunityRegistered();

            List<l_OpportunityRegistered> oppRegisteredList = oppRegistered.GetOpportunityRegisteredByStudentId(106288);

            Assert.AreEqual(oppRegisteredCount, oppRegisteredList.Count);
        }
    }
}
