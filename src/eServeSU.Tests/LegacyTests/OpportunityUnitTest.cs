﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eServeSU;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace eServeSU.Tests
{
    [TestClass]
    public class OpportunityUnitTest
    {
        public TestContext TestContext { get; set; }
        public string ConnectionString = ConfigurationManager.ConnectionStrings["eServeConnection"].ConnectionString;

        [TestMethod]
        [TestCategory("l_Opportunity")]
        [Description("")]
        public void Test_GetAllOpportunity()
        {
            //Initialize SqlQueryHelper object
            var sqlConnection = new SqlConnection(ConnectionString);
            sqlConnection.Open();

            var command = new SqlCommand("select count(*) from opportunity");
            command.Connection = sqlConnection;
            var oppCount = Convert.ToInt32(command.ExecuteScalar());

            //l_Opportunity
            l_Opportunity opp = new l_Opportunity();
            List<l_Opportunity> oppList = opp.GetAllOpportunities();
            Assert.AreEqual(oppCount, oppList.Count);
        }
    }
}
