USE [master]
GO
/****** Object:  Database [eServe]    Script Date: 5/25/2016 5:36:18 PM ******/
CREATE DATABASE [eServe]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'eServe.mdf', FILENAME = N'C:\Users\Yevgeni\Desktop\CPSC 5051 Soft Eng\SiteVersion2\eServeSU\App_Data\eServe.mdf' , SIZE = 4288KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'eServe_log.ldf', FILENAME = N'C:\Users\Yevgeni\Desktop\CPSC 5051 Soft Eng\SiteVersion2\eServeSU\App_Data\eServe_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [eServe] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [eServe].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [eServe] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [eServe] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [eServe] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [eServe] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [eServe] SET ARITHABORT OFF 
GO
ALTER DATABASE [eServe] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [eServe] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [eServe] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [eServe] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [eServe] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [eServe] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [eServe] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [eServe] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [eServe] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [eServe] SET  ENABLE_BROKER 
GO
ALTER DATABASE [eServe] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [eServe] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [eServe] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [eServe] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [eServe] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [eServe] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [eServe] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [eServe] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [eServe] SET  MULTI_USER 
GO
ALTER DATABASE [eServe] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [eServe] SET DB_CHAINING OFF 
GO
ALTER DATABASE [eServe] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [eServe] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [eServe] SET DELAYED_DURABILITY = DISABLED 
GO
USE [eServe]
GO
/****** Object:  User [eserve2]    Script Date: 5/25/2016 5:36:18 PM ******/
CREATE USER [eserve2] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [eserve2]
GO
/****** Object:  Schema [eserve]    Script Date: 5/25/2016 5:36:18 PM ******/
CREATE SCHEMA [eserve]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_diagramobjects]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

	CREATE FUNCTION [dbo].[fn_diagramobjects]() 
	RETURNS int
	WITH EXECUTE AS N'dbo'
	AS
	BEGIN
		declare @id_upgraddiagrams		int
		declare @id_sysdiagrams			int
		declare @id_helpdiagrams		int
		declare @id_helpdiagramdefinition	int
		declare @id_creatediagram	int
		declare @id_renamediagram	int
		declare @id_alterdiagram 	int 
		declare @id_dropdiagram		int
		declare @InstalledObjects	int

		select @InstalledObjects = 0

		select 	@id_upgraddiagrams = object_id(N'dbo.sp_upgraddiagrams'),
			@id_sysdiagrams = object_id(N'dbo.sysdiagrams'),
			@id_helpdiagrams = object_id(N'dbo.sp_helpdiagrams'),
			@id_helpdiagramdefinition = object_id(N'dbo.sp_helpdiagramdefinition'),
			@id_creatediagram = object_id(N'dbo.sp_creatediagram'),
			@id_renamediagram = object_id(N'dbo.sp_renamediagram'),
			@id_alterdiagram = object_id(N'dbo.sp_alterdiagram'), 
			@id_dropdiagram = object_id(N'dbo.sp_dropdiagram')

		if @id_upgraddiagrams is not null
			select @InstalledObjects = @InstalledObjects + 1
		if @id_sysdiagrams is not null
			select @InstalledObjects = @InstalledObjects + 2
		if @id_helpdiagrams is not null
			select @InstalledObjects = @InstalledObjects + 4
		if @id_helpdiagramdefinition is not null
			select @InstalledObjects = @InstalledObjects + 8
		if @id_creatediagram is not null
			select @InstalledObjects = @InstalledObjects + 16
		if @id_renamediagram is not null
			select @InstalledObjects = @InstalledObjects + 32
		if @id_alterdiagram  is not null
			select @InstalledObjects = @InstalledObjects + 64
		if @id_dropdiagram is not null
			select @InstalledObjects = @InstalledObjects + 128
		
		return @InstalledObjects 
	END
	

GO
/****** Object:  UserDefinedFunction [dbo].[fnSplitString]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fnSplitString] 
( 
    @string NVARCHAR(MAX), 
    @delimiter CHAR(1) 
) 
RETURNS @output TABLE(splitdata NVARCHAR(MAX) 
) 
/**************************************************************************
This function is to split the string with the specific delimiter to a table
For Example: 12;34;56;67;78;123;456, this will split to multi records below
12
34
56
67
78
123
456 
***************************************************************************/
BEGIN 
    DECLARE @start INT, @end INT 
    SELECT @start = 1, @end = CHARINDEX(@delimiter, @string) 
    WHILE @start < LEN(@string) + 1 BEGIN 
        IF @end = 0  
            SET @end = LEN(@string) + 1
       
        INSERT INTO @output (splitdata)  
        VALUES(SUBSTRING(@string, @start, @end - @start)) 
        SET @start = @end + 1 
        SET @end = CHARINDEX(@delimiter, @string, @start)
        
    END 
    RETURN 
END




GO
/****** Object:  Table [dbo].[__MigrationHistory]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[__MigrationHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ContextKey] [nvarchar](300) NOT NULL,
	[Model] [varbinary](max) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC,
	[ContextKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[13FQ]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[13FQ](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Term] [varchar](50) NULL,
	[Course Section Name] [varchar](50) NULL,
	[''Course Section Title''] [varchar](50) NULL,
	[Course Section ID] [varchar](50) NULL,
	[Course Name] [varchar](50) NULL,
	[Core Section?] [varchar](50) NULL,
	[Minimum Credits] [varchar](50) NULL,
	[Maximum Credits] [varchar](50) NULL,
	[Has Cross Listed Sections?] [varchar](50) NULL,
	[Acad Level Code] [varchar](50) NULL,
	[Acad Level Description] [varchar](50) NULL,
	[Department Code] [varchar](50) NULL,
	[''Department Description''] [varchar](50) NULL,
	[School Code] [varchar](50) NULL,
	[''School Description''] [varchar](50) NULL,
	[First Faculty ID] [varchar](50) NULL,
	[''First Faculty First Name''] [varchar](50) NULL,
	[''First Faculty Last Name''] [varchar](50) NULL,
	[First Faculty Email] [varchar](50) NULL,
	[Instructional Method Code] [varchar](50) NULL,
	[Instructional Method Description] [varchar](50) NULL,
	[''CORE Code''] [varchar](50) NULL,
	[Student Registrations] [varchar](50) NULL,
	[Credit Hours Attempted] [varchar](50) NULL,
	[Credit Hours Earned] [varchar](50) NULL,
	[Column 25] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Admin]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Admin](
	[LastName] [varchar](15) NOT NULL,
	[FirstName] [varchar](15) NOT NULL,
	[Email] [varchar](50) NOT NULL,
	[Password] [varchar](10) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[LastName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AdminAlert]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AdminAlert](
	[AlertID] [int] IDENTITY(1,1) NOT NULL,
	[Message] [nvarchar](500) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[AlertID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](128) NOT NULL,
	[RoleId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEndDateUtc] [datetime] NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Class]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Class](
	[CourseID] [int] IDENTITY(1,1) NOT NULL,
	[ShortName] [int] NOT NULL,
	[CourseName] [nvarchar](50) NULL,
 CONSTRAINT [PK_CourseID] PRIMARY KEY CLUSTERED 
(
	[CourseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CommunityPartnerAlert]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CommunityPartnerAlert](
	[AlertID] [int] IDENTITY(1,1) NOT NULL,
	[Message] [nvarchar](500) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[AlertID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CommunityPartners]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CommunityPartners](
	[CPID] [int] IDENTITY(1,1) NOT NULL,
	[OrganizationName] [nvarchar](150) NOT NULL,
	[Address] [nvarchar](500) NOT NULL,
	[Website] [nvarchar](500) NOT NULL,
	[MainPhone] [nvarchar](10) NOT NULL,
	[MissionStatement] [nvarchar](50) NULL,
	[WorkDescription] [nvarchar](500) NULL,
 CONSTRAINT [PK_CPID] PRIMARY KEY CLUSTERED 
(
	[CPID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CommunityPartnersPeople]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CommunityPartnersPeople](
	[CPPID] [int] IDENTITY(1,1) NOT NULL,
	[CPID] [int] NOT NULL,
	[FirstName] [nvarchar](15) NOT NULL,
	[LastName] [nvarchar](15) NOT NULL,
	[Title] [nvarchar](15) NOT NULL,
	[Phone] [nvarchar](13) NOT NULL,
	[EmailID] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](10) NULL,
 CONSTRAINT [PK_CPPID] PRIMARY KEY CLUSTERED 
(
	[CPPID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Ethinicity]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ethinicity](
	[EthinicityID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[EthinicityID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FocusArea]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FocusArea](
	[FocusAreaID] [int] IDENTITY(1,1) NOT NULL,
	[AreaName] [varchar](200) NOT NULL,
 CONSTRAINT [PK_FocusAreaID] PRIMARY KEY CLUSTERED 
(
	[FocusAreaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Opportunity]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Opportunity](
	[OpportunityID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Location] [nvarchar](50) NOT NULL,
	[DateOfCreation] [date] NOT NULL,
	[JobDescription] [varchar](max) NOT NULL,
	[Requirements] [varchar](1000) NOT NULL,
	[TimeCommittment] [varchar](9) NOT NULL,
	[TotalNumberOfSlots] [int] NOT NULL,
	[OrientationDate] [datetime2](7) NOT NULL,
	[Status] [varchar](50) NOT NULL CONSTRAINT [DF_Status]  DEFAULT ('Pending'),
	[ResumeRequired] [varchar](3) NOT NULL,
	[MinimumAge] [varchar](8) NOT NULL,
	[CRCRequiredByPartner] [varchar](3) NOT NULL,
	[CRCRequiredBySU] [varchar](3) NOT NULL,
	[LinkToOnlineApp] [varchar](200) NOT NULL,
	[SupervisorEmail] [nvarchar](50) NOT NULL,
	[TypeID] [int] NOT NULL,
	[CPID] [int] NOT NULL,
	[QuarterID] [int] NOT NULL,
	[CPPID] [int] NOT NULL,
	[JobHours] [varchar](20) NOT NULL,
	[DistanceFromSU] [varchar](8) NULL,
 CONSTRAINT [PK_OpportunityID] PRIMARY KEY CLUSTERED 
(
	[OpportunityID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Opportunity_FocusArea]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Opportunity_FocusArea](
	[OpportunityID] [int] NOT NULL,
	[FocusAreaID] [int] NOT NULL,
 CONSTRAINT [PK_OpportunityFocusArea] PRIMARY KEY CLUSTERED 
(
	[OpportunityID] ASC,
	[FocusAreaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Opportunity_Section]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Opportunity_Section](
	[OpportunityID] [int] NOT NULL,
	[SectionID] [int] NOT NULL,
 CONSTRAINT [PK_SectionOpportunity] PRIMARY KEY CLUSTERED 
(
	[OpportunityID] ASC,
	[SectionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[OpportunityType]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OpportunityType](
	[TypeID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_TypeID] PRIMARY KEY CLUSTERED 
(
	[TypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Professor]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Professor](
	[ProfessorID] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](15) NOT NULL,
	[LastName] [varchar](15) NOT NULL,
	[EmailID] [varchar](50) NOT NULL,
	[Phone] [varchar](12) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ProfessorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Quarter]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Quarter](
	[QuarterID] [int] IDENTITY(1,1) NOT NULL,
	[QuarterName] [nvarchar](15) NOT NULL,
	[ShortName] [nvarchar](10) NOT NULL,
	[StartDate] [date] NOT NULL,
	[EndDate] [date] NOT NULL,
 CONSTRAINT [PK_QuarterID] PRIMARY KEY CLUSTERED 
(
	[QuarterID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Section]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Section](
	[SectionID] [int] IDENTITY(1,1) NOT NULL,
	[CourseID] [int] NOT NULL,
	[ProfessorID] [int] NOT NULL,
	[QuarterID] [int] NOT NULL,
	[RoomNumber] [nvarchar](70) NOT NULL,
	[ClassHours] [varchar](20) NOT NULL,
	[NumberOfSlots] [int] NOT NULL,
	[SectionName] [varchar](50) NULL,
 CONSTRAINT [PK_SectionID] PRIMARY KEY CLUSTERED 
(
	[SectionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SignUpFor]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SignUpFor](
	[StudentID] [int] NOT NULL,
	[CPPID] [int] NULL,
	[OpportunityID] [int] NOT NULL,
	[SignUpStatus] [nvarchar](10) NOT NULL,
	[StudentReflection] [nvarchar](max) NULL,
	[PartnerEvaluation] [nvarchar](max) NULL,
	[StudentEvaluation] [nvarchar](max) NULL,
 CONSTRAINT [PK_SignUpID] PRIMARY KEY CLUSTERED 
(
	[OpportunityID] ASC,
	[StudentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Student]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Student](
	[StudentID] [int] NOT NULL,
	[DateOfBirth] [date] NOT NULL,
	[FirstName] [nvarchar](15) NOT NULL,
	[LastName] [nvarchar](15) NOT NULL,
	[PreferedName] [nvarchar](10) NULL,
	[EmailID] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](10) NOT NULL,
	[Gender] [nvarchar](25) NULL CONSTRAINT [DF_Student_Gender]  DEFAULT ('Prefer not to answer'),
	[InternationalStudent] [nvarchar](3) NOT NULL,
	[LastBackgroundCheck] [date] NULL,
 CONSTRAINT [PK_StudentID] PRIMARY KEY CLUSTERED 
(
	[StudentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Student_Ethinicity]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Student_Ethinicity](
	[StudentID] [int] NOT NULL,
	[EthinicityID] [int] NOT NULL,
 CONSTRAINT [PK_StudentEthinicity] PRIMARY KEY CLUSTERED 
(
	[StudentID] ASC,
	[EthinicityID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Student_FocusArea]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Student_FocusArea](
	[StudentID] [int] NOT NULL,
	[FocusAreaID] [int] NOT NULL,
 CONSTRAINT [PK_StudentFocus] PRIMARY KEY CLUSTERED 
(
	[StudentID] ASC,
	[FocusAreaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Student_Section]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Student_Section](
	[StudentID] [int] NOT NULL,
	[SectionID] [int] NOT NULL,
 CONSTRAINT [PK_StudentSection] PRIMARY KEY CLUSTERED 
(
	[StudentID] ASC,
	[SectionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[StudentAlert]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StudentAlert](
	[AlertID] [int] IDENTITY(1,1) NOT NULL,
	[Message] [nvarchar](500) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[AlertID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TimeEntries]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TimeEntries](
	[Date] [date] NOT NULL,
	[OpportunityID] [int] NOT NULL,
	[StudentID] [int] NOT NULL,
	[CPPID] [int] NOT NULL,
	[PartnerApprovedHours] [int] NULL,
	[WorkDateEntry] [date] NOT NULL,
	[HoursVolunteered] [int] NOT NULL,
 CONSTRAINT [PK_TimeEntries] PRIMARY KEY CLUSTERED 
(
	[Date] ASC,
	[OpportunityID] ASC,
	[StudentID] ASC,
	[CPPID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [eserve].[__MigrationHistory]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [eserve].[__MigrationHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ContextKey] [nvarchar](300) NOT NULL,
	[Model] [varbinary](max) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_eserve.__MigrationHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC,
	[ContextKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [eserve].[CommunityPartners]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [eserve].[CommunityPartners](
	[AspNetUser_ID] [nvarchar](128) NOT NULL,
	[OrganizationName] [nvarchar](150) NOT NULL,
	[Address] [nvarchar](300) NOT NULL,
	[Website] [nvarchar](128) NULL,
	[MainPhone] [nvarchar](16) NOT NULL,
	[MissionStatement] [nvarchar](50) NULL,
	[WorkDescription] [nvarchar](500) NULL,
 CONSTRAINT [PK_eserve.CommunityPartners] PRIMARY KEY CLUSTERED 
(
	[AspNetUser_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [eserve].[CommunityPartnerStaff]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [eserve].[CommunityPartnerStaff](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CommunityPartner_ID] [nvarchar](128) NOT NULL,
	[FirstName] [nvarchar](15) NOT NULL,
	[LastName] [nvarchar](15) NOT NULL,
	[Title] [nvarchar](15) NOT NULL,
	[Email] [nvarchar](100) NOT NULL,
	[Phone] [nvarchar](13) NULL,
	[IsOrganizationAdmin] [bit] NOT NULL,
 CONSTRAINT [PK_eserve.CommunityPartnerStaff] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [eserve].[Ethinicities]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [eserve].[Ethinicities](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_eserve.Ethinicities] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [eserve].[Map_Opportunities_FocusAreas]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [eserve].[Map_Opportunities_FocusAreas](
	[FocusArea_ID] [int] NOT NULL,
	[Opportunity_ID] [int] NOT NULL,
 CONSTRAINT [PK_eserve.Map_Opportunities_FocusAreas] PRIMARY KEY CLUSTERED 
(
	[FocusArea_ID] ASC,
	[Opportunity_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [eserve].[Map_Opportunities_Students]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [eserve].[Map_Opportunities_Students](
	[Opportunity_ID] [int] NOT NULL,
	[Student_ID] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_eserve.Map_Opportunities_Students] PRIMARY KEY CLUSTERED 
(
	[Opportunity_ID] ASC,
	[Student_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [eserve].[Map_Opportunities_UnivCourseSections]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [eserve].[Map_Opportunities_UnivCourseSections](
	[OpportunityID] [int] NOT NULL,
	[SectionID] [int] NOT NULL,
 CONSTRAINT [PK_eserve.Map_Opportunities_UnivCourseSections] PRIMARY KEY CLUSTERED 
(
	[OpportunityID] ASC,
	[SectionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [eserve].[Map_Students_Ethinicities]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [eserve].[Map_Students_Ethinicities](
	[Ethinicity_ID] [int] NOT NULL,
	[Student_ID] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_eserve.Map_Students_Ethinicities] PRIMARY KEY CLUSTERED 
(
	[Ethinicity_ID] ASC,
	[Student_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [eserve].[Map_Students_OppFocusAreas]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [eserve].[Map_Students_OppFocusAreas](
	[FocusArea_ID] [int] NOT NULL,
	[Student_ID] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_eserve.Map_Students_OppFocusAreas] PRIMARY KEY CLUSTERED 
(
	[FocusArea_ID] ASC,
	[Student_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [eserve].[Map_Students_OppTypes]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [eserve].[Map_Students_OppTypes](
	[Opportunity_ID] [int] NOT NULL,
	[Student_id] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_eserve.Map_Students_OppTypes] PRIMARY KEY CLUSTERED 
(
	[Opportunity_ID] ASC,
	[Student_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [eserve].[Map_Students_UnivCourseSections]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [eserve].[Map_Students_UnivCourseSections](
	[StudentID] [int] NOT NULL,
	[SectionID] [int] NOT NULL,
 CONSTRAINT [PK_eserve.Map_Students_UnivCourseSections] PRIMARY KEY CLUSTERED 
(
	[StudentID] ASC,
	[SectionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [eserve].[Opp_FocusArea]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [eserve].[Opp_FocusArea](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AreaName] [varchar](200) NOT NULL,
 CONSTRAINT [PK_eserve.Opp_FocusArea] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [eserve].[Opp_Type]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [eserve].[Opp_Type](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_eserve.Opp_Type] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [eserve].[Opportunities]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [eserve].[Opportunities](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Type_ID] [int] NOT NULL,
	[CommunityPartner_ID] [nvarchar](128) NOT NULL,
	[CommPartnerStaff_ID] [int] NOT NULL,
	[DateOfCreation] [date] NOT NULL,
	[Quarter_ID] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Location] [nvarchar](50) NOT NULL,
	[JobDescription] [varchar](1000) NOT NULL,
	[Requirements] [varchar](1000) NOT NULL,
	[TimeCommittment] [varchar](50) NULL,
	[TotalNumberOfSlots] [int] NULL,
	[OrientationDate] [date] NULL,
	[Status] [varchar](50) NOT NULL,
	[ResumeRequired] [bit] NOT NULL,
	[MinimumAge] [int] NOT NULL,
	[CRCRequiredByPartner] [bit] NULL,
	[CRCRequiredBySU] [bit] NULL,
	[LinkToOnlineApp] [varchar](200) NULL,
	[JobHours] [int] NOT NULL,
	[DistanceFromSU] [float] NOT NULL,
 CONSTRAINT [PK_eserve.Opportunities] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [eserve].[Opportunity_Student_Records]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [eserve].[Opportunity_Student_Records](
	[Opportunity_ID] [int] NOT NULL,
	[Student_id] [nvarchar](128) NOT NULL,
	[SignUpStatus] [nvarchar](10) NOT NULL,
	[StudentReflection] [nvarchar](max) NULL,
	[PartnerEvaluation] [nvarchar](max) NULL,
	[StudentEvaluation] [nvarchar](max) NULL,
 CONSTRAINT [PK_eserve.Opportunity_Student_Records] PRIMARY KEY CLUSTERED 
(
	[Opportunity_ID] ASC,
	[Student_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [eserve].[Opportunity_Student_TimeEntries]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [eserve].[Opportunity_Student_TimeEntries](
	[ID] [int] NOT NULL,
	[Opportunity_ID] [int] NOT NULL,
	[Student_ID] [nvarchar](128) NOT NULL,
	[Date] [date] NOT NULL,
	[PartnerApprovedHours] [int] NULL,
	[WorkDateEntry] [date] NOT NULL,
	[HoursVolunteered] [int] NOT NULL,
 CONSTRAINT [PK_eserve.Opportunity_Student_TimeEntries] PRIMARY KEY CLUSTERED 
(
	[ID] ASC,
	[Opportunity_ID] ASC,
	[Student_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [eserve].[Students]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [eserve].[Students](
	[AspNetUser_ID] [nvarchar](128) NOT NULL,
	[DateOfBirth] [date] NOT NULL,
	[FirstName] [nvarchar](15) NOT NULL,
	[LastName] [nvarchar](15) NOT NULL,
	[PreferedName] [nvarchar](10) NULL,
	[Gender] [nvarchar](25) NULL,
	[InternationalStudent] [bit] NOT NULL,
	[LastBackgroundCheck] [date] NULL,
 CONSTRAINT [PK_eserve.Students] PRIMARY KEY CLUSTERED 
(
	[AspNetUser_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [eserve].[Univ_Courses]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [eserve].[Univ_Courses](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Quarter_ID] [int] NOT NULL,
	[ShortName] [int] NOT NULL,
	[CourseName] [nvarchar](50) NULL,
 CONSTRAINT [PK_eserve.Univ_Courses] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [eserve].[Univ_CourseSections]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [eserve].[Univ_CourseSections](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Course_ID] [int] NOT NULL,
	[Professor_ID] [int] NOT NULL,
	[RoomNumber] [nvarchar](70) NOT NULL,
	[ClassHours] [varchar](20) NOT NULL,
	[NumberOfSlots] [int] NOT NULL,
	[SectionName] [varchar](50) NULL,
 CONSTRAINT [PK_eserve.Univ_CourseSections] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [eserve].[Univ_Professors]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [eserve].[Univ_Professors](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](15) NOT NULL,
	[LastName] [varchar](15) NOT NULL,
	[EmailID] [varchar](50) NOT NULL,
	[Phone] [varchar](12) NOT NULL,
 CONSTRAINT [PK_eserve.Univ_Professors] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [eserve].[Univ_Quarter]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [eserve].[Univ_Quarter](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[QuarterName] [nvarchar](15) NOT NULL,
	[ShortName] [nvarchar](10) NOT NULL,
	[StartDate] [date] NOT NULL,
	[EndDate] [date] NOT NULL,
 CONSTRAINT [PK_eserve.Univ_Quarter] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[StudentsByEthinicity]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[StudentsByEthinicity]
as
select Student.StudentID as StudentID, InternationalStudent, Ethinicity.EthinicityID as EthinicityID ,Description from Student
Join Student_Ethinicity
on Student.StudentID = Student_Ethinicity.StudentID
join Ethinicity
on Student_Ethinicity.EthinicityID = Ethinicity.EthinicityID



GO
/****** Object:  View [dbo].[StudentsByFocusArea]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[StudentsByFocusArea]
as
select Student.StudentID as StudentID,Student.FirstName as FirstName,Student.LastName as LastName, FocusArea.FocusAreaID as FocusAreaID, FocusArea.AreaName as AreaName from Student
join Student_FocusArea
on Student.StudentID = Student_FocusArea.StudentID
join FocusArea
on FocusArea.FocusAreaID =  Student_FocusArea.FocusAreaID



GO
/****** Object:  View [dbo].[vwGetStudentReflectionAndPartnerEvaluation]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[vwGetStudentReflectionAndPartnerEvaluation]
as
select SignUpFor.StudentID as StudentID, SignUpFor.OpportunityID as OpportunityID,StudentReflection,PartnerEvaluation,SignUpFor.CPPID as CPPID from SignUpFor
join Student
on Student.StudentID = SignUpFor.StudentID 
join Opportunity
on Opportunity.OpportunityID = SignUpFor.OpportunityID
join CommunityPartnersPeople
on CommunityPartnersPeople.CPPID = SignUpFor.CPPID



GO
/****** Object:  View [dbo].[vwOpportunityByCommunityPartner]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create View [dbo].[vwOpportunityByCommunityPartner]
as
select OpportunityId,Name,Location,DateOfCreation, OrganizationName
from Opportunity
join CommunityPartners
on Opportunity.CPID= CommunityPartners.CPID



GO
/****** Object:  View [dbo].[vwOpportunityByFocusArea]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create View [dbo].[vwOpportunityByFocusArea]
as
select Opportunity.OpportunityId as OpportunityId,Name, FocusArea.FocusAreaId as FocusAreaId,AreaName from Opportunity
join Opportunity_FocusArea
on Opportunity.OpportunityId = Opportunity_FocusArea.OpportunityId
join FocusArea
on FocusArea.FocusAreaId = Opportunity_FocusArea.FocusAreaId



GO
/****** Object:  View [dbo].[vwOpportunityByQuarter]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create View [dbo].[vwOpportunityByQuarter]
as
select OpportunityId,Name,JobDescription,QuarterName 
from Opportunity
join Quarter
on Opportunity.QuarterID= Quarter.QuarterID



GO
/****** Object:  View [dbo].[vwOpportunityBySection]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create View [dbo].[vwOpportunityBySection]
as
select Opportunity.OpportunityId as OpportunityId,Name, Section.SectionID as SectionID,class.CourseID as CourseID,class.CourseName as CourseName from Opportunity
join Opportunity_Section
on Opportunity.OpportunityId = Opportunity_Section.OpportunityId
join Section
on Section.SectionID = Opportunity_Section.SectionID
join Class
on Class.CourseID = Section.CourseID



GO
/****** Object:  View [dbo].[vwOpportunityByType]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create View [dbo].[vwOpportunityByType]
as
select OpportunityId, Opportunity.Name as Name,Opportunity.TypeID as TypeID from Opportunity
join OpportunityType
on Opportunity.TypeID = OpportunityType.TypeID



GO
/****** Object:  View [dbo].[vwSignUpStatus]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[vwSignUpStatus]
as 
select SignUpFor.StudentID as StudentID, SignUpFor.OpportunityID as OpportunityID,SignUpStatus from SignUpFor
join Student
on Student.StudentID = SignUpFor.StudentID 
join Opportunity
on Opportunity.OpportunityID = SignUpFor.OpportunityID



GO
/****** Object:  View [dbo].[vwStudentBySection]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[vwStudentBySection]
as
select Student.StudentID as StudentID, Student.FirstName as FirstName,Student.LastName as LastName,Section.SectionID as SectionID, SectionName from Student
join Student_Section
on Student.StudentID = Student_Section.StudentID
join Section
on Section.SectionID = Student_Section.SectionID



GO
/****** Object:  View [dbo].[vwTotalVolunteeredHours]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  Create View [dbo].[vwTotalVolunteeredHours]
  As
  Select OpportunityID,StudentID,sum(HoursVolunteered) as TotalHours,
  sum(PartnerApprovedHours) as PartnerApprovedHours
  From TimeEntries
  Group BY OpportunityID,StudentID



GO
INSERT [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'ab5c45aa-7840-4f7d-9d3f-67812dcb71ed', N'Admin')
INSERT [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'f8b68ad2-61bc-426a-ad19-80eb1b1e58b2', N'Alumni')
INSERT [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'f48b3c22-7231-4273-9b37-6427f63a1627', N'Faculty')
INSERT [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'e7166f76-08b5-4887-8b78-51348476bf13', N'Partner')
INSERT [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'476e32ac-97ce-4639-8c2d-adc48a747610', N'Student')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'41033daf-da4b-4af0-88fb-c6bf2075d43f', N'ab5c45aa-7840-4f7d-9d3f-67812dcb71ed')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'41033daf-da4b-4af0-88fb-c6bf2075d43f', NULL, 0, N'AA8xhYJEuXupXDRdkWYfIY1zwAbzAnSaIVVYjzQL8DprVCEvA0cxZ2eXiI2ISpqyPg==', N'eebad5f7-15e9-4e81-aa75-3af300a10f85', NULL, 0, 0, NULL, 0, 0, N'admin')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'6cea2fa0-634f-4f98-a128-3604b19a2a9f', N'Student@eServeSU.net', 0, N'ABN379Gs04z/NSZ25YH33GczVO+C4osOaRPK18BvRwHCZxH+VfgNdFryuux+SjBx3w==', N'2b17a07a-4d69-4f7a-93f7-a2c99a8815a5', NULL, 0, 0, NULL, 1, 0, N'Student@eServeSU.net')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'8cba18d7-3e6f-454c-8e7d-ea8a82c6e53b', N'Faculty@eServeSU.net', 0, N'AKXjnuO24ynInX6o4d/wB4dd5X5XcUrtdOotBOh8tUfyBasvDQjXkVGZXNGibDOSAA==', N'4690084e-5a13-4b84-9aa6-19737607279b', NULL, 0, 0, NULL, 1, 0, N'Faculty@eServeSU.net')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'a95a2ef2-5ac5-4c84-b87b-446627d3eb8d', N'Unknown@eServeSU.net', 0, N'AFppovpvklVZkarp4juugr+Ehpn7BjM1sjo0s6NdSWvHVanjS3DD3cKiFVIuxePJ7g==', N'8ac4f4e4-a6b9-4f10-a0c7-b98c071b6c56', NULL, 0, 0, NULL, 1, 0, N'Unknown@eServeSU.net')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'd9092ce2-653e-4212-88e1-2eded2e3dfd4', N'Admin@eServeSU.net', 0, N'AG8oefCd5vr00RoEy4WiNDGSEkuf6cmhKdjY9FonZ8cPvR+CgaokXpPbDiF8FjR//w==', N'6d5f606f-75b8-4296-b034-49446788010e', NULL, 0, 0, NULL, 1, 0, N'Admin@eServeSU.net')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'e9bacbbb-a362-48b2-9eff-91c660efd167', N'Partner@eServeSU.net', 0, N'AIYRG/4A8m7Zbpzu6UOK44pzO+FH/gQqG8kPBn1LawhVI5vJF3Zy/37sH6u3hDY6CQ==', N'd3f1e486-a896-4b56-a0a4-5dac29247aaa', NULL, 0, 0, NULL, 1, 0, N'Partner@eServeSU.net')
SET IDENTITY_INSERT [dbo].[CommunityPartners] ON 

INSERT [dbo].[CommunityPartners] ([CPID], [OrganizationName], [Address], [Website], [MainPhone], [MissionStatement], [WorkDescription]) VALUES (1, N'Kamenski Evil Industries', N'1004 Broadway', N'seattleu.edu', N'2060000000', N'We are the best!', N'We work hard.')
SET IDENTITY_INSERT [dbo].[CommunityPartners] OFF
SET IDENTITY_INSERT [dbo].[CommunityPartnersPeople] ON 

INSERT [dbo].[CommunityPartnersPeople] ([CPPID], [CPID], [FirstName], [LastName], [Title], [Phone], [EmailID], [Password]) VALUES (1, 1, N'Yev', N'K', N'Mr', N'2060000000', N'kamenski@seattleu.edu', N'password')
SET IDENTITY_INSERT [dbo].[CommunityPartnersPeople] OFF
SET IDENTITY_INSERT [dbo].[FocusArea] ON 

INSERT [dbo].[FocusArea] ([FocusAreaID], [AreaName]) VALUES (6, N'Aging and Disabilities')
INSERT [dbo].[FocusArea] ([FocusAreaID], [AreaName]) VALUES (1, N'Children and Youth')
INSERT [dbo].[FocusArea] ([FocusAreaID], [AreaName]) VALUES (4, N'Hunger and Homelessness')
INSERT [dbo].[FocusArea] ([FocusAreaID], [AreaName]) VALUES (2, N'Intercultural Connections')
INSERT [dbo].[FocusArea] ([FocusAreaID], [AreaName]) VALUES (7, N'SU Youth Initiative')
INSERT [dbo].[FocusArea] ([FocusAreaID], [AreaName]) VALUES (5, N'Sustainability')
INSERT [dbo].[FocusArea] ([FocusAreaID], [AreaName]) VALUES (3, N'Working with Families')
SET IDENTITY_INSERT [dbo].[FocusArea] OFF
SET IDENTITY_INSERT [dbo].[Opportunity] ON 

INSERT [dbo].[Opportunity] ([OpportunityID], [Name], [Location], [DateOfCreation], [JobDescription], [Requirements], [TimeCommittment], [TotalNumberOfSlots], [OrientationDate], [Status], [ResumeRequired], [MinimumAge], [CRCRequiredByPartner], [CRCRequiredBySU], [LinkToOnlineApp], [SupervisorEmail], [TypeID], [CPID], [QuarterID], [CPPID], [JobHours], [DistanceFromSU]) VALUES (3, N'weft', N'qwefqwf', CAST(N'2016-05-20' AS Date), N'weave', N'acvaev', N'1 quarter', 12, CAST(N'2016-12-12 00:00:00.0000000' AS DateTime2), N'Pending', N'Yes', N'5', N'Yes', N'No', N'dsffef', N'N/A', 3, 1, 1, 1, N'12', N'5')
SET IDENTITY_INSERT [dbo].[Opportunity] OFF
SET IDENTITY_INSERT [dbo].[OpportunityType] ON 

INSERT [dbo].[OpportunityType] ([TypeID], [Name]) VALUES (3, N'Community Service')
INSERT [dbo].[OpportunityType] ([TypeID], [Name]) VALUES (2, N'Internship')
INSERT [dbo].[OpportunityType] ([TypeID], [Name]) VALUES (5, N'Mentorship Opportunity')
INSERT [dbo].[OpportunityType] ([TypeID], [Name]) VALUES (4, N'Other Volunteering')
SET IDENTITY_INSERT [dbo].[OpportunityType] OFF
SET IDENTITY_INSERT [dbo].[Quarter] ON 

INSERT [dbo].[Quarter] ([QuarterID], [QuarterName], [ShortName], [StartDate], [EndDate]) VALUES (1, N'Spring 2016', N'16SQ', CAST(N'2016-01-01' AS Date), CAST(N'2016-06-01' AS Date))
INSERT [dbo].[Quarter] ([QuarterID], [QuarterName], [ShortName], [StartDate], [EndDate]) VALUES (4, N'Summer 2016', N'16SU', CAST(N'2016-07-01' AS Date), CAST(N'2016-09-01' AS Date))
SET IDENTITY_INSERT [dbo].[Quarter] OFF
INSERT [dbo].[Student] ([StudentID], [DateOfBirth], [FirstName], [LastName], [PreferedName], [EmailID], [Password], [Gender], [InternationalStudent], [LastBackgroundCheck]) VALUES (106288, CAST(N'2016-01-01' AS Date), N'Yev', N'K', N'Yev', N'1', N'1', N'Male', N'No', NULL)
INSERT [eserve].[__MigrationHistory] ([MigrationId], [ContextKey], [Model], [ProductVersion]) VALUES (N'201605260025130_Initial', N'eServeSU.Migrations.Configuration', 0x1F8B0800000000000400ED3DDB721CBB71EFA9CA3F6CED53E292B59454B68F55A45D142939728E44862B1DE78D35DC05C929CD65CFCCAC4A742A5F96877C527E21983BEEE8C660766665155FB803A0D168F40D4037F07FFFF3BFA77FFE16478BAF24CBC334395BBE787EB25C9064936EC3E4E16CB92FEE7FFBD3F2CF7FFAE77F3A7DBB8DBF2D7E69EBBD2AEBD196497EB67C2C8ADDEBD52ADF3C9238C89FC7E1264BF3F4BE78BE49E355B04D572F4F4EFEB87AF1624528882585B5589CDEEC93228C49F583FEBC48930DD915FB20FA906E499437DF69C9BA82BAF818C424DF051B72B6246B927D25EBCFCFEBAACBC5791406148D3589EE978B2049D222282892AF3FE7645D6469F2B0DED10F41F4E9694768BDFB20CA4983FCEBBE3A741C272FCB71ACFA862DA8CD3E2FD21809F0C5AB86302BB1B91379971DE128E9DE5212174FE5A82BF29D2D2FD238DE27F4DB75901509C9960BB1D3D71751563690A8FC5C6CFB6CD1D678D67105659EF2EFD9E2621F15FB8C9C25645F6441F46C71BDBF8BC2CDBF93A74FE917929C25FB286251A5C8D232EE03FD749DA53B92154F37E4BE19C079BEFB480A3AAFD9EDFBCBE562C5835889303A08EAE6F540298B50565F2E3E04DF7E26C943F14885E0E54FCBC5BBF01BD9B65F1A9EF99C84543268A322DBD39F1FE93082BB8874E52B63F757D94390847FAF085D7E3161F0BB93313038DF6E3392E7868E5F9D8CD2F1DFC85D1E16C61103696EEEE7431026D78F6962ECE9F7638CF043989732B0A67244629214060480736BA1689A7DB924F9260B77B5B63074E7D4DFC7E06BF850F1AAD0B3A809E890EFA9E2BD2151553B7F0C77B5329674C6ADA6E9BB2C8D6FD248015BDDE2769DEEB34D39C729AAD9A7207B20057CA457BB5D9A15259C90E4B0110A4D0C23E36ADA47C457578DE474D52B7C9419781BEFA2F4899021E6A08531895970B105BD01789F14AF5E2AE49D21C3BA4833F21742C749A57B7B1D1405C9A87D7FBF25156D6DB22A4DE614D6E75D98E585D5EC8CD1F3CFC1441D7F0A8BE8F0BDBE8D833032F53A8E89B59ABD571EACCEFB9C7562CEB771D8599E3729555C4162C71B6C580E6B535A958AB429ADE21ECFA6B4AA156E5BD42DAC3646D36C90ADE9413D61CC0BD3EC8745516936DACFADBDC323304B25129C400D1DD52525E8D5FD454602D62F2EBF7E0A633C7EFFB1A7C871B47143CB6200C759EFFD9C6E02EBEA608C8EFF9ADEC11627D418DA10687E2331B821BFEEC3AC5A8A99D6BBA3F55F325BC9DB61510C5D0EC2FAA39A25FAB88FEF487675BF8ED27ED40DBFDAF626428A65C52CA5A8C862636E5E2E7BF7263A4307899EE57C1F9366AEB76857445AC32761BC8FCF1FC840C57A73D1A2F4A6776604DC1020D69F91AD7F0E932F9FD2AB240A1372BEDB19E6E52594FDADF2FE6FD4BB10790EABBAC3BC08920D293D9B7ECC9729B5E900D6F0EC57BAAEE4AD7EA4DA291B7FF7C59B1F691BA1C5FD4478CAB7EF52EA339E535BAE7695B92AFA7119AA498331D5751941C9C35ADC2B37CE88B65C4389B1A29AFBC2E4E9765DEC4B47F4F6866CD26CABA57D57DFD8961B10A489B45081B6C3EEEF35EDEDE3EB2BAA07D3961B31EF2A61D1A4AAF8EB6DE3872A51652BE8D9495F4B62294355155B615683BD7021D7837DC31F2B42455F25612C6B0CB0AD77B6B4F6CD0D0F1A5B216356EDEE4B23301D697482A2860563A35E008B56030523544D931F27B98E7B0C6FC2AC781CBCC1F00FB82F7F9D917B425736B6CE7D9C93528DBAED575F2AAD081BA2655F3C29B575256F41D489E2B0D568393D6F82CD97872CDD27DB8B47B2F9625D966B15DBDBE2912E6E377AC5DC55507B3B8A62C96B50D519DDE947A861B3AB3F08E52AB4C8ECE6EB10E50AF5CEFD10F4EC3619EFE92A10D5BBC363AD41DA3AC0F507A0BA64AB216D0619EE5E6A30B6BB6FF5C31B56F4058D8CF162DDDCBC4907852BF2A649293BAFCFEA0D0BDCD2AC2CF9C1878ABE869EFA1C6641E6B40DA55AD41876ABBCADC010C6548BA23769D15B1DA7B36E11CC2432C5E223CB97B2498B76B8C58BA3D8DD50D1FC5885C1D88EAB7A7C0FBD765C870FC9E79DF5C00CB8F8C1765E0F9CCE5C443616EB48FFF5B0326ACF04BE06D1DE7616EDA7C7668C63F60871600FBD696E72C8210EEF101FBD5C8B52F5982156185DFF5678C68508140C64671E0CCBD1A61D6A0923720266D9E3CD10725338CC1832A066E264FAB69BE3B8B196888F09EDEE547BB683376B1B6B76BEDB65E957B255C53C00D23668EF253F3F0D46A7EAFF9734DA2705214C000C902346390B9E48BF03F6A3D0B6C24919F2A7B570CDC7B69B899A9BD75ABAA1CD242716EB47CA307E8E2BD01E3B1DB517DDF536D9BAC119B0BB3038324174DB00410CA8B08A8B52834290E76B6B70672B9951E76A0EDA85E01143EA9BA6DD0F7DA3D7378323BF25DDE11AAD5F4E959F3072071159D7FB05064969B959D9481018435DB5DC981A8C1B4F85177C63349552450C15FC9ECE4EF2DF36FFA106B47237580BD09FF724CFD3E1FAE4264DE33AD0DFA008FE308A3372110579CE2D7A946166A384FA9B721BB01AB9E6774F87337EFD0D5F5A54A984206A17359C8EAB0D23EAEB800665AEAE1E97A5CD7035CB8E13A962FBA63FD4ABA22F2FE16F6E1AC547FC9B5BCF558AB871136CA47C296B92F8CB910FA4511EA55FCDA1F42B81DAC649737C0876C212B5ECC2DD6383C09BFAC418BAF15DA3EBA29C84CE0EB26DDD63EBBF3B143B75810BC339C9006A12266AF0199F81988EBE5BE639A72A6D1356C05481B5C2561C8FFFDB64BB406461D4A6440ACCA56685324AB88BAAF8B4B3E56F243AC1FAE9FC3DAE9FFE90DDD8CBE98A2184993EAA703C1DBEC6D8BC1E4F36B2134E0C63A4750FBC3B53F645004D209869CE4C51613C5BD441853CAA2F2C1C614A7D746106DAC14D9577909497415E503D47F5579814B2B209934DB80B22D04885D6F003E455D78F5872497665D2425280880141A06AADC6A2EB4CD0A23652B9709655B00C217C767EB269184DA0FDB822E57C2C6A18C9F01818A5F8C827BD28791D7E726BC68A3B331E4BD0879216228CB6381164842556950C9DA7D18738999E828462E90400955A62553926514345803909BA2771429004C21EC68C5194102028385C340F645F150971306FDB66693D38F493D95C1741C686CDC25C51A8ED9CBD5023C97318EF18433E1FE6EB50522D676522FC2F2C8B5AE4D990013AA2341BE28774B84282897A8CF95800B8D002E2900EBA48B58FFA00A268270A040936EE6612C9839C3D18F9027A1021B0217BC888E444E8A1A9D0A3B8833C1E6BC24872281E85910B820D1FD6311DBB9AC2068C9C038AC452B20D9E4B41F10A93B128801287E24F00A120A830A14BD371A6328E0E645695A172BE6D37D789990947E53ED3A00F6DBA5534390ACB0DBCF252C718D87BD47B2ED13FA901674BECED9B87742F71943900BFE2880553968A8BC867C1C59A270EA07C647B4347CFC543B8D7F6CA025E7646626533790EBB3389232294AB3988F3E16A379D8CD7C583B87886BAD75DE78EC8A883D4EE140C5AC7CDD036056D41B2069B3AEEA9FC4ABEA9EED2A4B46B82AAF226845EE4A412EA9A149AD15117B20FD8D1F2ABC49E66A0CD15E056C0BD62B5742024EA4980399EB7C3E22EF45301630F192CE0FA8B7F2440DDE6A305047F15A204868DDA018CADE205CDB0EA280530B1157BF826D2CBB51D7AE24ED541BD712D2C3DB2AB0A157C7E050701D6ADC4D4B0BA6238AC7E6BC104B2AF0581CCEEE4A9A1B2352C106161C45237B06680BE8D31A7CA6E8D2D841E19F5AC511A62DE30D30270A9B4684970D18D1D3D644526D9285C3C230F991D9F18CFCA5308403DE5CD8232D5AC118FDC984C318FCC5878E56AA09129CC9181D70F60305934018E1A7EB285424A136E088614B8A83119160632843F8EC83B72AC9E893E00BED187F4B953E5700CE37EAB89926A7EA2016D47B3E87840253F59FD0B6F6818D91AE678E0271774EF963C89E818318E4A982831004FA3610329AD7328DDD489EAAE6E9B0D072A164318930FCB3D819E41B2A353B4935670916C69B43D4E9D4CC19F72900D4C772395B2470A1E882DB92321ABCF64A8AD1F95BE918A5AC26AD2402D03DC03F850C2024F9DBBAAA12026ECC439F044A4A97E31EA1C6922766159053A12D978598286C2E04809B75809F5C001848504471C86AAEA9B6F2C22CF55064A26376E7F02CF813593CE07CDA0EF2CCA047439AE1E7260CDD040B3876C2031F2787A44FD0A7D02DE4E70C8C9EA90B35503C1CDFC8C3C4DC54EAC0FA2A3B9DB99ABF1DC8C24EE681CDCA66777C74C5DD9E96ABD792471D07C385DD12A1BB22BF64154E7E8B7051F82DD2E4C1EF2BE65F365B1DE059B7234BF5D2F17DFE228C9CF968F45B17BBD5AE515E8FC791C6EB2344FEF8BE79B345E05DB74F5F2E4E48FAB172F56710D63B5E1E6413C14EB7A2AD22C7820426949C82DA9EE6FB90C8AE02E2893D82FB6B1548D3F54D36C38B77DE9CECDE4E96C379EDB96E5FFEC295E7FDD8174C2261F4C3640DED17196CF75574326060E93415020EB4D10059990FF2F1CBA5EA4D13E4E40E7B17A9857D94390847FAF38AEBEC186052B97C2219F6FB71975EF043CDB8F70387F237779580888751FE1703E501E6A2EAD6121319F11B0C2BCBCCAA27C7A81D46FB27320A552C468CB5B9DD92787B8518B858A6CC895C07FD2F9B7C4EE820A12E5C849CA74D6CF5DD2BA23E7E112A707A59B1551DC7032A60C4C60C1812217F4F0998BAF58A8CC6738ACFE262B1654FF150EE95358440298E6131C4675BB150FA3F90487A19079B4BCBFCF594D78BE8D434132951566239D3677092E956CBC065E108DADC791BDEEBE0A8E0F759758E8E18C2DC3CA985111BE35A8540FBF7E67F62223816C56C4323854364C9B85680ADFD64393F50E5A7BA51BC5F8FAAF70487F4DEFB466582C8343BD21BFEEC3AC720A04C7882FC168D99894AC111685EC88488508B8691144C245A91C684539C6EB0C293ED5A4D417C9F34EA7500887DBBE85C5826BBF616629DFC7A49991AD384F7C19C6674CC2781F9F3F880E28F31DA12F6E2E5A24DEF4E17E9CC250D670EC61FDD900BC2C44C868987CF9945E25115DD29DEF7682A88A8528896DEE131664B5F98AD095615E04740D5D5E0A290E5C2C9B938D678F26071A79268ED2C9CC9BDA8F63E8CBBE6403D27F9DCD44190EFDE053D446A8E22747DBF290BB0EDCABF7B22BD2141CFB0A887F9C9E5B8070257088ED13F42CACF61B6235A37C629E133D650D1C0DA507E745724A156623A25CE8E110316522C0F1926A6A3C8E0ED57ABCF3DC74EA63FD865ABB2ABCDECDD0A99B8E333FF005D2E41B0D90C021A76D071B5CDC36841D9A760923DCECC3AD60AC97D6E91730FD0D60FC22467F3398011AF74030078F2B41E3C7BEFAAB40932D46984BF97D5FCE66CAC568BC75B015C5B396297340EF30B932C076942D23C47134E5B8D2294234DD37A98726EF7C60B73BD46F882A8446A8813C8EEA1F19950EA3FA22384CF9A95116AC5C3A1B49E4838886C81D0BC941C8CCCDC79128EECD4CC55E2F7629C3BC65C78952FF19B5EDD7BE6C29ECFCB59F11474EEDDB96DCA153FB715EBCA80F9243B2A206109415B5CD4765454FA70E3E19917D59913FBCE9BFCF91850C61AB4E9CA4838763283D94B1CEE9BB4BAEE4C94372157F9B1BBFF763BAE74D0F917DAC8F3B9160BE23C6CABCBFC70D96F98E5898EA0F8A9CCF88B8A7F538C9640BE6254D4CF8FB6049D2C3824A9109C2381234CF7DD8EE8D34298E0429D388489289B81094AA3F84355D3A00F0AB1B58C0EACFB0F843BA09FDE34B0A65A4799A603A26305D9C3074FE11B081538F8268599D6B16E7C73FDB7C6CB8E1F4570CA8079EF48ACD8CC7BA8650AE323E5E1648DB0519323941F6A987AABEED89C165189ADA8BA84068DAF69CF4888A390168D650DEE10139B262AAEB8FA634C7C90AE21AAEFD70242A03D1C3E41BAE1101E267382937BF4833DADC2BEEE0B01F8419E6BDAD029F75EDA51D071724207ADEA75CFD98CBA853AEBA27016A04CC93DFD5C37180FE7685F9A9FDEF49F8153712A00E83D45CD0D642F180F61683D92802EDAD08F3E500292B50ACD2F9A4CD97EE779715D864E471A982D508CBC4BF6A6479931D28A6E8D555960B8AFBD7705BA6E7AD9FF282C4CFCB0ACFD7BF46175158C50DB5153E0449784FF2A27EDD77F9F2A47C23FC3C0A83BCCEF36C920F5F8B776582B2115FBC2AB311C9365E89CDF1398D25943CDF728F0FCBAF289B13F9604F151B23E8EC0F100BCDEBCBF392AF41B6790C32E139F69FD04FBCCBD97FA60ECA77E6911D74D98006B8AF4EF070BBEC403841AA1B582D6099544113E0DFA3F19513060DF0793A43F096D2068DD02DE0C18F8CC3B2EE6062E2221BBD40842545350F61BF2FF572B988C5CE9932DF4724AC74EBEFFB644BBE9D2DFFAB02F27AF1FE3FA5747C0AE7D9E22AA3EAF2F5E264F1DF838598D97B354A2F1A70BF13EB176E9324E8176893356802EAA06700CAE0155658959984750F77616141112C9CDAED9BA395C72EA78FE903207E4D335EE4BE5355A04C21C4914B016210E9C4D4C31A9B6D156B8104C59EFEE306D5B71C3416AB3274F090FAD445BF70C514C61ABA462FE2C1F3D98C9E814B598D7AF8785F4995D7C870130E9894C9A8666F08A83654173A56E034F1C98C5033A3F260FB244625B9600A4A99B2A8C40A42332949D119929496A89F869736075A298C4DC4853BE9C4BCC41AD27D94063E7D065D5ADFD17A0D7D7E207846879050B963F61DEC1470697B036CF8D12D56F8443EE3F202AB15DABC3E03D097BF43AF2E94897DEE7A5F99C707B57260B1D165C21DADDA81EFC6F854DE72AADAD112D0A6B3BD120E962C06A3A5F5D8426EA23F48B44F83D81D6E61C4B71EB4386207E1B034EE9B7B5D11F3697060F58D19319B0BC777F02F71F0ED5FB10A5C9105E701AA22FF0D0D7590446953C45C35946FE1F3A003B1A72F1E45F7B61F86172976DCE0B223F5C283509B96DA30A74E9934E7BC0B2024CA0D404CCE8D83B2175838F5A96547EB2B70096A7EDD7F2649C8B7F1E852D706F04B97B6068681E313F51BEC47CE27D36D204BDCE4B487C6A49A0D384576610475C8F6D1F20393F7853D24691A0EE2866B2E4F0C8701DB7610126C7299819BFE80D7706CA29969EF0DBF34046CDEC3C491CD33733F63C049D2B52667EB68A548DAD21B6947CF67A0404F124F072D5CA0807AD3D59761C66752A1372EA04B275DB20C6ADB62D8228AC1C1BBAB8C4A5C8211D990ABE497C04C47F3242E2CD90846554BB038769FC04E5DBE439CE966DB0E32DD136DFD396CEA0F3815B3A500F9DDD0117AC351946B3CE52E8C6E2F08B5ED823E781830C93FF6CCA7DB337753D803261BADAC8708F42C14F5B189B331FD6774891EE84ECE43A2E738D54CAE914AA40159DBBAD78FDBB1CAAF3353D4F75111EEA2CA2E52ECA55CB2ABE49244A4208BF34D9D0A7411E49B602B53BACCA20261C33A935AC4B84A3C8EBF91BABEA96240285183E8823AFE45168472F2FF7516269B7017444A0A09B5E1875CAB0EAE5872497665044952D88800E9DB966FD975264C8A8D345C8A9B3B37AA12F5D44F1E8B53AE9BE57F2C4E44F1C2947C68CFFA3C0027426F3260E69D896062679AFD7C10FE93175D6A74BAD25138CE782DBA7786B35E56E1B4B29C8AD12CAA4E35AD9AC9FCAE590C33D596672C4667357376F8814C6B95086578284E3062552CA168B4EA8F229789F339C82E7BF4BEE47048CDFC0C3778281B57CDC4F4BC00356D486E18CDAD62379954A88CAB6F0ECC50A65B78E6EB38C16EDB9995359B9EB3A6B1642E3C66BEF1E830FCA58BD5BE5595E9A34E95364A8AFFD6D82CB99E934954A269EB92AB3BF67211F0160B5E1339B0968B2906BE77E119FFC9546F8B003C9DC1510BE3587BA0241DBB8A1D24487350B7D09BDD8E636B5867E4E50ADFE19630460866B71D7C5C0EE55C186E3AD7D289EB2637A2ACC67631A44EE710C768500F7FA430AA4B3AD5E205624BE773B025C60568101ADD864EC479C7B81B03BBF2746EF673168C36F99AE1A82C279B56083C65E03211D929E60B8EE1B4C1FC5EDFF4270EA687D30EC71E7DEACBAD3DA94CE013266D466215B60CCE2D2A1424D062F978DCA3CB0B1A8781ACF4D7747B6D7CDBEC70ACD424A6E2F9A8CD68D5CCF4317390FE8DC7D9B08FE1A5BDC35B294386F38846CA890B8FD44CA1D972723BA5BBD31AE8D268AFC466275A5FE9185C1DD8ADDFE3F013D6ED51DE873B0BBE92AE02AEF003F395919F307CE4956147E2B003AFC00631B8F2A6E879709C9B06F3C669F3D158D3ADE867CD426FF987663A6C84476124CE691E0ED260BF5CBCED9251B45C543F3473B62479F96011658F3AB745AC28BD2E6CEDBDD754562CFAAA406C6A8D6DC388636F0909AE54D32F2FB780FED82356558F6CB9BE4FB696A5CF6E1F4EEAAD2BD1F4D3BF4265E9828DA3977A610B351D7555A034AC631B95E4AB8B0C94AB2BC019433EB331318A5CDBCA388A360ED8719147200CB916082CB976164CF9759884165FACC181AF04E9B05BA7A93BEC8A4D1D7695E01DF6DB08A67EFB5AF6EEFBBA102CD89D3635066C0D53EF6C3D4BCFA00B4F647460CD3438C21A031037DD1FA2C6D9D8C280AEB11DC66A584D96A62A98945D738CDED71B19451D08999C4C821E0BA9060407DAA86C839C203312723520268869511DC2992C0204159E47B49E01FF62223C9179C13454510BF468B53ECAADA390C579D2C350889DF9556EE9509527CD40B235E7C448B2A91FD4D42D03F951EA06362B9219524315A4823F89AD49256506C57E361048AB3135EF7A8F43123DF7C05FE15645253043D10C606262741930C2B24D2D46B69C3C89F5D96509F760B74D68B0423764E800C1D0D61D69F82AE3AD7CF07C0C329855A93D5FCA8B281C9C08CE893A6A3AF9C9FBD189856E092E8A896D216D85AF58446BFB302D83F15302491051901E9D57E28B5B7D4FCD60FF08A8D6800913E33A933A29972B8C4524A8FF7818CD37116140C1E4167DE726781EFCEC7908A036641A662660C2E78B541AEE928AC7218C51E46091C0BE95F7C108C245F6587D6F436DFDB0543BCDD5804CDBC713F8E1C2FEAA32464E47134C88A64C1C797BB8A78F7EC35786A3DE27ED615976371D49668A29D4D10B1C87A81BA47674B32593327CCE26635CE53145CC89B40EA400068A29E8E21262068CD96186ABAF34B97E02C64201480789A2B244B71848662495D749F0414434DF39F39B3BD1FCF2577BCF6917B2D2959DAEEA7386E603FD59A459F0403EA45B12E5D5D7D3D5CD9EB68EEB1B524F2F491E3EF4204E29CCA4D69B3DD0B6CEFBE43E6DC376048CDA2AC2ADB01F48116C832238CF8AF03ED814B478436D61983C2C17BF04D1BEDCA98CEFC8F67D72B52F76FB820E99C47711478C32E2C7D4FFE94AC2F9F4AA7A8034F731048A66583E2F7595BCD987D1B6C3FB9DE2FE590D883294A87977A49CCBA27C7FE4E1A983F4B17A410302A8215F1701F58950B9A2C0F2AB641D94A74A78DC3EE7E467F2106C9EE8F7AF61F52CAF0E887D2278B29F5E86C14316C47903A36F4F7F521EDEC6DFFEF4FF89721D975D510100, N'6.1.3-40302')
SET IDENTITY_INSERT [eserve].[Ethinicities] ON 

INSERT [eserve].[Ethinicities] ([ID], [Description]) VALUES (1, N'American Indian or Alaska Native')
INSERT [eserve].[Ethinicities] ([ID], [Description]) VALUES (2, N'Asian')
INSERT [eserve].[Ethinicities] ([ID], [Description]) VALUES (3, N'Black or African American')
INSERT [eserve].[Ethinicities] ([ID], [Description]) VALUES (4, N'Native Hawaiian or Other Pacific Islander')
INSERT [eserve].[Ethinicities] ([ID], [Description]) VALUES (5, N'Hispanic or Latino')
INSERT [eserve].[Ethinicities] ([ID], [Description]) VALUES (6, N'White')
SET IDENTITY_INSERT [eserve].[Ethinicities] OFF
SET IDENTITY_INSERT [eserve].[Opp_FocusArea] ON 

INSERT [eserve].[Opp_FocusArea] ([ID], [AreaName]) VALUES (1, N'Children and Youth')
INSERT [eserve].[Opp_FocusArea] ([ID], [AreaName]) VALUES (2, N'Intercultural Connections')
INSERT [eserve].[Opp_FocusArea] ([ID], [AreaName]) VALUES (3, N'Working with Families')
INSERT [eserve].[Opp_FocusArea] ([ID], [AreaName]) VALUES (4, N'Hunger and Homelessness')
INSERT [eserve].[Opp_FocusArea] ([ID], [AreaName]) VALUES (5, N'Sustainability')
INSERT [eserve].[Opp_FocusArea] ([ID], [AreaName]) VALUES (6, N'Aging and Disabilities')
INSERT [eserve].[Opp_FocusArea] ([ID], [AreaName]) VALUES (7, N'Seattle University Youth Initiative')
SET IDENTITY_INSERT [eserve].[Opp_FocusArea] OFF
SET IDENTITY_INSERT [eserve].[Opp_Type] ON 

INSERT [eserve].[Opp_Type] ([ID], [Name]) VALUES (1, N'Community Service')
INSERT [eserve].[Opp_Type] ([ID], [Name]) VALUES (2, N'Internship')
INSERT [eserve].[Opp_Type] ([ID], [Name]) VALUES (3, N'Other')
SET IDENTITY_INSERT [eserve].[Opp_Type] OFF
SET IDENTITY_INSERT [eserve].[Univ_Quarter] ON 

INSERT [eserve].[Univ_Quarter] ([ID], [QuarterName], [ShortName], [StartDate], [EndDate]) VALUES (1, N'Winter Quarter', N'16WQ', CAST(N'2016-01-01' AS Date), CAST(N'2016-03-10' AS Date))
INSERT [eserve].[Univ_Quarter] ([ID], [QuarterName], [ShortName], [StartDate], [EndDate]) VALUES (2, N'Spring Quarter', N'16SQ', CAST(N'2016-03-20' AS Date), CAST(N'2016-06-10' AS Date))
SET IDENTITY_INSERT [eserve].[Univ_Quarter] OFF
SET ANSI_PADDING ON

GO
/****** Object:  Index [RoleNameIndex]    Script Date: 5/25/2016 5:36:18 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [RoleNameIndex] ON [dbo].[AspNetRoles]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_UserId]    Script Date: 5/25/2016 5:36:18 PM ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserClaims]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_UserId]    Script Date: 5/25/2016 5:36:18 PM ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserLogins]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_RoleId]    Script Date: 5/25/2016 5:36:18 PM ******/
CREATE NONCLUSTERED INDEX [IX_RoleId] ON [dbo].[AspNetUserRoles]
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_UserId]    Script Date: 5/25/2016 5:36:18 PM ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserRoles]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UserNameIndex]    Script Date: 5/25/2016 5:36:18 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UserNameIndex] ON [dbo].[AspNetUsers]
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UQ_Class]    Script Date: 5/25/2016 5:36:18 PM ******/
ALTER TABLE [dbo].[Class] ADD  CONSTRAINT [UQ_Class] UNIQUE NONCLUSTERED 
(
	[CourseName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UQ_OrgName]    Script Date: 5/25/2016 5:36:18 PM ******/
ALTER TABLE [dbo].[CommunityPartners] ADD  CONSTRAINT [UQ_OrgName] UNIQUE NONCLUSTERED 
(
	[OrganizationName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UQ_PartnerPeople]    Script Date: 5/25/2016 5:36:18 PM ******/
ALTER TABLE [dbo].[CommunityPartnersPeople] ADD  CONSTRAINT [UQ_PartnerPeople] UNIQUE NONCLUSTERED 
(
	[FirstName] ASC,
	[LastName] ASC,
	[Phone] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UQ_EthinicName]    Script Date: 5/25/2016 5:36:18 PM ******/
ALTER TABLE [dbo].[Ethinicity] ADD  CONSTRAINT [UQ_EthinicName] UNIQUE NONCLUSTERED 
(
	[Description] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UQ_AreaName]    Script Date: 5/25/2016 5:36:18 PM ******/
ALTER TABLE [dbo].[FocusArea] ADD  CONSTRAINT [UQ_AreaName] UNIQUE NONCLUSTERED 
(
	[AreaName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UQ_TypeName]    Script Date: 5/25/2016 5:36:18 PM ******/
ALTER TABLE [dbo].[OpportunityType] ADD  CONSTRAINT [UQ_TypeName] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UQ_Professor]    Script Date: 5/25/2016 5:36:18 PM ******/
ALTER TABLE [dbo].[Professor] ADD  CONSTRAINT [UQ_Professor] UNIQUE NONCLUSTERED 
(
	[FirstName] ASC,
	[LastName] ASC,
	[Phone] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UQ_Quarter]    Script Date: 5/25/2016 5:36:18 PM ******/
ALTER TABLE [dbo].[Quarter] ADD  CONSTRAINT [UQ_Quarter] UNIQUE NONCLUSTERED 
(
	[QuarterName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UQ_Section]    Script Date: 5/25/2016 5:36:18 PM ******/
ALTER TABLE [dbo].[Section] ADD  CONSTRAINT [UQ_Section] UNIQUE NONCLUSTERED 
(
	[RoomNumber] ASC,
	[ClassHours] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_CommunityPartner_ID]    Script Date: 5/25/2016 5:36:18 PM ******/
CREATE NONCLUSTERED INDEX [IX_CommunityPartner_ID] ON [eserve].[CommunityPartnerStaff]
(
	[CommunityPartner_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_FocusArea_ID]    Script Date: 5/25/2016 5:36:18 PM ******/
CREATE NONCLUSTERED INDEX [IX_FocusArea_ID] ON [eserve].[Map_Opportunities_FocusAreas]
(
	[FocusArea_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Opportunity_ID]    Script Date: 5/25/2016 5:36:18 PM ******/
CREATE NONCLUSTERED INDEX [IX_Opportunity_ID] ON [eserve].[Map_Opportunities_FocusAreas]
(
	[Opportunity_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Opportunity_ID]    Script Date: 5/25/2016 5:36:18 PM ******/
CREATE NONCLUSTERED INDEX [IX_Opportunity_ID] ON [eserve].[Map_Opportunities_Students]
(
	[Opportunity_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Student_ID]    Script Date: 5/25/2016 5:36:18 PM ******/
CREATE NONCLUSTERED INDEX [IX_Student_ID] ON [eserve].[Map_Opportunities_Students]
(
	[Student_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Ethinicity_ID]    Script Date: 5/25/2016 5:36:18 PM ******/
CREATE NONCLUSTERED INDEX [IX_Ethinicity_ID] ON [eserve].[Map_Students_Ethinicities]
(
	[Ethinicity_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Student_ID]    Script Date: 5/25/2016 5:36:18 PM ******/
CREATE NONCLUSTERED INDEX [IX_Student_ID] ON [eserve].[Map_Students_Ethinicities]
(
	[Student_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_FocusArea_ID]    Script Date: 5/25/2016 5:36:18 PM ******/
CREATE NONCLUSTERED INDEX [IX_FocusArea_ID] ON [eserve].[Map_Students_OppFocusAreas]
(
	[FocusArea_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Student_ID]    Script Date: 5/25/2016 5:36:18 PM ******/
CREATE NONCLUSTERED INDEX [IX_Student_ID] ON [eserve].[Map_Students_OppFocusAreas]
(
	[Student_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Opportunity_ID]    Script Date: 5/25/2016 5:36:18 PM ******/
CREATE NONCLUSTERED INDEX [IX_Opportunity_ID] ON [eserve].[Map_Students_OppTypes]
(
	[Opportunity_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Student_id]    Script Date: 5/25/2016 5:36:18 PM ******/
CREATE NONCLUSTERED INDEX [IX_Student_id] ON [eserve].[Map_Students_OppTypes]
(
	[Student_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_CommPartnerStaff_ID]    Script Date: 5/25/2016 5:36:18 PM ******/
CREATE NONCLUSTERED INDEX [IX_CommPartnerStaff_ID] ON [eserve].[Opportunities]
(
	[CommPartnerStaff_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_CommunityPartner_ID]    Script Date: 5/25/2016 5:36:18 PM ******/
CREATE NONCLUSTERED INDEX [IX_CommunityPartner_ID] ON [eserve].[Opportunities]
(
	[CommunityPartner_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Quarter_ID]    Script Date: 5/25/2016 5:36:18 PM ******/
CREATE NONCLUSTERED INDEX [IX_Quarter_ID] ON [eserve].[Opportunities]
(
	[Quarter_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Type_ID]    Script Date: 5/25/2016 5:36:18 PM ******/
CREATE NONCLUSTERED INDEX [IX_Type_ID] ON [eserve].[Opportunities]
(
	[Type_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Opportunity_ID]    Script Date: 5/25/2016 5:36:18 PM ******/
CREATE NONCLUSTERED INDEX [IX_Opportunity_ID] ON [eserve].[Opportunity_Student_Records]
(
	[Opportunity_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Student_id]    Script Date: 5/25/2016 5:36:18 PM ******/
CREATE NONCLUSTERED INDEX [IX_Student_id] ON [eserve].[Opportunity_Student_Records]
(
	[Student_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Opportunity_ID_Student_ID]    Script Date: 5/25/2016 5:36:18 PM ******/
CREATE NONCLUSTERED INDEX [IX_Opportunity_ID_Student_ID] ON [eserve].[Opportunity_Student_TimeEntries]
(
	[Opportunity_ID] ASC,
	[Student_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Quarter_ID]    Script Date: 5/25/2016 5:36:18 PM ******/
CREATE NONCLUSTERED INDEX [IX_Quarter_ID] ON [eserve].[Univ_Courses]
(
	[Quarter_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Course_ID]    Script Date: 5/25/2016 5:36:18 PM ******/
CREATE NONCLUSTERED INDEX [IX_Course_ID] ON [eserve].[Univ_CourseSections]
(
	[Course_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Professor_ID]    Script Date: 5/25/2016 5:36:18 PM ******/
CREATE NONCLUSTERED INDEX [IX_Professor_ID] ON [eserve].[Univ_CourseSections]
(
	[Professor_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Section] ADD  CONSTRAINT [DF_NumberOfSlots]  DEFAULT ((0)) FOR [NumberOfSlots]
GO
ALTER TABLE [dbo].[SignUpFor] ADD  CONSTRAINT [DF_SignUpStatus]  DEFAULT ('Pending') FOR [SignUpStatus]
GO
ALTER TABLE [dbo].[TimeEntries] ADD  CONSTRAINT [DF_CPP]  DEFAULT ('N') FOR [PartnerApprovedHours]
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[CommunityPartnersPeople]  WITH CHECK ADD  CONSTRAINT [FK_CPID] FOREIGN KEY([CPID])
REFERENCES [dbo].[CommunityPartners] ([CPID])
GO
ALTER TABLE [dbo].[CommunityPartnersPeople] CHECK CONSTRAINT [FK_CPID]
GO
ALTER TABLE [dbo].[Opportunity]  WITH CHECK ADD  CONSTRAINT [FK_CommunityPartnerID] FOREIGN KEY([CPID])
REFERENCES [dbo].[CommunityPartners] ([CPID])
GO
ALTER TABLE [dbo].[Opportunity] CHECK CONSTRAINT [FK_CommunityPartnerID]
GO
ALTER TABLE [dbo].[Opportunity]  WITH CHECK ADD  CONSTRAINT [FK_CommunityPartnerPeopleID] FOREIGN KEY([CPPID])
REFERENCES [dbo].[CommunityPartnersPeople] ([CPPID])
GO
ALTER TABLE [dbo].[Opportunity] CHECK CONSTRAINT [FK_CommunityPartnerPeopleID]
GO
ALTER TABLE [dbo].[Opportunity]  WITH CHECK ADD  CONSTRAINT [FK_Quarter] FOREIGN KEY([QuarterID])
REFERENCES [dbo].[Quarter] ([QuarterID])
GO
ALTER TABLE [dbo].[Opportunity] CHECK CONSTRAINT [FK_Quarter]
GO
ALTER TABLE [dbo].[Opportunity]  WITH CHECK ADD  CONSTRAINT [FK_TypeID] FOREIGN KEY([TypeID])
REFERENCES [dbo].[OpportunityType] ([TypeID])
GO
ALTER TABLE [dbo].[Opportunity] CHECK CONSTRAINT [FK_TypeID]
GO
ALTER TABLE [dbo].[Section]  WITH CHECK ADD  CONSTRAINT [FK_CourseID] FOREIGN KEY([CourseID])
REFERENCES [dbo].[Class] ([CourseID])
GO
ALTER TABLE [dbo].[Section] CHECK CONSTRAINT [FK_CourseID]
GO
ALTER TABLE [dbo].[Section]  WITH CHECK ADD  CONSTRAINT [FK_ProfessorID] FOREIGN KEY([ProfessorID])
REFERENCES [dbo].[Professor] ([ProfessorID])
GO
ALTER TABLE [dbo].[Section] CHECK CONSTRAINT [FK_ProfessorID]
GO
ALTER TABLE [dbo].[Section]  WITH CHECK ADD  CONSTRAINT [FK_QuarterID] FOREIGN KEY([QuarterID])
REFERENCES [dbo].[Quarter] ([QuarterID])
GO
ALTER TABLE [dbo].[Section] CHECK CONSTRAINT [FK_QuarterID]
GO
ALTER TABLE [dbo].[SignUpFor]  WITH CHECK ADD  CONSTRAINT [FK_CommunityPerson] FOREIGN KEY([CPPID])
REFERENCES [dbo].[CommunityPartnersPeople] ([CPPID])
GO
ALTER TABLE [dbo].[SignUpFor] CHECK CONSTRAINT [FK_CommunityPerson]
GO
ALTER TABLE [dbo].[TimeEntries]  WITH CHECK ADD  CONSTRAINT [FK_CPP] FOREIGN KEY([CPPID])
REFERENCES [dbo].[CommunityPartnersPeople] ([CPPID])
GO
ALTER TABLE [dbo].[TimeEntries] CHECK CONSTRAINT [FK_CPP]
GO
ALTER TABLE [dbo].[TimeEntries]  WITH CHECK ADD  CONSTRAINT [FK_Opportunity] FOREIGN KEY([OpportunityID])
REFERENCES [dbo].[Opportunity] ([OpportunityID])
GO
ALTER TABLE [dbo].[TimeEntries] CHECK CONSTRAINT [FK_Opportunity]
GO
ALTER TABLE [dbo].[TimeEntries]  WITH CHECK ADD  CONSTRAINT [FK_StudentID] FOREIGN KEY([StudentID])
REFERENCES [dbo].[Student] ([StudentID])
GO
ALTER TABLE [dbo].[TimeEntries] CHECK CONSTRAINT [FK_StudentID]
GO
ALTER TABLE [dbo].[TimeEntries]  WITH CHECK ADD  CONSTRAINT [FK_TimeDateApproval] FOREIGN KEY([OpportunityID], [StudentID])
REFERENCES [dbo].[SignUpFor] ([OpportunityID], [StudentID])
GO
ALTER TABLE [dbo].[TimeEntries] CHECK CONSTRAINT [FK_TimeDateApproval]
GO
ALTER TABLE [eserve].[CommunityPartnerStaff]  WITH CHECK ADD  CONSTRAINT [FK_eserve.CommunityPartnerStaff_eserve.CommunityPartners_CommunityPartner_ID] FOREIGN KEY([CommunityPartner_ID])
REFERENCES [eserve].[CommunityPartners] ([AspNetUser_ID])
GO
ALTER TABLE [eserve].[CommunityPartnerStaff] CHECK CONSTRAINT [FK_eserve.CommunityPartnerStaff_eserve.CommunityPartners_CommunityPartner_ID]
GO
ALTER TABLE [eserve].[Map_Opportunities_FocusAreas]  WITH CHECK ADD  CONSTRAINT [FK_eserve.Map_Opportunities_FocusAreas_eserve.Opp_FocusArea_FocusArea_ID] FOREIGN KEY([FocusArea_ID])
REFERENCES [eserve].[Opp_FocusArea] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [eserve].[Map_Opportunities_FocusAreas] CHECK CONSTRAINT [FK_eserve.Map_Opportunities_FocusAreas_eserve.Opp_FocusArea_FocusArea_ID]
GO
ALTER TABLE [eserve].[Map_Opportunities_FocusAreas]  WITH CHECK ADD  CONSTRAINT [FK_eserve.Map_Opportunities_FocusAreas_eserve.Opportunities_Opportunity_ID] FOREIGN KEY([Opportunity_ID])
REFERENCES [eserve].[Opportunities] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [eserve].[Map_Opportunities_FocusAreas] CHECK CONSTRAINT [FK_eserve.Map_Opportunities_FocusAreas_eserve.Opportunities_Opportunity_ID]
GO
ALTER TABLE [eserve].[Map_Opportunities_Students]  WITH CHECK ADD  CONSTRAINT [FK_eserve.Map_Opportunities_Students_eserve.Opportunities_Opportunity_ID] FOREIGN KEY([Opportunity_ID])
REFERENCES [eserve].[Opportunities] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [eserve].[Map_Opportunities_Students] CHECK CONSTRAINT [FK_eserve.Map_Opportunities_Students_eserve.Opportunities_Opportunity_ID]
GO
ALTER TABLE [eserve].[Map_Opportunities_Students]  WITH CHECK ADD  CONSTRAINT [FK_eserve.Map_Opportunities_Students_eserve.Students_Student_ID] FOREIGN KEY([Student_ID])
REFERENCES [eserve].[Students] ([AspNetUser_ID])
ON DELETE CASCADE
GO
ALTER TABLE [eserve].[Map_Opportunities_Students] CHECK CONSTRAINT [FK_eserve.Map_Opportunities_Students_eserve.Students_Student_ID]
GO
ALTER TABLE [eserve].[Map_Students_Ethinicities]  WITH CHECK ADD  CONSTRAINT [FK_eserve.Map_Students_Ethinicities_eserve.Ethinicities_Ethinicity_ID] FOREIGN KEY([Ethinicity_ID])
REFERENCES [eserve].[Ethinicities] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [eserve].[Map_Students_Ethinicities] CHECK CONSTRAINT [FK_eserve.Map_Students_Ethinicities_eserve.Ethinicities_Ethinicity_ID]
GO
ALTER TABLE [eserve].[Map_Students_Ethinicities]  WITH CHECK ADD  CONSTRAINT [FK_eserve.Map_Students_Ethinicities_eserve.Students_Student_ID] FOREIGN KEY([Student_ID])
REFERENCES [eserve].[Students] ([AspNetUser_ID])
ON DELETE CASCADE
GO
ALTER TABLE [eserve].[Map_Students_Ethinicities] CHECK CONSTRAINT [FK_eserve.Map_Students_Ethinicities_eserve.Students_Student_ID]
GO
ALTER TABLE [eserve].[Map_Students_OppFocusAreas]  WITH CHECK ADD  CONSTRAINT [FK_eserve.Map_Students_OppFocusAreas_eserve.Opp_FocusArea_FocusArea_ID] FOREIGN KEY([FocusArea_ID])
REFERENCES [eserve].[Opp_FocusArea] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [eserve].[Map_Students_OppFocusAreas] CHECK CONSTRAINT [FK_eserve.Map_Students_OppFocusAreas_eserve.Opp_FocusArea_FocusArea_ID]
GO
ALTER TABLE [eserve].[Map_Students_OppFocusAreas]  WITH CHECK ADD  CONSTRAINT [FK_eserve.Map_Students_OppFocusAreas_eserve.Students_Student_ID] FOREIGN KEY([Student_ID])
REFERENCES [eserve].[Students] ([AspNetUser_ID])
ON DELETE CASCADE
GO
ALTER TABLE [eserve].[Map_Students_OppFocusAreas] CHECK CONSTRAINT [FK_eserve.Map_Students_OppFocusAreas_eserve.Students_Student_ID]
GO
ALTER TABLE [eserve].[Map_Students_OppTypes]  WITH CHECK ADD  CONSTRAINT [FK_eserve.Map_Students_OppTypes_eserve.Opp_Type_Opportunity_ID] FOREIGN KEY([Opportunity_ID])
REFERENCES [eserve].[Opp_Type] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [eserve].[Map_Students_OppTypes] CHECK CONSTRAINT [FK_eserve.Map_Students_OppTypes_eserve.Opp_Type_Opportunity_ID]
GO
ALTER TABLE [eserve].[Map_Students_OppTypes]  WITH CHECK ADD  CONSTRAINT [FK_eserve.Map_Students_OppTypes_eserve.Students_Student_id] FOREIGN KEY([Student_id])
REFERENCES [eserve].[Students] ([AspNetUser_ID])
ON DELETE CASCADE
GO
ALTER TABLE [eserve].[Map_Students_OppTypes] CHECK CONSTRAINT [FK_eserve.Map_Students_OppTypes_eserve.Students_Student_id]
GO
ALTER TABLE [eserve].[Opportunities]  WITH CHECK ADD  CONSTRAINT [FK_eserve.Opportunities_eserve.CommunityPartners_CommunityPartner_ID] FOREIGN KEY([CommunityPartner_ID])
REFERENCES [eserve].[CommunityPartners] ([AspNetUser_ID])
GO
ALTER TABLE [eserve].[Opportunities] CHECK CONSTRAINT [FK_eserve.Opportunities_eserve.CommunityPartners_CommunityPartner_ID]
GO
ALTER TABLE [eserve].[Opportunities]  WITH CHECK ADD  CONSTRAINT [FK_eserve.Opportunities_eserve.CommunityPartnerStaff_CommPartnerStaff_ID] FOREIGN KEY([CommPartnerStaff_ID])
REFERENCES [eserve].[CommunityPartnerStaff] ([ID])
GO
ALTER TABLE [eserve].[Opportunities] CHECK CONSTRAINT [FK_eserve.Opportunities_eserve.CommunityPartnerStaff_CommPartnerStaff_ID]
GO
ALTER TABLE [eserve].[Opportunities]  WITH CHECK ADD  CONSTRAINT [FK_eserve.Opportunities_eserve.Opp_Type_Type_ID] FOREIGN KEY([Type_ID])
REFERENCES [eserve].[Opp_Type] ([ID])
GO
ALTER TABLE [eserve].[Opportunities] CHECK CONSTRAINT [FK_eserve.Opportunities_eserve.Opp_Type_Type_ID]
GO
ALTER TABLE [eserve].[Opportunities]  WITH CHECK ADD  CONSTRAINT [FK_eserve.Opportunities_eserve.Univ_Quarter_Quarter_ID] FOREIGN KEY([Quarter_ID])
REFERENCES [eserve].[Univ_Quarter] ([ID])
GO
ALTER TABLE [eserve].[Opportunities] CHECK CONSTRAINT [FK_eserve.Opportunities_eserve.Univ_Quarter_Quarter_ID]
GO
ALTER TABLE [eserve].[Opportunity_Student_Records]  WITH CHECK ADD  CONSTRAINT [FK_eserve.Opportunity_Student_Records_eserve.Opportunities_Opportunity_ID] FOREIGN KEY([Opportunity_ID])
REFERENCES [eserve].[Opportunities] ([ID])
GO
ALTER TABLE [eserve].[Opportunity_Student_Records] CHECK CONSTRAINT [FK_eserve.Opportunity_Student_Records_eserve.Opportunities_Opportunity_ID]
GO
ALTER TABLE [eserve].[Opportunity_Student_Records]  WITH CHECK ADD  CONSTRAINT [FK_eserve.Opportunity_Student_Records_eserve.Students_Student_id] FOREIGN KEY([Student_id])
REFERENCES [eserve].[Students] ([AspNetUser_ID])
GO
ALTER TABLE [eserve].[Opportunity_Student_Records] CHECK CONSTRAINT [FK_eserve.Opportunity_Student_Records_eserve.Students_Student_id]
GO
ALTER TABLE [eserve].[Opportunity_Student_TimeEntries]  WITH CHECK ADD  CONSTRAINT [FK_eserve.Opportunity_Student_TimeEntries_eserve.Opportunity_Student_Records_Opportunity_ID_Student_ID] FOREIGN KEY([Opportunity_ID], [Student_ID])
REFERENCES [eserve].[Opportunity_Student_Records] ([Opportunity_ID], [Student_id])
GO
ALTER TABLE [eserve].[Opportunity_Student_TimeEntries] CHECK CONSTRAINT [FK_eserve.Opportunity_Student_TimeEntries_eserve.Opportunity_Student_Records_Opportunity_ID_Student_ID]
GO
ALTER TABLE [eserve].[Univ_Courses]  WITH CHECK ADD  CONSTRAINT [FK_eserve.Univ_Courses_eserve.Univ_Quarter_Quarter_ID] FOREIGN KEY([Quarter_ID])
REFERENCES [eserve].[Univ_Quarter] ([ID])
GO
ALTER TABLE [eserve].[Univ_Courses] CHECK CONSTRAINT [FK_eserve.Univ_Courses_eserve.Univ_Quarter_Quarter_ID]
GO
ALTER TABLE [eserve].[Univ_CourseSections]  WITH CHECK ADD  CONSTRAINT [FK_eserve.Univ_CourseSections_eserve.Univ_Courses_Course_ID] FOREIGN KEY([Course_ID])
REFERENCES [eserve].[Univ_Courses] ([ID])
GO
ALTER TABLE [eserve].[Univ_CourseSections] CHECK CONSTRAINT [FK_eserve.Univ_CourseSections_eserve.Univ_Courses_Course_ID]
GO
ALTER TABLE [eserve].[Univ_CourseSections]  WITH CHECK ADD  CONSTRAINT [FK_eserve.Univ_CourseSections_eserve.Univ_Professors_Professor_ID] FOREIGN KEY([Professor_ID])
REFERENCES [eserve].[Univ_Professors] ([ID])
GO
ALTER TABLE [eserve].[Univ_CourseSections] CHECK CONSTRAINT [FK_eserve.Univ_CourseSections_eserve.Univ_Professors_Professor_ID]
GO
/****** Object:  StoredProcedure [dbo].[.spGetAdminInfo]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  CREATE PROCEDURE [dbo].[.spGetAdminInfo]	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		LastName,FirstName,
		Email		
	FROM
	   Admin
END








GO
/****** Object:  StoredProcedure [dbo].[GetOpportunityType]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetOpportunityType]	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		TypeID as OpportunityTypeId,
		Name		
	FROM
		OpportunityType
END






GO
/****** Object:  StoredProcedure [dbo].[SHOWSCHEMA]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SHOWSCHEMA]
	@schemaname		varchar(max)
AS
BEGIN

DECLARE @tablename VARCHAR(max)

DECLARE load_cursor CURSOR FOR 
       SELECT table_name 
	   from information_schema.tables 
       where table_schema = @schemaname
	   and table_type = 'BASE TABLE'

OPEN load_cursor 
FETCH NEXT FROM load_cursor INTO @tablename 

SELECT GETDATE() AS CurrentTime, SYSTEM_USER as 'User'

WHILE @@FETCH_STATUS = 0 
BEGIN 
       SELECT @TABLENAME as TableName
	   SELECT COLUMN_NAME, DATA_TYPE 
		FROM INFORMATION_SCHEMA.COLUMNS
		WHERE TABLE_NAME = @TABLENAME

		SELECT
			trigger_name   = st.name
			,trigger_text   = sc.text
			,create_date    = st.create_date
		FROM sys.triggers st
			JOIN sysobjects so
				ON st.parent_id = so.id
			JOIN syscomments sc
				ON sc.id = st.[object_id]
		WHERE so.name = @TABLENAME
 
       FETCH NEXT FROM load_cursor INTO @TABLENAME 
END 

CLOSE load_cursor 
DEALLOCATE load_cursor 
END




GO
/****** Object:  StoredProcedure [dbo].[spAddCourseSectionToOpportunity]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spAddCourseSectionToOpportunity]	
	@OpportunityID int,
	@CourseSectionIDs varchar(1000)	
AS
/****************************************************************************
This proc is to add the assigned sections to one opportunity to the table
*****************************************************************************/
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO 
	Opportunity_Section (OpportunityID, SectionID)
	SELECT 
		@OpportunityID,
		splitData
	FROM dbo.fnSplitString(@CourseSectionIDs,';') --call function fnSplitString to split the strings to table
	WHERE 		 
		Convert(varchar(10),@OpportunityID) + '_' + splitData
		NOT IN
		(SELECT Convert(varchar(10),OpportunityID) + '_' + Convert(varchar(10),SectionID) FROM Opportunity_Section)

END








GO
/****** Object:  StoredProcedure [dbo].[spAddOpportunity]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
/****************Stored Procedure for adding opportunity ***************/
CREATE PROCEDURE [dbo].[spAddOpportunity]	
	@Name varchar(50)
   ,@Location varchar(50)
   ,@JobDescription varchar(max)	
   ,@Requirements varchar(1000)
   ,@TimeCommittment varchar(9)
   ,@TotalNumberOfSlots int
   ,@OrientationDate datetime
   ,@ResumeRequired varchar(3)   
   ,@MinimumAge varchar(8)
   ,@CRCRequiredByPartner varchar(3)
   ,@CRCRequiredBySU varchar(3)
   ,@LinkToOnlineApp varchar(200)
   ,@TypeID int
   ,@CPID int
   ,@CPPID int
   ,@QuarterID int
   ,@JobHours varchar(20)
   ,@DistanceFromSU varchar(8)   
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Insert Into Opportunity
	(Name
	, Location
	, JobDescription
	, Requirements
	, TimeCommittment
	, TotalNumberOfSlots
	, OrientatioNDate
	, ResumeRequired	
	, MinimumAge
	, CRCRequiredByPartner
	, CRCRequiredBySU
	, LinkToOnlineApp
	, TypeID
	, CPID
	, CPPID
	, QuarterID
	, JobHours
	, DistanceFromSU
	, DateOfCreation
	, Status
	, SupervisorEmail
	)
	Values
	(@Name
	, @Location
	, @JobDescription
	, @Requirements
	, @TimeCommittment
	, @TotalNumberOfSlots
	, @OrientatioNDate
	, @ResumeRequired	
	, @MinimumAge
	, @CRCRequiredByPartner
	, @CRCRequiredBySU
	, @LinkToOnlineApp
	, @TypeID
	, @CPID
	, @CPPID
	, @QuarterID
	, @JobHours
	, @DistanceFromSU
	, GetDate()
	, 'Pending'
	, 'N/A')	
END



GO
/****** Object:  StoredProcedure [dbo].[spAddSignUpFor]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  Create Procedure [dbo].[spAddSignUpFor]
		 
		 @OpportunityId int,
		 @StudentID int,
		 @CPPID int
		 
		 As
		 Insert into [SignUpFor]
		 (OpportunityId,StudentID,CPPID)
		 Values
		 ( @OpportunityId,@StudentID,@CPPID)



GO
/****** Object:  StoredProcedure [dbo].[spApproveOpportunityByAdmin]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spApproveOpportunityByAdmin]	
	@OpportunityId int   
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Update Opportunity
	Set 
		Status = 'Open'
	WHERE 
		OpportunityId = @OpportunityId	
END



GO
/****** Object:  StoredProcedure [dbo].[spDeleteCommunityPartnerPeople]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spDeleteCommunityPartnerPeople]	
	@CPPID int  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Delete FROM CommunityPartnersPeople WHERE CPPID = @CPPID
	
END






GO
/****** Object:  StoredProcedure [dbo].[spDeleteOpportunity]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spDeleteOpportunity]	
	@OpportunityID int  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Delete FROM Opportunity WHERE OpportunityID = @OpportunityID
	
END






GO
/****** Object:  StoredProcedure [dbo].[spDeleteStudentOpportunityRegistration]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE Procedure [dbo].[spDeleteStudentOpportunityRegistration]
	@StudentId int, @OpportunityId int
AS
Begin
  SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM [dbo].[SignUpFor]
	WHERE StudentID=@StudentID 
	AND OpportunityID=@OpportunityID	

End





GO
/****** Object:  StoredProcedure [dbo].[spGetAssignedCourseSection]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetAssignedCourseSection]		
	@OpportunityID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT		
		s.SectionID,
		c.CourseID,
		c.CourseName,
		c.ShortName	AS CourseShortName,
		s.RoomNumber,
		s.ClassHours,
		s.NumberOfSlots,
		q.QuarterName,
		q.ShortName AS QuarterShortName,
		s.SectionName
	FROM		
		Section s	
	INNER JOIN
		Class c
	ON
		s.CourseID = c.CourseID	
	INNER JOIN
		Quarter q
	ON
		s.QuarterID = q.QuarterID
	WHERE
		s.SectionID IN 
		(SELECT SectionID FROM Opportunity_Section WHERE OpportunityID = @OpportunityID)
END







GO
/****** Object:  StoredProcedure [dbo].[spGetClassInfo]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[spGetClassInfo]
   @CourseID int
   as
   Begin
    Select ShortName,CourseName, CourseID from Class
	where CourseID = @CourseID
	End




GO
/****** Object:  StoredProcedure [dbo].[spGetCommunityPartnerAlertview]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[spGetCommunityPartnerAlertview]
As
Begin
Select
AlertID, 
Message 
from CommunityPartnerAlert
End




GO
/****** Object:  StoredProcedure [dbo].[spGetCommunityPartnerOpportunityList]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[spGetCommunityPartnerOpportunityList]	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		cp.CPID,
		cp.OrganizationName AS CommunityPartnerName,
		o.Status,
		count(*) as OpportunityCount
	FROM 
		Opportunity o
	JOIN 
		CommunityPartners cp
	ON 
		o.CPID = cp.CPID
	GROUP BY 
		cp.CPID, o.Status, cp.OrganizationName
END






GO
/****** Object:  StoredProcedure [dbo].[spGetCommunityPartnerPeople]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetCommunityPartnerPeople]	
	@CPID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		CPPID as SupervisorID,
		FirstName,
		LastName,
		Title,
		Phone,
		EmailID
		
	FROM
		CommunityPartnersPeople
	WHERE
		CPID = @CPID
END




GO
/****** Object:  StoredProcedure [dbo].[spGetCommunityPartnerProfile]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetCommunityPartnerProfile]	
	@CPID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		CPID AS CommunityPartnerID,
		OrganizationName,
		Address,
		Website,
		MainPhone,
		MissionStatement,
		WorkDescription
			
	From CommunityPartners
	WHERE
		CPID = @CPID
END




GO
/****** Object:  StoredProcedure [dbo].[spGetCommunityPartners]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetCommunityPartners]	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		CPID AS CommunityPartnerID,
		OrganizationName,
		Address,
		Website,
		MainPhone,
		MissionStatement,
		WorkDescription
			
	From CommunityPartners
END




GO
/****** Object:  StoredProcedure [dbo].[spGetCommunityPartnerStudentView]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[spGetCommunityPartnerStudentView]
As
Begin
Set NoCount On
Select
o.OpportunityID,
o.Name,
s.StudentID,
s.FirstName,
s.LastName,
SignUpStatus,
PartnerEvaluation,
ss.SectionID,
ss.SectionName,
p.ProfessorID,
p.FirstName as ProfessorFirstName,
p.LastName as ProfessorLastName,
vt.TotalHours as TotalHoursVolunteered,
PartnerApprovedHours
from vwTotalVolunteeredHours vt
inner join
SignUpFor
on vt.OpportunityID = SignUpFor.OpportunityID
and vt.StudentID = SignUpFor.StudentID
inner join
Student s
on SignUpFor.StudentID = s.StudentID
   
inner join 
Opportunity o
on SignUpFor.OpportunityID = o.OpportunityID
inner join
Opportunity_Section
on o.OpportunityID = Opportunity_Section.OpportunityID
inner join
Section ss
on Opportunity_Section.SectionID = ss.SectionID
inner join
Professor p
on ss.ProfessorID = p.ProfessorID
 end




GO
/****** Object:  StoredProcedure [dbo].[spGetCourseEvaluationByStudent]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  procedure [dbo].[spGetCourseEvaluationByStudent]
As
Begin
Select
o.OpportunityID,
o.Name,
StudentEvaluation as CourseEvaluationByStudent,
s.StudentID,
s.FirstName,
s.LastName,
SectionName
from Opportunity o 
inner join SignUpFor
on o.OpportunityID = SignUpFor.OpportunityID
inner join
Student s
on SignUpFor.StudentID = s.StudentID
inner join
vwStudentBySection ss
on ss.StudentID = s.StudentID
End




GO
/****** Object:  StoredProcedure [dbo].[spGetEthinicity]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[spGetEthinicity]
	As
	Begin
	Select EthinicityID,Description from Ethinicity
	End




GO
/****** Object:  StoredProcedure [dbo].[spGetFocusArea]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGetFocusArea]	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		FocusAreaId,
		AreaName		
	FROM
		FocusArea
END






GO
/****** Object:  StoredProcedure [dbo].[spGetOpportunityByDate]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[spGetOpportunityByDate]
@DateOfCreation date
as
Begin
  Select OpportunityId,Name,DateOfCreation from Opportunity where DateOfCreation = @DateOfCreation 
End




GO
/****** Object:  StoredProcedure [dbo].[spGetOpportunityById]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[spGetOpportunityById]
	@OpportunityId int
AS
Begin
  SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		OpportunityID,
		Name,
		Location,
		DateOfCreation,
		JobDescription,
		Requirements,
		TimeCommittment,
		TotalNumberOfSlots,
		OrientationDate,
		Status,
		ResumeRequired,
		'' AS SupervisorEmail,
		MinimumAge,
		CRCRequiredByPartner,
		CRCRequiredBySU,
		LinkToOnlineApp,	
		TypeID,
		CPID,
		CPPID,
		QuarterID,		
		JobHours,
		DistanceFromSU
	FROM
		Opportunity  
	WHERE 
		OpportunityId = @OpportunityId
End






GO
/****** Object:  StoredProcedure [dbo].[spGetOpportunityByStatus]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGetOpportunityByStatus]
	-- Add the parameters for the stored procedure here
	@Status nvarchar (8)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT OpportunityId,Name,Status from Opportunity
	where Status = @Status
END




GO
/****** Object:  StoredProcedure [dbo].[spGetOpportunityDetailByOpportunityId]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE Procedure [dbo].[spGetOpportunityDetailByOpportunityId]
	@OpportunityId int
AS
Begin
  SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT CP.[CPID]
		,[OpportunityID]
      ,[OrganizationName]
      ,[WorkDescription] OrganizationDesc
	  ,[Name] OpportunityName
	  ,[JobDescription] OpportunityDesc
	  ,[Address] Location
	  ,[TimeCommittment]
	  ,CPP.[FirstName]+ ', ' + CPP.[LastName] SiteSupervisorName
	  ,[SupervisorEmail]
	  ,[CRCRequiredByPartner]
	  ,[MinimumAge] 
	  ,[ResumeRequired]
      ,[Website] Link
      ,[MainPhone]           
  FROM [dbo].[CommunityPartners] CP
  INNER JOIN [dbo].[Opportunity] O ON (O.CPID=CP.CPID)
  INNER JOIN [dbo].[CommunityPartnersPeople] CPP ON (CPP.CPPID=O.CPPID)
  WHERE [OpportunityID]=@OpportunityId

End





GO
/****** Object:  StoredProcedure [dbo].[spGetOpportunityList]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetOpportunityList]	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		OpportunityID,
		o.Name,
		Location,
		DateOfCreation,
		JobDescription,
		Requirements,
		TimeCommittment,
		TotalNumberOfSlots,
		OrientationDate,
		Status,
		ResumeRequired,		
		MinimumAge,
		CRCRequiredByPartner,
		CRCRequiredBySU,
		LinkToOnlineApp,
		ot.Name AS OpportunityTypeName,
		q.QuarterName,
		cp.OrganizationName,
		cpp.FirstName + ' ' + cpp.LastName AS Supervisor,
		cpp.EmailId as SupervisorEmail,
		DateOfCreation AS DateApproved,
		JobHours,
		DistanceFromSU as DistanceFromSU
	FROM
		Opportunity o
	INNER JOIN
		OpportunityType ot
	ON
		o.TypeID = ot.TypeID
	INNER JOIN
		CommunityPartners cp
	ON
		o.CPID = cp.CPID
	INNER JOIN
		[dbo].[CommunityPartnersPeople] cpp
	ON
		o.CPPID = cpp.CPPID
	INNER JOIN
		Quarter q
	ON
		o.QuarterID = q.QuarterID

END






GO
/****** Object:  StoredProcedure [dbo].[spGetOpportunityListByStudentId]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE Procedure [dbo].[spGetOpportunityListByStudentId]
	@StudentId int
AS
Begin
  SET NOCOUNT ON;
  
    -- Insert statements for procedure here
	SELECT O.[OpportunityID]
      ,[Name] 'Position'
	  ,CP.[OrganizationName] 'Organization'
	  ,5 'SlotsAvailable'
	  ,O.[DistanceFromSU] + ' miles' As 'DistanceFromSU'
      ,[Location]
      ,[TimeCommittment]
      ,[MinimumAge]
      ,CASE 
	    WHEN [CRCRequiredByPartner] = 'Yes' OR [CRCRequiredBySU] = 'Yes' THEN 'Yes' 
		ELSE 'No'
		END AS 'CRCRequired'
	  ,[JobDescription]
  FROM [dbo].[Opportunity] O
  INNER JOIN [dbo].[CommunityPartners] CP ON (CP.CPID=O.CPID) 
  INNER JOIN [dbo].[Opportunity_Section] OS ON (OS.OpportunityID=O.OpportunityID)
  INNER JOIN [dbo].[Student_Section] SS ON (SS.SectionID=OS.SectionID)
  WHERE O.Status<>'Pending' AND StudentID=@StudentId

End





GO
/****** Object:  StoredProcedure [dbo].[spGetOpportunityListForAdmin]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetOpportunityListForAdmin]	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		OpportunityID,
		cp.OrganizationName,
		o.Name,				
		TotalNumberOfSlots,		
		Status,		
		ot.Name AS OpportunityTypeName,		
		cpp.FirstName + ' ' + cpp.LastName AS Supervisor,
		SupervisorEmail,
		DateOfCreation AS DateApproved	
	FROM
		Opportunity o
	INNER JOIN
		OpportunityType ot
	ON
		o.TypeID = ot.TypeID
	INNER JOIN
		CommunityPartners cp
	ON
		o.CPID = cp.CPID
	INNER JOIN
		[dbo].[CommunityPartnersPeople] cpp
	ON
		o.CPPID = cpp.CPPID	
END



GO
/****** Object:  StoredProcedure [dbo].[spGetOpportunityListForFaculty]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetOpportunityListForFaculty]	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		OpportunityID,
		cp.OrganizationName,
		o.Name,				
		TotalNumberOfSlots,		
		Status,		
		ot.Name AS OpportunityTypeName,		
		cpp.FirstName + ' ' + cpp.LastName AS Supervisor,
		SupervisorEmail,
		DateOfCreation AS DateApproved		
	FROM
		Opportunity o
	INNER JOIN
		OpportunityType ot
	ON
		o.TypeID = ot.TypeID
	INNER JOIN
		CommunityPartners cp
	ON
		o.CPID = cp.CPID
	INNER JOIN
		[dbo].[CommunityPartnersPeople] cpp
	ON
		o.CPPID = cpp.CPPID	
END



GO
/****** Object:  StoredProcedure [dbo].[spGetOpportunityRegisteredByStudentId]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE Procedure [dbo].[spGetOpportunityRegisteredByStudentId]
	@StudentId int
AS
Begin
  SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT O.OpportunityID
	  ,SUF.[StudentId]
	  ,O.Name 'Opportunity'
	  ,C.[CourseName]
	  ,Q.QuarterName 'Quarter'
	  ,CP.OrganizationName 'Organization'
	  ,CPP.EmailId
      ,[SignUpStatus] 'Status'		
	  ,'NEED' [HoursVolunteered]
      ,'NEED' [PartnerEvaluation]
	  ,'NEED' [StudentEvaluation]
	  ,'NEED' [StudentReflection]      	  
  FROM [dbo].[SignUpFor] SUF
  INNER JOIN [dbo].[Opportunity] O ON (O.OpportunityID=SUF.OpportunityID)  
  INNER JOIN [dbo].[Opportunity_Section] OS ON (OS.OpportunityID=O.OpportunityID)
  INNER JOIN [dbo].[CommunityPartnersPeople] CPP ON (CPP.CPPID=SUF.CPPID)
  INNER JOIN [dbo].[CommunityPartners] CP ON (CP.CPID=CPP.CPID)
  RIGHT JOIN [dbo].[Quarter] Q ON (Q.QuarterID=O.QuarterID)   
  RIGHT JOIN [dbo].[Section] S ON (S.SectionID=OS.SectionID)
  RIGHT JOIN [dbo].[Class] C ON (C.CourseID=S.CourseID)
  WHERE StudentID=@StudentId AND OS.SectionID=1
END









GO
/****** Object:  StoredProcedure [dbo].[spGetOpportunitySectionList]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetOpportunitySectionList]	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		o.Name as OpportunityName,
		cp.OrganizationName,
		s.SectionName,
		c.CourseName,
		p.FirstName + ' ' + p.LastName as ProfessorName,
		q.QuarterName,
		s.NumberOfSlots
	FROM
		Opportunity_Section os
	INNER JOIN
		Section s
	ON
		os.SectionID = s.SectionID
	INNER JOIN
		Opportunity o
	ON
		os.OpportunityID = o.OpportunityID
	INNER JOIN
		CommunityPartners cp
	ON
		o.CPID = cp.CPID
	INNER JOIN
		Class c
	ON
		s.CourseID = c.CourseID
	INNER JOIN
		Professor p
	ON
		s.ProfessorID = p.ProfessorID
	INNER JOIN
		Quarter q
	ON
		s.QuarterID = q.QuarterID

END





GO
/****** Object:  StoredProcedure [dbo].[spGetOpportunityType]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetOpportunityType]	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		TypeID as OpportunityTypeID,
		Name		
	FROM
		OpportunityType
END



GO
/****** Object:  StoredProcedure [dbo].[spGetProfile]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGetProfile]	
	@StudentId int  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT S.[StudentID]
      ,[FirstName]
      ,[LastName]      
	  ,[PreferedName]
	  ,[DateOfBirth]
      ,[Gender]
	  --,E.Description As 'Ethinicity'      
	  --,FA.AreaName
      ,[InternationalStudent]
      ,[LastBackgroundCheck]
  FROM [dbo].[Student] S 
  --LEFT JOIN [dbo].[Student_Ethinicity] SE ON (SE.StudentID=S.StudentID)
  --INNER JOIN [dbo].[Ethinicity] E ON (E.EthinicityID=SE.EthinicityID) 
  --LEFT JOIN [dbo].[Student_FocusArea] SFA ON (SFA.StudentID=S.StudentID) 
  --INNER JOIN [dbo].[FocusArea] FA ON (FA.FocusAreaID=SFA.FocusAreaID)  
  where StudentID=@StudentId
	
END











GO
/****** Object:  StoredProcedure [dbo].[spGetProfileEthinicity]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGetProfileEthinicity]	
	@StudentId int  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT S.[StudentID]
	  ,E.Description As 'Ethinicity'      	        
  FROM [dbo].[Student] S 
  LEFT JOIN [dbo].[Student_Ethinicity] SE ON (SE.StudentID=S.StudentID)
  INNER JOIN [dbo].[Ethinicity] E ON (E.EthinicityID=SE.EthinicityID)   
  where S.StudentID=@StudentId
	
END











GO
/****** Object:  StoredProcedure [dbo].[spGetProfileFocusAreas]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGetProfileFocusAreas]	
	@StudentId int  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT S.[StudentID]     
	  ,FA.AreaName      
  FROM [dbo].[Student] S   
  LEFT JOIN [dbo].[Student_FocusArea] SFA ON (SFA.StudentID=S.StudentID) 
  INNER JOIN [dbo].[FocusArea] FA ON (FA.FocusAreaID=SFA.FocusAreaID)  
  where S.StudentID=@StudentId
	
END











GO
/****** Object:  StoredProcedure [dbo].[spGetQuarter]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGetQuarter]	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		QuarterID,
		QuarterName,
		ShortName,
		StartDate,
		EndDate				
	FROM
		Quarter
END






GO
/****** Object:  StoredProcedure [dbo].[spGetSectionInfo]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[spGetSectionInfo]
 @SectionID int ,
 @QuarterID int
 as
 Begin
   Select SectionID,QuarterID,NumberOfSlots,SectionName from Section 
   where SectionID = @SectionID
   and  QuarterID = @QuarterID
   End




GO
/****** Object:  StoredProcedure [dbo].[spGetSignUpStatus]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[spGetSignUpStatus]
@StudentID int,
@OpportunityID int
as
Begin
Select OpportunityID,StudentID, SignUpStatus
from SignUpFor
End




GO
/****** Object:  StoredProcedure [dbo].[spGetStudentCourseOppDetailForFaculty]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetStudentCourseOppDetailForFaculty]
	-- Parameter for the stored procedure: professorID
	@ProfessorID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		OpportunityID,
		cp.OrganizationName,
		o.Name,				
		TotalNumberOfSlots,		
		Status,		
		ot.Name AS OpportunityTypeName,		
		cpp.FirstName + ' ' + cpp.LastName AS Supervisor,
		DateOfCreation AS DateApproved		
	FROM
		Opportunity o
	INNER JOIN
		OpportunityType ot
	ON
		o.TypeID = ot.TypeID
	INNER JOIN
		CommunityPartners cp
	ON
		o.CPID = cp.CPID
	INNER JOIN
		[dbo].[CommunityPartnersPeople] cpp
	ON
		o.CPPID = cpp.CPPID	
END



GO
/****** Object:  StoredProcedure [dbo].[spGetStudentProfile]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGetStudentProfile]	
	@StudentId int  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT S.[StudentID]
      ,[FirstName]
      ,[LastName]      
	  ,[PreferedName]
	  ,[DateOfBirth]
      ,[Gender]
	  --,E.Description As 'Ethinicity'      
	  --,FA.AreaName
      ,[InternationalStudent]
      ,[LastBackgroundCheck]
  FROM [dbo].[Student] S 
  --LEFT JOIN [dbo].[Student_Ethinicity] SE ON (SE.StudentID=S.StudentID)
  --INNER JOIN [dbo].[Ethinicity] E ON (E.EthinicityID=SE.EthinicityID) 
  --LEFT JOIN [dbo].[Student_FocusArea] SFA ON (SFA.StudentID=S.StudentID) 
  --INNER JOIN [dbo].[FocusArea] FA ON (FA.FocusAreaID=SFA.FocusAreaID)  
  where StudentID=@StudentId
	
END






GO
/****** Object:  StoredProcedure [dbo].[spGetSupervisor]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetSupervisor]	
	@CPPID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		CPPID as SupervisorID,
		FirstName,
		LastName,
		Title,
		Phone,
		EmailID
		
	FROM
		CommunityPartnersPeople
	WHERE
		CPPID = @CPPID
END




GO
/****** Object:  StoredProcedure [dbo].[spGetUnAssignedCourseSection]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






CREATE PROCEDURE [dbo].[spGetUnAssignedCourseSection]	
	@OpportunityID int	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT		
		s.SectionID,
		c.CourseID,
		c.CourseName,
		c.ShortName	AS CourseShortName,
		s.RoomNumber,
		s.ClassHours,
		s.NumberOfSlots,
		q.QuarterName,
		q.ShortName AS QuarterShortName,
		s.SectionName
	FROM		
		Section s	
	INNER JOIN
		Class c
	ON
		s.CourseID = c.CourseID	
	INNER JOIN
		Quarter q
	ON
		s.QuarterID = q.QuarterID
	WHERE
		s.SectionID NOT IN 
		(SELECT SectionID FROM Opportunity_Section WHERE OpportunityID = @OpportunityID)
END







GO
/****** Object:  StoredProcedure [dbo].[spOpportunity_FocusArea]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 Create Procedure [dbo].[spOpportunity_FocusArea]
		 (
		 @OpportunityID int,
		 @FocusAreaID int
		 )
		 As
		 Insert into [Opportunity_FocusArea]
		 Values ( @OpportunityID,@FocusAreaID)
		 



GO
/****** Object:  StoredProcedure [dbo].[spOpportunity_Section]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[spOpportunity_Section]
		
		 (
		 @OpportunityID int,
		 @SectionID int
		 )
		 As
		 Insert into [Opportunity_Section]
		 Values ( @OpportunityID,@SectionID)



GO
/****** Object:  StoredProcedure [dbo].[spOpportunityByDistance]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[spOpportunityByDistance]
@DistanceFromSU varchar (8)
as
Begin
  Select OpportunityID,Name,Location,DistanceFromSU from Opportunity where DistanceFromSU = @DistanceFromSU
  End




GO
/****** Object:  StoredProcedure [dbo].[spOpportunityByQuarter]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create Procedure  [dbo].[spOpportunityByQuarter]
  @QuarterID int
  As
  Begin
  Select OpportunityId,Name, QuarterID from Opportunity
  where 
  QuarterID = @QuarterID
  End




GO
/****** Object:  StoredProcedure [dbo].[spOpportunityByType]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 Create procedure [dbo].[spOpportunityByType]
  @OpportunityType varchar (20)
  as
  Begin
       Select OpportunityId, Name,TypeID from Oppotunity where TypeID = @OpportunityType
	   End



GO
/****** Object:  StoredProcedure [dbo].[spRegisterStudentOpportunity]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE Procedure [dbo].[spRegisterStudentOpportunity]
	@StudentId int, @OpportunityId int
AS
Begin
  SET NOCOUNT ON;

    -- Insert statements for procedure here
DECLARE @CPPID int
SELECT @CPPID = (SELECT CPPID FROM [dbo].[Opportunity] WHERE OpportunityId=@OpportunityId)

BEGIN
	IF NOT EXISTS (SELECT [StudentID]
						  ,[CPPID]
						  ,[OpportunityID]
					  FROM [dbo].[SignUpFor]
					  WHERE [StudentID]=@StudentId
						AND [CPPID]=@CPPID
						AND [OpportunityID]=@OpportunityId
					)
	BEGIN
	INSERT INTO [dbo].[SignUpFor] ([StudentID]
								  ,[CPPID]
								  ,[OpportunityID]
								  ,[SignUpStatus]
								  )
	VALUES (@StudentId, @CPPID, @OpportunityId, 'Pending')
	END
END
		

End







GO
/****** Object:  StoredProcedure [dbo].[spRegisterStudentOpportunityTEST]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[spRegisterStudentOpportunityTEST]
	@StudentId int, @OpportunityId int
AS
Begin
	SET NOCOUNT ON;
	DECLARE @CPPID NVARCHAR(30)  -- ID of the community partner person assigned
	-- Retrieve the ID of the community partner person assigned to this opportunity
	SELECT @CPPID =  CPPID 
			FROM [dbo].[Opportunity] 
			WHERE OpportunityId=@OpportunityId

	BEGIN
	-- First make sure the student is not already signed up
	-- for this opportunity
		IF EXISTS (SELECT *
			FROM [dbo].[SignUpFor]
			WHERE [StudentID]=@StudentId
				AND [CPPID]=@CPPID
				AND [OpportunityID]=@OpportunityId)
		-- if this comes back true, the student is already enrolled
		RETURN 1  -- CALLER should check return value - if 1 student already signed up
		else -- student is not already assigned
		begin
		-- first make sure there is still an open slot
		-- and lock the row to make sure no one else gets it
			begin transaction
			declare	@slots int
			-- this statement sees if there are any slots available 
			--AND locks the opportunity row so no one else can change it
			select @slots = totalnumberofslots 
				from [dbo].[Opportunity]  WITH (updlock)
				where opportunityid = @opportunityid;
			if (@slots = 0 ) -- there are no slots left, rollback with error message
				begin
					rollback
					return 2 -- 2 indicates NOT ADDED - no slots left open
				end
			-- ELSE
			-- there are slots so go ahead and do the insert
			-- this will fire the trigger on OPPORTUNITY to decrement slots
			INSERT INTO [dbo].[SignUpFor] ([StudentID]
								  ,[CPPID]
								  ,[OpportunityID]
								  ,[SignUpStatus])
				VALUES (@StudentId, @CPPID, @OpportunityId, 'Pending')
			commit  -- commit the transaction and release the locks
		end
	END
		return 0 -- 0 indicates success
End




GO
/****** Object:  StoredProcedure [dbo].[spRemoveCourseSectionFromOpportunity]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spRemoveCourseSectionFromOpportunity]	
	@OpportunityID int,
	@CourseSectionIDs varchar(1000)	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM
		Opportunity_Section
	WHERE 
		OpportunityID = @OpportunityID
	AND
		SectionID in
		(SELECT splitData FROM dbo.fnSplitString(@CourseSectionIDs,';'))

END





GO
/****** Object:  StoredProcedure [dbo].[spSignUpFor]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 Create Procedure [dbo].[spSignUpFor]
		 (
		 @StudentID int,
		 @OpportunityID int
		 )
		 As
		 Begin
		 Select @StudentID,@OpportunityID,CPPID,SignUpStatus,PartnerEvaluation,StudentReflection,StudentEvaluation
		 from SignUpFor
		 where StudentID = @StudentID
		 and OpportunityID = @OpportunityID
		 End




GO
/****** Object:  StoredProcedure [dbo].[spStudent_Ethinicity]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 Create Procedure [dbo].[spStudent_Ethinicity]
		 (
		 @EthinicityID int,
		 @StudentID int
		 )
		 As
		 Insert into [Student_Ethinicity]
		 Select @EthinicityID,@StudentID



GO
/****** Object:  StoredProcedure [dbo].[spStudent_FocusArea]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 Create Procedure [dbo].[spStudent_FocusArea]
		 (
		 @FocusAreaID int,
		 @StudentID int
		 )
		 As
		 Insert into [Student_FocusArea]
		 Select @FocusAreaID,@StudentID



GO
/****** Object:  StoredProcedure [dbo].[spStudent_Section]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 Create Procedure [dbo].[spStudent_Section]
		 (
		 @StudentID int,
		 @SectionID int
		 )
		 As
		 Insert into [Student_Section]
		 Select @StudentID,@SectionID



GO
/****** Object:  StoredProcedure [dbo].[spUpdateCommunityPartnerInfo]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[spUpdateCommunityPartnerInfo]
 
@CPID int,
@OrganizationName nvarchar (150),
@Address nvarchar (500),
@Website nvarchar(500),
@MainPhone nvarchar (10),
@MissionStatement nvarchar (50),
@WorkDescription nvarchar (500)
As
Begin
Update communitypartners Set OrganizationName = @OrganizationName,
Address = @Address,
Website = @Website,
MainPhone = @MainPhone,
MissionStatement = @MissionStatement,
WorkDescription = @WorkDescription
where @CPID = CPID
END




GO
/****** Object:  StoredProcedure [dbo].[spUpdateCommunityPartnersPeople]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[spUpdateCommunityPartnersPeople]
@CPPID int,
@FirstName nvarchar (15),
@LastName nvarchar (15),
@Title nvarchar (15),
@Phone nvarchar (15),
@EmailID nvarchar (50)
As
Begin
Update CommunityPartnersPeople Set FirstName = @FirstName,
LastName = @LastName,
Title = @Title,
Phone = @Phone,
EmailID = @EmailID
where 
CPPID = @CPPID
End




GO
/****** Object:  StoredProcedure [dbo].[spUpdateOpportunity]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spUpdateOpportunity]	
	@OpportunityId int
   ,@Name varchar(50)
   ,@Location varchar(50)
   ,@JobDescription varchar(max)	
   ,@Requirements varchar(1000)
   ,@TimeCommittment varchar(9)
   ,@TotalNumberOfSlots int
   ,@OrientationDate datetime
   ,@ResumeRequired varchar(3)   
   ,@MinimumAge varchar(8)
   ,@CRCRequiredByPartner varchar(3)
   ,@CRCRequiredBySU varchar(3)
   ,@LinkToOnlineApp varchar(200)
   ,@TypeID int
   ,@CPID int
   ,@CPPID int
   ,@QuarterID int
   ,@JobHours varchar(20)
   ,@DistanceFromSU varchar(8)   
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Update Opportunity
	Set Name = @Name
		, Location = @Location
		, JobDescription = @JobDescription
		, Requirements = @Requirements
		, TimeCommittment = @TimeCommittment
		, TotalNumberOfSlots = @TotalNumberOfSlots
		, OrientatioNDate = @OrientatioNDate
		, ResumeRequired = @ResumeRequired		
		, MinimumAge = @MinimumAge
		, CRCRequiredByPartner = @CRCRequiredByPartner
		, CRCRequiredBySU = @CRCRequiredBySU
		, LinkToOnlineApp = @LinkToOnlineApp
		, TypeID = @TypeID
		, CPID = @CPID
		, CPPID = @CPPID
		, QuarterID = @QuarterID
		, JobHours = @JobHours
		, DistanceFromSU = @DistanceFromSU
	WHERE 
		OpportunityId = @OpportunityId	
END








GO
/****** Object:  StoredProcedure [dbo].[spUpdateSectionInfo]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[spUpdateSectionInfo]
   @SectionID int,
   @NumberOfSlots int
   as
   Begin
   Update Section Set NumberOfSlots = @NumberOfSlots
   where SectionID = @SectionID

   End




GO
/****** Object:  StoredProcedure [dbo].[spUpdateSignUpFor]    Script Date: 5/25/2016 5:36:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[spUpdateSignUpFor]
   @StudentID int,
   @OpportunityID int,
   @CPPID int,
   @SignUpStatus nvarchar (8),
   @StudentReflection nvarchar (max),
   @PartnerEvaluation nvarchar (max),
   @StudentEvaluation nvarchar (max)
   as
   Begin
   Update SignUpFor Set SignUpStatus = @SignUpStatus,
     StudentReflection = @StudentReflection,
	 PartnerEvaluation = @PartnerEvaluation,
	 StudentEvaluation = @StudentEvaluation
   where StudentID = @StudentID 
   and 
         OpportunityID = @OpportunityID
  and 
         CPPID = @CPPID

   End




GO
USE [master]
GO
ALTER DATABASE [eServe] SET  READ_WRITE 
GO
