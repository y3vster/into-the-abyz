USE [eServe]
GO
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'6cea2fa0-634f-4f98-a128-3604b19a2a9f', N'Student@eServeSU.net', 0, N'ABN379Gs04z/NSZ25YH33GczVO+C4osOaRPK18BvRwHCZxH+VfgNdFryuux+SjBx3w==', N'2b17a07a-4d69-4f7a-93f7-a2c99a8815a5', NULL, 0, 0, NULL, 1, 0, N'Student@eServeSU.net')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'8cba18d7-3e6f-454c-8e7d-ea8a82c6e53b', N'Faculty@eServeSU.net', 0, N'AKXjnuO24ynInX6o4d/wB4dd5X5XcUrtdOotBOh8tUfyBasvDQjXkVGZXNGibDOSAA==', N'4690084e-5a13-4b84-9aa6-19737607279b', NULL, 0, 0, NULL, 1, 0, N'Faculty@eServeSU.net')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'a95a2ef2-5ac5-4c84-b87b-446627d3eb8d', N'Unknown@eServeSU.net', 0, N'AFppovpvklVZkarp4juugr+Ehpn7BjM1sjo0s6NdSWvHVanjS3DD3cKiFVIuxePJ7g==', N'8ac4f4e4-a6b9-4f10-a0c7-b98c071b6c56', NULL, 0, 0, NULL, 1, 0, N'Unknown@eServeSU.net')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'd9092ce2-653e-4212-88e1-2eded2e3dfd4', N'Admin@eServeSU.net', 0, N'AG8oefCd5vr00RoEy4WiNDGSEkuf6cmhKdjY9FonZ8cPvR+CgaokXpPbDiF8FjR//w==', N'6d5f606f-75b8-4296-b034-49446788010e', NULL, 0, 0, NULL, 1, 0, N'Admin@eServeSU.net')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'e9bacbbb-a362-48b2-9eff-91c660efd167', N'Partner@eServeSU.net', 0, N'AIYRG/4A8m7Zbpzu6UOK44pzO+FH/gQqG8kPBn1LawhVI5vJF3Zy/37sH6u3hDY6CQ==', N'd3f1e486-a896-4b56-a0a4-5dac29247aaa', NULL, 0, 0, NULL, 1, 0, N'Partner@eServeSU.net')
SET IDENTITY_INSERT [dbo].[Quarter] ON 

INSERT [dbo].[Quarter] ([QuarterID], [QuarterName], [ShortName], [StartDate], [EndDate]) VALUES (1, N'Spring 2016', N'16SQ', CAST(N'2016-01-01' AS Date), CAST(N'2016-06-01' AS Date))
INSERT [dbo].[Quarter] ([QuarterID], [QuarterName], [ShortName], [StartDate], [EndDate]) VALUES (4, N'Summer 2016', N'16SU', CAST(N'2016-07-01' AS Date), CAST(N'2016-09-01' AS Date))
SET IDENTITY_INSERT [dbo].[Quarter] OFF
SET IDENTITY_INSERT [dbo].[CommunityPartners] ON 

INSERT [dbo].[CommunityPartners] ([CPID], [OrganizationName], [Address], [Website], [MainPhone], [MissionStatement], [WorkDescription]) VALUES (1, N'Kamenski Evil Industries', N'1004 Broadway', N'seattleu.edu', N'2060000000', N'We are the best!', N'We work hard.')
SET IDENTITY_INSERT [dbo].[CommunityPartners] OFF
SET IDENTITY_INSERT [dbo].[CommunityPartnersPeople] ON 

INSERT [dbo].[CommunityPartnersPeople] ([CPPID], [CPID], [FirstName], [LastName], [Title], [Phone], [EmailID], [Password]) VALUES (1, 1, N'Yev', N'K', N'Mr', N'2060000000', N'kamenski@seattleu.edu', N'password')
SET IDENTITY_INSERT [dbo].[CommunityPartnersPeople] OFF
SET IDENTITY_INSERT [dbo].[OpportunityType] ON 

INSERT [dbo].[OpportunityType] ([TypeID], [Name]) VALUES (3, N'Community Service')
INSERT [dbo].[OpportunityType] ([TypeID], [Name]) VALUES (2, N'Internship')
INSERT [dbo].[OpportunityType] ([TypeID], [Name]) VALUES (5, N'Mentorship Opportunity')
INSERT [dbo].[OpportunityType] ([TypeID], [Name]) VALUES (4, N'Other Volunteering')
SET IDENTITY_INSERT [dbo].[OpportunityType] OFF
SET IDENTITY_INSERT [dbo].[Opportunity] ON 

INSERT [dbo].[Opportunity] ([OpportunityID], [Name], [Location], [DateOfCreation], [JobDescription], [Requirements], [TimeCommittment], [TotalNumberOfSlots], [OrientationDate], [Status], [ResumeRequired], [MinimumAge], [CRCRequiredByPartner], [CRCRequiredBySU], [LinkToOnlineApp], [SupervisorEmail], [TypeID], [CPID], [QuarterID], [CPPID], [JobHours], [DistanceFromSU]) VALUES (3, N'weft', N'qwefqwf', CAST(N'2016-05-20' AS Date), N'weave', N'acvaev', N'1 quarter', 12, CAST(N'2016-12-12 00:00:00.0000000' AS DateTime2), N'Pending', N'Yes', N'5', N'Yes', N'No', N'dsffef', N'N/A', 3, 1, 1, 1, N'12', N'5')
SET IDENTITY_INSERT [dbo].[Opportunity] OFF
INSERT [dbo].[Student] ([StudentID], [DateOfBirth], [FirstName], [LastName], [PreferedName], [EmailID], [Password], [Gender], [InternationalStudent], [LastBackgroundCheck]) VALUES (106288, CAST(N'2016-01-01' AS Date), N'Yev', N'K', N'Yev', N'1', N'1', N'Male', N'No', NULL)
SET IDENTITY_INSERT [dbo].[FocusArea] ON 

INSERT [dbo].[FocusArea] ([FocusAreaID], [AreaName]) VALUES (6, N'Aging and Disabilities')
INSERT [dbo].[FocusArea] ([FocusAreaID], [AreaName]) VALUES (1, N'Children and Youth')
INSERT [dbo].[FocusArea] ([FocusAreaID], [AreaName]) VALUES (4, N'Hunger and Homelessness')
INSERT [dbo].[FocusArea] ([FocusAreaID], [AreaName]) VALUES (2, N'Intercultural Connections')
INSERT [dbo].[FocusArea] ([FocusAreaID], [AreaName]) VALUES (7, N'SU Youth Initiative')
INSERT [dbo].[FocusArea] ([FocusAreaID], [AreaName]) VALUES (5, N'Sustainability')
INSERT [dbo].[FocusArea] ([FocusAreaID], [AreaName]) VALUES (3, N'Working with Families')
SET IDENTITY_INSERT [dbo].[FocusArea] OFF
