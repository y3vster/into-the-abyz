USE [eServe]
GO
/****** Object:  User [eserve2]    Script Date: 5/25/2016 5:18:05 PM ******/
CREATE USER [eserve2] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [eserve2]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_diagramobjects]    Script Date: 5/25/2016 5:18:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

	CREATE FUNCTION [dbo].[fn_diagramobjects]() 
	RETURNS int
	WITH EXECUTE AS N'dbo'
	AS
	BEGIN
		declare @id_upgraddiagrams		int
		declare @id_sysdiagrams			int
		declare @id_helpdiagrams		int
		declare @id_helpdiagramdefinition	int
		declare @id_creatediagram	int
		declare @id_renamediagram	int
		declare @id_alterdiagram 	int 
		declare @id_dropdiagram		int
		declare @InstalledObjects	int

		select @InstalledObjects = 0

		select 	@id_upgraddiagrams = object_id(N'dbo.sp_upgraddiagrams'),
			@id_sysdiagrams = object_id(N'dbo.sysdiagrams'),
			@id_helpdiagrams = object_id(N'dbo.sp_helpdiagrams'),
			@id_helpdiagramdefinition = object_id(N'dbo.sp_helpdiagramdefinition'),
			@id_creatediagram = object_id(N'dbo.sp_creatediagram'),
			@id_renamediagram = object_id(N'dbo.sp_renamediagram'),
			@id_alterdiagram = object_id(N'dbo.sp_alterdiagram'), 
			@id_dropdiagram = object_id(N'dbo.sp_dropdiagram')

		if @id_upgraddiagrams is not null
			select @InstalledObjects = @InstalledObjects + 1
		if @id_sysdiagrams is not null
			select @InstalledObjects = @InstalledObjects + 2
		if @id_helpdiagrams is not null
			select @InstalledObjects = @InstalledObjects + 4
		if @id_helpdiagramdefinition is not null
			select @InstalledObjects = @InstalledObjects + 8
		if @id_creatediagram is not null
			select @InstalledObjects = @InstalledObjects + 16
		if @id_renamediagram is not null
			select @InstalledObjects = @InstalledObjects + 32
		if @id_alterdiagram  is not null
			select @InstalledObjects = @InstalledObjects + 64
		if @id_dropdiagram is not null
			select @InstalledObjects = @InstalledObjects + 128
		
		return @InstalledObjects 
	END
	
GO
/****** Object:  UserDefinedFunction [dbo].[fnSplitString]    Script Date: 5/25/2016 5:18:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fnSplitString] 
( 
    @string NVARCHAR(MAX), 
    @delimiter CHAR(1) 
) 
RETURNS @output TABLE(splitdata NVARCHAR(MAX) 
) 
/**************************************************************************
This function is to split the string with the specific delimiter to a table
For Example: 12;34;56;67;78;123;456, this will split to multi records below
12
34
56
67
78
123
456 
***************************************************************************/
BEGIN 
    DECLARE @start INT, @end INT 
    SELECT @start = 1, @end = CHARINDEX(@delimiter, @string) 
    WHILE @start < LEN(@string) + 1 BEGIN 
        IF @end = 0  
            SET @end = LEN(@string) + 1
       
        INSERT INTO @output (splitdata)  
        VALUES(SUBSTRING(@string, @start, @end - @start)) 
        SET @start = @end + 1 
        SET @end = CHARINDEX(@delimiter, @string, @start)
        
    END 
    RETURN 
END



GO
/****** Object:  View [dbo].[StudentsByEthinicity]    Script Date: 5/25/2016 5:18:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[StudentsByEthinicity]
as
select Student.StudentID as StudentID, InternationalStudent, Ethinicity.EthinicityID as EthinicityID ,Description from Student
Join Student_Ethinicity
on Student.StudentID = Student_Ethinicity.StudentID
join Ethinicity
on Student_Ethinicity.EthinicityID = Ethinicity.EthinicityID


GO
/****** Object:  View [dbo].[StudentsByFocusArea]    Script Date: 5/25/2016 5:18:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[StudentsByFocusArea]
as
select Student.StudentID as StudentID,Student.FirstName as FirstName,Student.LastName as LastName, FocusArea.FocusAreaID as FocusAreaID, FocusArea.AreaName as AreaName from Student
join Student_FocusArea
on Student.StudentID = Student_FocusArea.StudentID
join FocusArea
on FocusArea.FocusAreaID =  Student_FocusArea.FocusAreaID


GO
/****** Object:  View [dbo].[vwGetStudentReflectionAndPartnerEvaluation]    Script Date: 5/25/2016 5:18:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[vwGetStudentReflectionAndPartnerEvaluation]
as
select SignUpFor.StudentID as StudentID, SignUpFor.OpportunityID as OpportunityID,StudentReflection,PartnerEvaluation,SignUpFor.CPPID as CPPID from SignUpFor
join Student
on Student.StudentID = SignUpFor.StudentID 
join Opportunity
on Opportunity.OpportunityID = SignUpFor.OpportunityID
join CommunityPartnersPeople
on CommunityPartnersPeople.CPPID = SignUpFor.CPPID


GO
/****** Object:  View [dbo].[vwOpportunityByCommunityPartner]    Script Date: 5/25/2016 5:18:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create View [dbo].[vwOpportunityByCommunityPartner]
as
select OpportunityId,Name,Location,DateOfCreation, OrganizationName
from Opportunity
join CommunityPartners
on Opportunity.CPID= CommunityPartners.CPID


GO
/****** Object:  View [dbo].[vwOpportunityByFocusArea]    Script Date: 5/25/2016 5:18:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create View [dbo].[vwOpportunityByFocusArea]
as
select Opportunity.OpportunityId as OpportunityId,Name, FocusArea.FocusAreaId as FocusAreaId,AreaName from Opportunity
join Opportunity_FocusArea
on Opportunity.OpportunityId = Opportunity_FocusArea.OpportunityId
join FocusArea
on FocusArea.FocusAreaId = Opportunity_FocusArea.FocusAreaId


GO
/****** Object:  View [dbo].[vwOpportunityByQuarter]    Script Date: 5/25/2016 5:18:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create View [dbo].[vwOpportunityByQuarter]
as
select OpportunityId,Name,JobDescription,QuarterName 
from Opportunity
join Quarter
on Opportunity.QuarterID= Quarter.QuarterID


GO
/****** Object:  View [dbo].[vwOpportunityBySection]    Script Date: 5/25/2016 5:18:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create View [dbo].[vwOpportunityBySection]
as
select Opportunity.OpportunityId as OpportunityId,Name, Section.SectionID as SectionID,class.CourseID as CourseID,class.CourseName as CourseName from Opportunity
join Opportunity_Section
on Opportunity.OpportunityId = Opportunity_Section.OpportunityId
join Section
on Section.SectionID = Opportunity_Section.SectionID
join Class
on Class.CourseID = Section.CourseID


GO
/****** Object:  View [dbo].[vwOpportunityByType]    Script Date: 5/25/2016 5:18:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create View [dbo].[vwOpportunityByType]
as
select OpportunityId, Opportunity.Name as Name,Opportunity.TypeID as TypeID from Opportunity
join OpportunityType
on Opportunity.TypeID = OpportunityType.TypeID


GO
/****** Object:  View [dbo].[vwSignUpStatus]    Script Date: 5/25/2016 5:18:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[vwSignUpStatus]
as 
select SignUpFor.StudentID as StudentID, SignUpFor.OpportunityID as OpportunityID,SignUpStatus from SignUpFor
join Student
on Student.StudentID = SignUpFor.StudentID 
join Opportunity
on Opportunity.OpportunityID = SignUpFor.OpportunityID


GO
/****** Object:  View [dbo].[vwStudentBySection]    Script Date: 5/25/2016 5:18:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[vwStudentBySection]
as
select Student.StudentID as StudentID, Student.FirstName as FirstName,Student.LastName as LastName,Section.SectionID as SectionID, SectionName from Student
join Student_Section
on Student.StudentID = Student_Section.StudentID
join Section
on Section.SectionID = Student_Section.SectionID


GO
/****** Object:  View [dbo].[vwTotalVolunteeredHours]    Script Date: 5/25/2016 5:18:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  Create View [dbo].[vwTotalVolunteeredHours]
  As
  Select OpportunityID,StudentID,sum(HoursVolunteered) as TotalHours,
  sum(PartnerApprovedHours) as PartnerApprovedHours
  From TimeEntries
  Group BY OpportunityID,StudentID


GO
/****** Object:  StoredProcedure [dbo].[.spGetAdminInfo]    Script Date: 5/25/2016 5:18:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  CREATE PROCEDURE [dbo].[.spGetAdminInfo]	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		LastName,FirstName,
		Email		
	FROM
	   Admin
END







GO
/****** Object:  StoredProcedure [dbo].[GetOpportunityType]    Script Date: 5/25/2016 5:18:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetOpportunityType]	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		TypeID as OpportunityTypeId,
		Name		
	FROM
		OpportunityType
END





GO
/****** Object:  StoredProcedure [dbo].[SHOWSCHEMA]    Script Date: 5/25/2016 5:18:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SHOWSCHEMA]
	@schemaname		varchar(max)
AS
BEGIN

DECLARE @tablename VARCHAR(max)

DECLARE load_cursor CURSOR FOR 
       SELECT table_name 
	   from information_schema.tables 
       where table_schema = @schemaname
	   and table_type = 'BASE TABLE'

OPEN load_cursor 
FETCH NEXT FROM load_cursor INTO @tablename 

SELECT GETDATE() AS CurrentTime, SYSTEM_USER as 'User'

WHILE @@FETCH_STATUS = 0 
BEGIN 
       SELECT @TABLENAME as TableName
	   SELECT COLUMN_NAME, DATA_TYPE 
		FROM INFORMATION_SCHEMA.COLUMNS
		WHERE TABLE_NAME = @TABLENAME

		SELECT
			trigger_name   = st.name
			,trigger_text   = sc.text
			,create_date    = st.create_date
		FROM sys.triggers st
			JOIN sysobjects so
				ON st.parent_id = so.id
			JOIN syscomments sc
				ON sc.id = st.[object_id]
		WHERE so.name = @TABLENAME
 
       FETCH NEXT FROM load_cursor INTO @TABLENAME 
END 

CLOSE load_cursor 
DEALLOCATE load_cursor 
END



GO
/****** Object:  StoredProcedure [dbo].[sp_alterdiagram]    Script Date: 5/25/2016 5:18:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

	CREATE PROCEDURE [dbo].[sp_alterdiagram]
	(
		@diagramname 	sysname,
		@owner_id	int	= null,
		@version 	int,
		@definition 	varbinary(max)
	)
	WITH EXECUTE AS 'dbo'
	AS
	BEGIN
		set nocount on
	
		declare @theId 			int
		declare @retval 		int
		declare @IsDbo 			int
		
		declare @UIDFound 		int
		declare @DiagId			int
		declare @ShouldChangeUID	int
	
		if(@diagramname is null)
		begin
			RAISERROR ('Invalid ARG', 16, 1)
			return -1
		end
	
		execute as caller;
		select @theId = DATABASE_PRINCIPAL_ID();	 
		select @IsDbo = IS_MEMBER(N'db_owner'); 
		if(@owner_id is null)
			select @owner_id = @theId;
		revert;
	
		select @ShouldChangeUID = 0
		select @DiagId = diagram_id, @UIDFound = principal_id from dbo.sysdiagrams where principal_id = @owner_id and name = @diagramname 
		
		if(@DiagId IS NULL or (@IsDbo = 0 and @theId <> @UIDFound))
		begin
			RAISERROR ('Diagram does not exist or you do not have permission.', 16, 1);
			return -3
		end
	
		if(@IsDbo <> 0)
		begin
			if(@UIDFound is null or USER_NAME(@UIDFound) is null) -- invalid principal_id
			begin
				select @ShouldChangeUID = 1 ;
			end
		end

		-- update dds data			
		update dbo.sysdiagrams set definition = @definition where diagram_id = @DiagId ;

		-- change owner
		if(@ShouldChangeUID = 1)
			update dbo.sysdiagrams set principal_id = @theId where diagram_id = @DiagId ;

		-- update dds version
		if(@version is not null)
			update dbo.sysdiagrams set version = @version where diagram_id = @DiagId ;

		return 0
	END
	
GO
/****** Object:  StoredProcedure [dbo].[sp_creatediagram]    Script Date: 5/25/2016 5:18:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

	CREATE PROCEDURE [dbo].[sp_creatediagram]
	(
		@diagramname 	sysname,
		@owner_id		int	= null, 	
		@version 		int,
		@definition 	varbinary(max)
	)
	WITH EXECUTE AS 'dbo'
	AS
	BEGIN
		set nocount on
	
		declare @theId int
		declare @retval int
		declare @IsDbo	int
		declare @userName sysname
		if(@version is null or @diagramname is null)
		begin
			RAISERROR (N'E_INVALIDARG', 16, 1);
			return -1
		end
	
		execute as caller;
		select @theId = DATABASE_PRINCIPAL_ID(); 
		select @IsDbo = IS_MEMBER(N'db_owner');
		revert; 
		
		if @owner_id is null
		begin
			select @owner_id = @theId;
		end
		else
		begin
			if @theId <> @owner_id
			begin
				if @IsDbo = 0
				begin
					RAISERROR (N'E_INVALIDARG', 16, 1);
					return -1
				end
				select @theId = @owner_id
			end
		end
		-- next 2 line only for test, will be removed after define name unique
		if EXISTS(select diagram_id from dbo.sysdiagrams where principal_id = @theId and name = @diagramname)
		begin
			RAISERROR ('The name is already used.', 16, 1);
			return -2
		end
	
		insert into dbo.sysdiagrams(name, principal_id , version, definition)
				VALUES(@diagramname, @theId, @version, @definition) ;
		
		select @retval = @@IDENTITY 
		return @retval
	END
	
GO
/****** Object:  StoredProcedure [dbo].[sp_dropdiagram]    Script Date: 5/25/2016 5:18:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

	CREATE PROCEDURE [dbo].[sp_dropdiagram]
	(
		@diagramname 	sysname,
		@owner_id	int	= null
	)
	WITH EXECUTE AS 'dbo'
	AS
	BEGIN
		set nocount on
		declare @theId 			int
		declare @IsDbo 			int
		
		declare @UIDFound 		int
		declare @DiagId			int
	
		if(@diagramname is null)
		begin
			RAISERROR ('Invalid value', 16, 1);
			return -1
		end
	
		EXECUTE AS CALLER;
		select @theId = DATABASE_PRINCIPAL_ID();
		select @IsDbo = IS_MEMBER(N'db_owner'); 
		if(@owner_id is null)
			select @owner_id = @theId;
		REVERT; 
		
		select @DiagId = diagram_id, @UIDFound = principal_id from dbo.sysdiagrams where principal_id = @owner_id and name = @diagramname 
		if(@DiagId IS NULL or (@IsDbo = 0 and @UIDFound <> @theId))
		begin
			RAISERROR ('Diagram does not exist or you do not have permission.', 16, 1)
			return -3
		end
	
		delete from dbo.sysdiagrams where diagram_id = @DiagId;
	
		return 0;
	END
	
GO
/****** Object:  StoredProcedure [dbo].[sp_helpdiagramdefinition]    Script Date: 5/25/2016 5:18:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

	CREATE PROCEDURE [dbo].[sp_helpdiagramdefinition]
	(
		@diagramname 	sysname,
		@owner_id	int	= null 		
	)
	WITH EXECUTE AS N'dbo'
	AS
	BEGIN
		set nocount on

		declare @theId 		int
		declare @IsDbo 		int
		declare @DiagId		int
		declare @UIDFound	int
	
		if(@diagramname is null)
		begin
			RAISERROR (N'E_INVALIDARG', 16, 1);
			return -1
		end
	
		execute as caller;
		select @theId = DATABASE_PRINCIPAL_ID();
		select @IsDbo = IS_MEMBER(N'db_owner');
		if(@owner_id is null)
			select @owner_id = @theId;
		revert; 
	
		select @DiagId = diagram_id, @UIDFound = principal_id from dbo.sysdiagrams where principal_id = @owner_id and name = @diagramname;
		if(@DiagId IS NULL or (@IsDbo = 0 and @UIDFound <> @theId ))
		begin
			RAISERROR ('Diagram does not exist or you do not have permission.', 16, 1);
			return -3
		end

		select version, definition FROM dbo.sysdiagrams where diagram_id = @DiagId ; 
		return 0
	END
	
GO
/****** Object:  StoredProcedure [dbo].[sp_helpdiagrams]    Script Date: 5/25/2016 5:18:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

	CREATE PROCEDURE [dbo].[sp_helpdiagrams]
	(
		@diagramname sysname = NULL,
		@owner_id int = NULL
	)
	WITH EXECUTE AS N'dbo'
	AS
	BEGIN
		DECLARE @user sysname
		DECLARE @dboLogin bit
		EXECUTE AS CALLER;
			SET @user = USER_NAME();
			SET @dboLogin = CONVERT(bit,IS_MEMBER('db_owner'));
		REVERT;
		SELECT
			[Database] = DB_NAME(),
			[Name] = name,
			[ID] = diagram_id,
			[Owner] = USER_NAME(principal_id),
			[OwnerID] = principal_id
		FROM
			sysdiagrams
		WHERE
			(@dboLogin = 1 OR USER_NAME(principal_id) = @user) AND
			(@diagramname IS NULL OR name = @diagramname) AND
			(@owner_id IS NULL OR principal_id = @owner_id)
		ORDER BY
			4, 5, 1
	END
	
GO
/****** Object:  StoredProcedure [dbo].[sp_renamediagram]    Script Date: 5/25/2016 5:18:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

	CREATE PROCEDURE [dbo].[sp_renamediagram]
	(
		@diagramname 		sysname,
		@owner_id		int	= null,
		@new_diagramname	sysname
	
	)
	WITH EXECUTE AS 'dbo'
	AS
	BEGIN
		set nocount on
		declare @theId 			int
		declare @IsDbo 			int
		
		declare @UIDFound 		int
		declare @DiagId			int
		declare @DiagIdTarg		int
		declare @u_name			sysname
		if((@diagramname is null) or (@new_diagramname is null))
		begin
			RAISERROR ('Invalid value', 16, 1);
			return -1
		end
	
		EXECUTE AS CALLER;
		select @theId = DATABASE_PRINCIPAL_ID();
		select @IsDbo = IS_MEMBER(N'db_owner'); 
		if(@owner_id is null)
			select @owner_id = @theId;
		REVERT;
	
		select @u_name = USER_NAME(@owner_id)
	
		select @DiagId = diagram_id, @UIDFound = principal_id from dbo.sysdiagrams where principal_id = @owner_id and name = @diagramname 
		if(@DiagId IS NULL or (@IsDbo = 0 and @UIDFound <> @theId))
		begin
			RAISERROR ('Diagram does not exist or you do not have permission.', 16, 1)
			return -3
		end
	
		-- if((@u_name is not null) and (@new_diagramname = @diagramname))	-- nothing will change
		--	return 0;
	
		if(@u_name is null)
			select @DiagIdTarg = diagram_id from dbo.sysdiagrams where principal_id = @theId and name = @new_diagramname
		else
			select @DiagIdTarg = diagram_id from dbo.sysdiagrams where principal_id = @owner_id and name = @new_diagramname
	
		if((@DiagIdTarg is not null) and  @DiagId <> @DiagIdTarg)
		begin
			RAISERROR ('The name is already used.', 16, 1);
			return -2
		end		
	
		if(@u_name is null)
			update dbo.sysdiagrams set [name] = @new_diagramname, principal_id = @theId where diagram_id = @DiagId
		else
			update dbo.sysdiagrams set [name] = @new_diagramname where diagram_id = @DiagId
		return 0
	END
	
GO
/****** Object:  StoredProcedure [dbo].[sp_upgraddiagrams]    Script Date: 5/25/2016 5:18:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

	CREATE PROCEDURE [dbo].[sp_upgraddiagrams]
	AS
	BEGIN
		IF OBJECT_ID(N'dbo.sysdiagrams') IS NOT NULL
			return 0;
	
		CREATE TABLE dbo.sysdiagrams
		(
			name sysname NOT NULL,
			principal_id int NOT NULL,	-- we may change it to varbinary(85)
			diagram_id int PRIMARY KEY IDENTITY,
			version int,
	
			definition varbinary(max)
			CONSTRAINT UK_principal_name UNIQUE
			(
				principal_id,
				name
			)
		);


		/* Add this if we need to have some form of extended properties for diagrams */
		/*
		IF OBJECT_ID(N'dbo.sysdiagram_properties') IS NULL
		BEGIN
			CREATE TABLE dbo.sysdiagram_properties
			(
				diagram_id int,
				name sysname,
				value varbinary(max) NOT NULL
			)
		END
		*/

		IF OBJECT_ID(N'dbo.dtproperties') IS NOT NULL
		begin
			insert into dbo.sysdiagrams
			(
				[name],
				[principal_id],
				[version],
				[definition]
			)
			select	 
				convert(sysname, dgnm.[uvalue]),
				DATABASE_PRINCIPAL_ID(N'dbo'),			-- will change to the sid of sa
				0,							-- zero for old format, dgdef.[version],
				dgdef.[lvalue]
			from dbo.[dtproperties] dgnm
				inner join dbo.[dtproperties] dggd on dggd.[property] = 'DtgSchemaGUID' and dggd.[objectid] = dgnm.[objectid]	
				inner join dbo.[dtproperties] dgdef on dgdef.[property] = 'DtgSchemaDATA' and dgdef.[objectid] = dgnm.[objectid]
				
			where dgnm.[property] = 'DtgSchemaNAME' and dggd.[uvalue] like N'_EA3E6268-D998-11CE-9454-00AA00A3F36E_' 
			return 2;
		end
		return 1;
	END
	
GO
/****** Object:  StoredProcedure [dbo].[spAddCourseSectionToOpportunity]    Script Date: 5/25/2016 5:18:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spAddCourseSectionToOpportunity]	
	@OpportunityID int,
	@CourseSectionIDs varchar(1000)	
AS
/****************************************************************************
This proc is to add the assigned sections to one opportunity to the table
*****************************************************************************/
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO 
	Opportunity_Section (OpportunityID, SectionID)
	SELECT 
		@OpportunityID,
		splitData
	FROM dbo.fnSplitString(@CourseSectionIDs,';') --call function fnSplitString to split the strings to table
	WHERE 		 
		Convert(varchar(10),@OpportunityID) + '_' + splitData
		NOT IN
		(SELECT Convert(varchar(10),OpportunityID) + '_' + Convert(varchar(10),SectionID) FROM Opportunity_Section)

END







GO
/****** Object:  StoredProcedure [dbo].[spAddOpportunity]    Script Date: 5/25/2016 5:18:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
/****************Stored Procedure for adding opportunity ***************/
CREATE PROCEDURE [dbo].[spAddOpportunity]	
	@Name varchar(50)
   ,@Location varchar(50)
   ,@JobDescription varchar(max)	
   ,@Requirements varchar(1000)
   ,@TimeCommittment varchar(9)
   ,@TotalNumberOfSlots int
   ,@OrientationDate datetime
   ,@ResumeRequired varchar(3)   
   ,@MinimumAge varchar(8)
   ,@CRCRequiredByPartner varchar(3)
   ,@CRCRequiredBySU varchar(3)
   ,@LinkToOnlineApp varchar(200)
   ,@TypeID int
   ,@CPID int
   ,@CPPID int
   ,@QuarterID int
   ,@JobHours varchar(20)
   ,@DistanceFromSU varchar(8)   
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Insert Into Opportunity
	(Name
	, Location
	, JobDescription
	, Requirements
	, TimeCommittment
	, TotalNumberOfSlots
	, OrientatioNDate
	, ResumeRequired	
	, MinimumAge
	, CRCRequiredByPartner
	, CRCRequiredBySU
	, LinkToOnlineApp
	, TypeID
	, CPID
	, CPPID
	, QuarterID
	, JobHours
	, DistanceFromSU
	, DateOfCreation
	, Status
	, SupervisorEmail
	)
	Values
	(@Name
	, @Location
	, @JobDescription
	, @Requirements
	, @TimeCommittment
	, @TotalNumberOfSlots
	, @OrientatioNDate
	, @ResumeRequired	
	, @MinimumAge
	, @CRCRequiredByPartner
	, @CRCRequiredBySU
	, @LinkToOnlineApp
	, @TypeID
	, @CPID
	, @CPPID
	, @QuarterID
	, @JobHours
	, @DistanceFromSU
	, GetDate()
	, 'Pending'
	, 'N/A')	
END


GO
/****** Object:  StoredProcedure [dbo].[spAddSignUpFor]    Script Date: 5/25/2016 5:18:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  Create Procedure [dbo].[spAddSignUpFor]
		 
		 @OpportunityId int,
		 @StudentID int,
		 @CPPID int
		 
		 As
		 Insert into [SignUpFor]
		 (OpportunityId,StudentID,CPPID)
		 Values
		 ( @OpportunityId,@StudentID,@CPPID)


GO
/****** Object:  StoredProcedure [dbo].[spApproveOpportunityByAdmin]    Script Date: 5/25/2016 5:18:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spApproveOpportunityByAdmin]	
	@OpportunityId int   
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Update Opportunity
	Set 
		Status = 'Open'
	WHERE 
		OpportunityId = @OpportunityId	
END


GO
/****** Object:  StoredProcedure [dbo].[spDeleteCommunityPartnerPeople]    Script Date: 5/25/2016 5:18:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spDeleteCommunityPartnerPeople]	
	@CPPID int  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Delete FROM CommunityPartnersPeople WHERE CPPID = @CPPID
	
END





GO
/****** Object:  StoredProcedure [dbo].[spDeleteOpportunity]    Script Date: 5/25/2016 5:18:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spDeleteOpportunity]	
	@OpportunityID int  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Delete FROM Opportunity WHERE OpportunityID = @OpportunityID
	
END





GO
/****** Object:  StoredProcedure [dbo].[spDeleteStudentOpportunityRegistration]    Script Date: 5/25/2016 5:18:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE Procedure [dbo].[spDeleteStudentOpportunityRegistration]
	@StudentId int, @OpportunityId int
AS
Begin
  SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM [dbo].[SignUpFor]
	WHERE StudentID=@StudentID 
	AND OpportunityID=@OpportunityID	

End




GO
/****** Object:  StoredProcedure [dbo].[spGetAssignedCourseSection]    Script Date: 5/25/2016 5:18:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetAssignedCourseSection]		
	@OpportunityID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT		
		s.SectionID,
		c.CourseID,
		c.CourseName,
		c.ShortName	AS CourseShortName,
		s.RoomNumber,
		s.ClassHours,
		s.NumberOfSlots,
		q.QuarterName,
		q.ShortName AS QuarterShortName,
		s.SectionName
	FROM		
		Section s	
	INNER JOIN
		Class c
	ON
		s.CourseID = c.CourseID	
	INNER JOIN
		Quarter q
	ON
		s.QuarterID = q.QuarterID
	WHERE
		s.SectionID IN 
		(SELECT SectionID FROM Opportunity_Section WHERE OpportunityID = @OpportunityID)
END






GO
/****** Object:  StoredProcedure [dbo].[spGetClassInfo]    Script Date: 5/25/2016 5:18:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[spGetClassInfo]
   @CourseID int
   as
   Begin
    Select ShortName,CourseName, CourseID from Class
	where CourseID = @CourseID
	End



GO
/****** Object:  StoredProcedure [dbo].[spGetCommunityPartnerAlertview]    Script Date: 5/25/2016 5:18:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[spGetCommunityPartnerAlertview]
As
Begin
Select
AlertID, 
Message 
from CommunityPartnerAlert
End



GO
/****** Object:  StoredProcedure [dbo].[spGetCommunityPartnerOpportunityList]    Script Date: 5/25/2016 5:18:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[spGetCommunityPartnerOpportunityList]	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		cp.CPID,
		cp.OrganizationName AS CommunityPartnerName,
		o.Status,
		count(*) as OpportunityCount
	FROM 
		Opportunity o
	JOIN 
		CommunityPartners cp
	ON 
		o.CPID = cp.CPID
	GROUP BY 
		cp.CPID, o.Status, cp.OrganizationName
END





GO
/****** Object:  StoredProcedure [dbo].[spGetCommunityPartnerPeople]    Script Date: 5/25/2016 5:18:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetCommunityPartnerPeople]	
	@CPID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		CPPID as SupervisorID,
		FirstName,
		LastName,
		Title,
		Phone,
		EmailID
		
	FROM
		CommunityPartnersPeople
	WHERE
		CPID = @CPID
END



GO
/****** Object:  StoredProcedure [dbo].[spGetCommunityPartnerProfile]    Script Date: 5/25/2016 5:18:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetCommunityPartnerProfile]	
	@CPID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		CPID AS CommunityPartnerID,
		OrganizationName,
		Address,
		Website,
		MainPhone,
		MissionStatement,
		WorkDescription
			
	From CommunityPartners
	WHERE
		CPID = @CPID
END



GO
/****** Object:  StoredProcedure [dbo].[spGetCommunityPartners]    Script Date: 5/25/2016 5:18:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetCommunityPartners]	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		CPID AS CommunityPartnerID,
		OrganizationName,
		Address,
		Website,
		MainPhone,
		MissionStatement,
		WorkDescription
			
	From CommunityPartners
END



GO
/****** Object:  StoredProcedure [dbo].[spGetCommunityPartnerStudentView]    Script Date: 5/25/2016 5:18:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[spGetCommunityPartnerStudentView]
As
Begin
Set NoCount On
Select
o.OpportunityID,
o.Name,
s.StudentID,
s.FirstName,
s.LastName,
SignUpStatus,
PartnerEvaluation,
ss.SectionID,
ss.SectionName,
p.ProfessorID,
p.FirstName as ProfessorFirstName,
p.LastName as ProfessorLastName,
vt.TotalHours as TotalHoursVolunteered,
PartnerApprovedHours
from vwTotalVolunteeredHours vt
inner join
SignUpFor
on vt.OpportunityID = SignUpFor.OpportunityID
and vt.StudentID = SignUpFor.StudentID
inner join
Student s
on SignUpFor.StudentID = s.StudentID
   
inner join 
Opportunity o
on SignUpFor.OpportunityID = o.OpportunityID
inner join
Opportunity_Section
on o.OpportunityID = Opportunity_Section.OpportunityID
inner join
Section ss
on Opportunity_Section.SectionID = ss.SectionID
inner join
Professor p
on ss.ProfessorID = p.ProfessorID
 end



GO
/****** Object:  StoredProcedure [dbo].[spGetCourseEvaluationByStudent]    Script Date: 5/25/2016 5:18:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create  procedure [dbo].[spGetCourseEvaluationByStudent]
As
Begin
Select
o.OpportunityID,
o.Name,
StudentEvaluation as CourseEvaluationByStudent,
s.StudentID,
s.FirstName,
s.LastName,
SectionName
from Opportunity o 
inner join SignUpFor
on o.OpportunityID = SignUpFor.OpportunityID
inner join
Student s
on SignUpFor.StudentID = s.StudentID
inner join
vwStudentBySection ss
on ss.StudentID = s.StudentID
End



GO
/****** Object:  StoredProcedure [dbo].[spGetEthinicity]    Script Date: 5/25/2016 5:18:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[spGetEthinicity]
	As
	Begin
	Select EthinicityID,Description from Ethinicity
	End



GO
/****** Object:  StoredProcedure [dbo].[spGetFocusArea]    Script Date: 5/25/2016 5:18:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGetFocusArea]	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		FocusAreaId,
		AreaName		
	FROM
		FocusArea
END





GO
/****** Object:  StoredProcedure [dbo].[spGetOpportunityByDate]    Script Date: 5/25/2016 5:18:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[spGetOpportunityByDate]
@DateOfCreation date
as
Begin
  Select OpportunityId,Name,DateOfCreation from Opportunity where DateOfCreation = @DateOfCreation 
End



GO
/****** Object:  StoredProcedure [dbo].[spGetOpportunityById]    Script Date: 5/25/2016 5:18:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[spGetOpportunityById]
	@OpportunityId int
AS
Begin
  SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		OpportunityID,
		Name,
		Location,
		DateOfCreation,
		JobDescription,
		Requirements,
		TimeCommittment,
		TotalNumberOfSlots,
		OrientationDate,
		Status,
		ResumeRequired,
		'' AS SupervisorEmail,
		MinimumAge,
		CRCRequiredByPartner,
		CRCRequiredBySU,
		LinkToOnlineApp,	
		TypeID,
		CPID,
		CPPID,
		QuarterID,		
		JobHours,
		DistanceFromSU
	FROM
		Opportunity  
	WHERE 
		OpportunityId = @OpportunityId
End





GO
/****** Object:  StoredProcedure [dbo].[spGetOpportunityByStatus]    Script Date: 5/25/2016 5:18:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGetOpportunityByStatus]
	-- Add the parameters for the stored procedure here
	@Status nvarchar (8)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT OpportunityId,Name,Status from Opportunity
	where Status = @Status
END



GO
/****** Object:  StoredProcedure [dbo].[spGetOpportunityDetailByOpportunityId]    Script Date: 5/25/2016 5:18:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE Procedure [dbo].[spGetOpportunityDetailByOpportunityId]
	@OpportunityId int
AS
Begin
  SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT CP.[CPID]
		,[OpportunityID]
      ,[OrganizationName]
      ,[WorkDescription] OrganizationDesc
	  ,[Name] OpportunityName
	  ,[JobDescription] OpportunityDesc
	  ,[Address] Location
	  ,[TimeCommittment]
	  ,CPP.[FirstName]+ ', ' + CPP.[LastName] SiteSupervisorName
	  ,[SupervisorEmail]
	  ,[CRCRequiredByPartner]
	  ,[MinimumAge] 
	  ,[ResumeRequired]
      ,[Website] Link
      ,[MainPhone]           
  FROM [dbo].[CommunityPartners] CP
  INNER JOIN [dbo].[Opportunity] O ON (O.CPID=CP.CPID)
  INNER JOIN [dbo].[CommunityPartnersPeople] CPP ON (CPP.CPPID=O.CPPID)
  WHERE [OpportunityID]=@OpportunityId

End




GO
/****** Object:  StoredProcedure [dbo].[spGetOpportunityList]    Script Date: 5/25/2016 5:18:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetOpportunityList]	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		OpportunityID,
		o.Name,
		Location,
		DateOfCreation,
		JobDescription,
		Requirements,
		TimeCommittment,
		TotalNumberOfSlots,
		OrientationDate,
		Status,
		ResumeRequired,		
		MinimumAge,
		CRCRequiredByPartner,
		CRCRequiredBySU,
		LinkToOnlineApp,
		ot.Name AS OpportunityTypeName,
		q.QuarterName,
		cp.OrganizationName,
		cpp.FirstName + ' ' + cpp.LastName AS Supervisor,
		cpp.EmailId as SupervisorEmail,
		DateOfCreation AS DateApproved,
		JobHours,
		DistanceFromSU as DistanceFromSU
	FROM
		Opportunity o
	INNER JOIN
		OpportunityType ot
	ON
		o.TypeID = ot.TypeID
	INNER JOIN
		CommunityPartners cp
	ON
		o.CPID = cp.CPID
	INNER JOIN
		[dbo].[CommunityPartnersPeople] cpp
	ON
		o.CPPID = cpp.CPPID
	INNER JOIN
		Quarter q
	ON
		o.QuarterID = q.QuarterID

END





GO
/****** Object:  StoredProcedure [dbo].[spGetOpportunityListByStudentId]    Script Date: 5/25/2016 5:18:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE Procedure [dbo].[spGetOpportunityListByStudentId]
	@StudentId int
AS
Begin
  SET NOCOUNT ON;
  
    -- Insert statements for procedure here
	SELECT O.[OpportunityID]
      ,[Name] 'Position'
	  ,CP.[OrganizationName] 'Organization'
	  ,5 'SlotsAvailable'
	  ,O.[DistanceFromSU] + ' miles' As 'DistanceFromSU'
      ,[Location]
      ,[TimeCommittment]
      ,[MinimumAge]
      ,CASE 
	    WHEN [CRCRequiredByPartner] = 'Yes' OR [CRCRequiredBySU] = 'Yes' THEN 'Yes' 
		ELSE 'No'
		END AS 'CRCRequired'
	  ,[JobDescription]
  FROM [dbo].[Opportunity] O
  INNER JOIN [dbo].[CommunityPartners] CP ON (CP.CPID=O.CPID) 
  INNER JOIN [dbo].[Opportunity_Section] OS ON (OS.OpportunityID=O.OpportunityID)
  INNER JOIN [dbo].[Student_Section] SS ON (SS.SectionID=OS.SectionID)
  WHERE O.Status<>'Pending' AND StudentID=@StudentId

End




GO
/****** Object:  StoredProcedure [dbo].[spGetOpportunityListForAdmin]    Script Date: 5/25/2016 5:18:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spGetOpportunityListForAdmin]	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		OpportunityID,
		cp.OrganizationName,
		o.Name,				
		TotalNumberOfSlots,		
		Status,		
		ot.Name AS OpportunityTypeName,		
		cpp.FirstName + ' ' + cpp.LastName AS Supervisor,
		SupervisorEmail,
		DateOfCreation AS DateApproved	
	FROM
		Opportunity o
	INNER JOIN
		OpportunityType ot
	ON
		o.TypeID = ot.TypeID
	INNER JOIN
		CommunityPartners cp
	ON
		o.CPID = cp.CPID
	INNER JOIN
		[dbo].[CommunityPartnersPeople] cpp
	ON
		o.CPPID = cpp.CPPID	
END


GO
/****** Object:  StoredProcedure [dbo].[spGetOpportunityListForFaculty]    Script Date: 5/25/2016 5:18:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetOpportunityListForFaculty]	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		OpportunityID,
		cp.OrganizationName,
		o.Name,				
		TotalNumberOfSlots,		
		Status,		
		ot.Name AS OpportunityTypeName,		
		cpp.FirstName + ' ' + cpp.LastName AS Supervisor,
		SupervisorEmail,
		DateOfCreation AS DateApproved		
	FROM
		Opportunity o
	INNER JOIN
		OpportunityType ot
	ON
		o.TypeID = ot.TypeID
	INNER JOIN
		CommunityPartners cp
	ON
		o.CPID = cp.CPID
	INNER JOIN
		[dbo].[CommunityPartnersPeople] cpp
	ON
		o.CPPID = cpp.CPPID	
END


GO
/****** Object:  StoredProcedure [dbo].[spGetOpportunityRegisteredByStudentId]    Script Date: 5/25/2016 5:18:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE Procedure [dbo].[spGetOpportunityRegisteredByStudentId]
	@StudentId int
AS
Begin
  SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT O.OpportunityID
	  ,SUF.[StudentId]
	  ,O.Name 'Opportunity'
	  ,C.[CourseName]
	  ,Q.QuarterName 'Quarter'
	  ,CP.OrganizationName 'Organization'
	  ,CPP.EmailId
      ,[SignUpStatus] 'Status'		
	  ,'NEED' [HoursVolunteered]
      ,'NEED' [PartnerEvaluation]
	  ,'NEED' [StudentEvaluation]
	  ,'NEED' [StudentReflection]      	  
  FROM [dbo].[SignUpFor] SUF
  INNER JOIN [dbo].[Opportunity] O ON (O.OpportunityID=SUF.OpportunityID)  
  INNER JOIN [dbo].[Opportunity_Section] OS ON (OS.OpportunityID=O.OpportunityID)
  INNER JOIN [dbo].[CommunityPartnersPeople] CPP ON (CPP.CPPID=SUF.CPPID)
  INNER JOIN [dbo].[CommunityPartners] CP ON (CP.CPID=CPP.CPID)
  RIGHT JOIN [dbo].[Quarter] Q ON (Q.QuarterID=O.QuarterID)   
  RIGHT JOIN [dbo].[Section] S ON (S.SectionID=OS.SectionID)
  RIGHT JOIN [dbo].[Class] C ON (C.CourseID=S.CourseID)
  WHERE StudentID=@StudentId AND OS.SectionID=1
END








GO
/****** Object:  StoredProcedure [dbo].[spGetOpportunitySectionList]    Script Date: 5/25/2016 5:18:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetOpportunitySectionList]	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		o.Name as OpportunityName,
		cp.OrganizationName,
		s.SectionName,
		c.CourseName,
		p.FirstName + ' ' + p.LastName as ProfessorName,
		q.QuarterName,
		s.NumberOfSlots
	FROM
		Opportunity_Section os
	INNER JOIN
		Section s
	ON
		os.SectionID = s.SectionID
	INNER JOIN
		Opportunity o
	ON
		os.OpportunityID = o.OpportunityID
	INNER JOIN
		CommunityPartners cp
	ON
		o.CPID = cp.CPID
	INNER JOIN
		Class c
	ON
		s.CourseID = c.CourseID
	INNER JOIN
		Professor p
	ON
		s.ProfessorID = p.ProfessorID
	INNER JOIN
		Quarter q
	ON
		s.QuarterID = q.QuarterID

END




GO
/****** Object:  StoredProcedure [dbo].[spGetOpportunityType]    Script Date: 5/25/2016 5:18:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetOpportunityType]	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		TypeID as OpportunityTypeID,
		Name		
	FROM
		OpportunityType
END


GO
/****** Object:  StoredProcedure [dbo].[spGetProfile]    Script Date: 5/25/2016 5:18:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGetProfile]	
	@StudentId int  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT S.[StudentID]
      ,[FirstName]
      ,[LastName]      
	  ,[PreferedName]
	  ,[DateOfBirth]
      ,[Gender]
	  --,E.Description As 'Ethinicity'      
	  --,FA.AreaName
      ,[InternationalStudent]
      ,[LastBackgroundCheck]
  FROM [dbo].[Student] S 
  --LEFT JOIN [dbo].[Student_Ethinicity] SE ON (SE.StudentID=S.StudentID)
  --INNER JOIN [dbo].[Ethinicity] E ON (E.EthinicityID=SE.EthinicityID) 
  --LEFT JOIN [dbo].[Student_FocusArea] SFA ON (SFA.StudentID=S.StudentID) 
  --INNER JOIN [dbo].[FocusArea] FA ON (FA.FocusAreaID=SFA.FocusAreaID)  
  where StudentID=@StudentId
	
END










GO
/****** Object:  StoredProcedure [dbo].[spGetProfileEthinicity]    Script Date: 5/25/2016 5:18:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGetProfileEthinicity]	
	@StudentId int  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT S.[StudentID]
	  ,E.Description As 'Ethinicity'      	        
  FROM [dbo].[Student] S 
  LEFT JOIN [dbo].[Student_Ethinicity] SE ON (SE.StudentID=S.StudentID)
  INNER JOIN [dbo].[Ethinicity] E ON (E.EthinicityID=SE.EthinicityID)   
  where S.StudentID=@StudentId
	
END










GO
/****** Object:  StoredProcedure [dbo].[spGetProfileFocusAreas]    Script Date: 5/25/2016 5:18:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGetProfileFocusAreas]	
	@StudentId int  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT S.[StudentID]     
	  ,FA.AreaName      
  FROM [dbo].[Student] S   
  LEFT JOIN [dbo].[Student_FocusArea] SFA ON (SFA.StudentID=S.StudentID) 
  INNER JOIN [dbo].[FocusArea] FA ON (FA.FocusAreaID=SFA.FocusAreaID)  
  where S.StudentID=@StudentId
	
END










GO
/****** Object:  StoredProcedure [dbo].[spGetQuarter]    Script Date: 5/25/2016 5:18:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGetQuarter]	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		QuarterID,
		QuarterName,
		ShortName,
		StartDate,
		EndDate				
	FROM
		Quarter
END





GO
/****** Object:  StoredProcedure [dbo].[spGetSectionInfo]    Script Date: 5/25/2016 5:18:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[spGetSectionInfo]
 @SectionID int ,
 @QuarterID int
 as
 Begin
   Select SectionID,QuarterID,NumberOfSlots,SectionName from Section 
   where SectionID = @SectionID
   and  QuarterID = @QuarterID
   End



GO
/****** Object:  StoredProcedure [dbo].[spGetSignUpStatus]    Script Date: 5/25/2016 5:18:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[spGetSignUpStatus]
@StudentID int,
@OpportunityID int
as
Begin
Select OpportunityID,StudentID, SignUpStatus
from SignUpFor
End



GO
/****** Object:  StoredProcedure [dbo].[spGetStudentCourseOppDetailForFaculty]    Script Date: 5/25/2016 5:18:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetStudentCourseOppDetailForFaculty]
	-- Parameter for the stored procedure: professorID
	@ProfessorID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		OpportunityID,
		cp.OrganizationName,
		o.Name,				
		TotalNumberOfSlots,		
		Status,		
		ot.Name AS OpportunityTypeName,		
		cpp.FirstName + ' ' + cpp.LastName AS Supervisor,
		DateOfCreation AS DateApproved		
	FROM
		Opportunity o
	INNER JOIN
		OpportunityType ot
	ON
		o.TypeID = ot.TypeID
	INNER JOIN
		CommunityPartners cp
	ON
		o.CPID = cp.CPID
	INNER JOIN
		[dbo].[CommunityPartnersPeople] cpp
	ON
		o.CPPID = cpp.CPPID	
END


GO
/****** Object:  StoredProcedure [dbo].[spGetStudentProfile]    Script Date: 5/25/2016 5:18:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGetStudentProfile]	
	@StudentId int  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT S.[StudentID]
      ,[FirstName]
      ,[LastName]      
	  ,[PreferedName]
	  ,[DateOfBirth]
      ,[Gender]
	  --,E.Description As 'Ethinicity'      
	  --,FA.AreaName
      ,[InternationalStudent]
      ,[LastBackgroundCheck]
  FROM [dbo].[Student] S 
  --LEFT JOIN [dbo].[Student_Ethinicity] SE ON (SE.StudentID=S.StudentID)
  --INNER JOIN [dbo].[Ethinicity] E ON (E.EthinicityID=SE.EthinicityID) 
  --LEFT JOIN [dbo].[Student_FocusArea] SFA ON (SFA.StudentID=S.StudentID) 
  --INNER JOIN [dbo].[FocusArea] FA ON (FA.FocusAreaID=SFA.FocusAreaID)  
  where StudentID=@StudentId
	
END





GO
/****** Object:  StoredProcedure [dbo].[spGetSupervisor]    Script Date: 5/25/2016 5:18:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetSupervisor]	
	@CPPID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		CPPID as SupervisorID,
		FirstName,
		LastName,
		Title,
		Phone,
		EmailID
		
	FROM
		CommunityPartnersPeople
	WHERE
		CPPID = @CPPID
END



GO
/****** Object:  StoredProcedure [dbo].[spGetUnAssignedCourseSection]    Script Date: 5/25/2016 5:18:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






CREATE PROCEDURE [dbo].[spGetUnAssignedCourseSection]	
	@OpportunityID int	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT		
		s.SectionID,
		c.CourseID,
		c.CourseName,
		c.ShortName	AS CourseShortName,
		s.RoomNumber,
		s.ClassHours,
		s.NumberOfSlots,
		q.QuarterName,
		q.ShortName AS QuarterShortName,
		s.SectionName
	FROM		
		Section s	
	INNER JOIN
		Class c
	ON
		s.CourseID = c.CourseID	
	INNER JOIN
		Quarter q
	ON
		s.QuarterID = q.QuarterID
	WHERE
		s.SectionID NOT IN 
		(SELECT SectionID FROM Opportunity_Section WHERE OpportunityID = @OpportunityID)
END






GO
/****** Object:  StoredProcedure [dbo].[spOpportunity_FocusArea]    Script Date: 5/25/2016 5:18:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 Create Procedure [dbo].[spOpportunity_FocusArea]
		 (
		 @OpportunityID int,
		 @FocusAreaID int
		 )
		 As
		 Insert into [Opportunity_FocusArea]
		 Values ( @OpportunityID,@FocusAreaID)
		 


GO
/****** Object:  StoredProcedure [dbo].[spOpportunity_Section]    Script Date: 5/25/2016 5:18:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[spOpportunity_Section]
		
		 (
		 @OpportunityID int,
		 @SectionID int
		 )
		 As
		 Insert into [Opportunity_Section]
		 Values ( @OpportunityID,@SectionID)


GO
/****** Object:  StoredProcedure [dbo].[spOpportunityByDistance]    Script Date: 5/25/2016 5:18:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[spOpportunityByDistance]
@DistanceFromSU varchar (8)
as
Begin
  Select OpportunityID,Name,Location,DistanceFromSU from Opportunity where DistanceFromSU = @DistanceFromSU
  End



GO
/****** Object:  StoredProcedure [dbo].[spOpportunityByQuarter]    Script Date: 5/25/2016 5:18:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create Procedure  [dbo].[spOpportunityByQuarter]
  @QuarterID int
  As
  Begin
  Select OpportunityId,Name, QuarterID from Opportunity
  where 
  QuarterID = @QuarterID
  End



GO
/****** Object:  StoredProcedure [dbo].[spOpportunityByType]    Script Date: 5/25/2016 5:18:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 Create procedure [dbo].[spOpportunityByType]
  @OpportunityType varchar (20)
  as
  Begin
       Select OpportunityId, Name,TypeID from Oppotunity where TypeID = @OpportunityType
	   End


GO
/****** Object:  StoredProcedure [dbo].[spRegisterStudentOpportunity]    Script Date: 5/25/2016 5:18:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE Procedure [dbo].[spRegisterStudentOpportunity]
	@StudentId int, @OpportunityId int
AS
Begin
  SET NOCOUNT ON;

    -- Insert statements for procedure here
DECLARE @CPPID int
SELECT @CPPID = (SELECT CPPID FROM [dbo].[Opportunity] WHERE OpportunityId=@OpportunityId)

BEGIN
	IF NOT EXISTS (SELECT [StudentID]
						  ,[CPPID]
						  ,[OpportunityID]
					  FROM [dbo].[SignUpFor]
					  WHERE [StudentID]=@StudentId
						AND [CPPID]=@CPPID
						AND [OpportunityID]=@OpportunityId
					)
	BEGIN
	INSERT INTO [dbo].[SignUpFor] ([StudentID]
								  ,[CPPID]
								  ,[OpportunityID]
								  ,[SignUpStatus]
								  )
	VALUES (@StudentId, @CPPID, @OpportunityId, 'Pending')
	END
END
		

End






GO
/****** Object:  StoredProcedure [dbo].[spRegisterStudentOpportunityTEST]    Script Date: 5/25/2016 5:18:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[spRegisterStudentOpportunityTEST]
	@StudentId int, @OpportunityId int
AS
Begin
	SET NOCOUNT ON;
	DECLARE @CPPID NVARCHAR(30)  -- ID of the community partner person assigned
	-- Retrieve the ID of the community partner person assigned to this opportunity
	SELECT @CPPID =  CPPID 
			FROM [dbo].[Opportunity] 
			WHERE OpportunityId=@OpportunityId

	BEGIN
	-- First make sure the student is not already signed up
	-- for this opportunity
		IF EXISTS (SELECT *
			FROM [dbo].[SignUpFor]
			WHERE [StudentID]=@StudentId
				AND [CPPID]=@CPPID
				AND [OpportunityID]=@OpportunityId)
		-- if this comes back true, the student is already enrolled
		RETURN 1  -- CALLER should check return value - if 1 student already signed up
		else -- student is not already assigned
		begin
		-- first make sure there is still an open slot
		-- and lock the row to make sure no one else gets it
			begin transaction
			declare	@slots int
			-- this statement sees if there are any slots available 
			--AND locks the opportunity row so no one else can change it
			select @slots = totalnumberofslots 
				from [dbo].[Opportunity]  WITH (updlock)
				where opportunityid = @opportunityid;
			if (@slots = 0 ) -- there are no slots left, rollback with error message
				begin
					rollback
					return 2 -- 2 indicates NOT ADDED - no slots left open
				end
			-- ELSE
			-- there are slots so go ahead and do the insert
			-- this will fire the trigger on OPPORTUNITY to decrement slots
			INSERT INTO [dbo].[SignUpFor] ([StudentID]
								  ,[CPPID]
								  ,[OpportunityID]
								  ,[SignUpStatus])
				VALUES (@StudentId, @CPPID, @OpportunityId, 'Pending')
			commit  -- commit the transaction and release the locks
		end
	END
		return 0 -- 0 indicates success
End



GO
/****** Object:  StoredProcedure [dbo].[spRemoveCourseSectionFromOpportunity]    Script Date: 5/25/2016 5:18:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spRemoveCourseSectionFromOpportunity]	
	@OpportunityID int,
	@CourseSectionIDs varchar(1000)	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM
		Opportunity_Section
	WHERE 
		OpportunityID = @OpportunityID
	AND
		SectionID in
		(SELECT splitData FROM dbo.fnSplitString(@CourseSectionIDs,';'))

END




GO
/****** Object:  StoredProcedure [dbo].[spSignUpFor]    Script Date: 5/25/2016 5:18:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 Create Procedure [dbo].[spSignUpFor]
		 (
		 @StudentID int,
		 @OpportunityID int
		 )
		 As
		 Begin
		 Select @StudentID,@OpportunityID,CPPID,SignUpStatus,PartnerEvaluation,StudentReflection,StudentEvaluation
		 from SignUpFor
		 where StudentID = @StudentID
		 and OpportunityID = @OpportunityID
		 End



GO
/****** Object:  StoredProcedure [dbo].[spStudent_Ethinicity]    Script Date: 5/25/2016 5:18:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 Create Procedure [dbo].[spStudent_Ethinicity]
		 (
		 @EthinicityID int,
		 @StudentID int
		 )
		 As
		 Insert into [Student_Ethinicity]
		 Select @EthinicityID,@StudentID


GO
/****** Object:  StoredProcedure [dbo].[spStudent_FocusArea]    Script Date: 5/25/2016 5:18:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 Create Procedure [dbo].[spStudent_FocusArea]
		 (
		 @FocusAreaID int,
		 @StudentID int
		 )
		 As
		 Insert into [Student_FocusArea]
		 Select @FocusAreaID,@StudentID


GO
/****** Object:  StoredProcedure [dbo].[spStudent_Section]    Script Date: 5/25/2016 5:18:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 Create Procedure [dbo].[spStudent_Section]
		 (
		 @StudentID int,
		 @SectionID int
		 )
		 As
		 Insert into [Student_Section]
		 Select @StudentID,@SectionID


GO
/****** Object:  StoredProcedure [dbo].[spUpdateCommunityPartnerInfo]    Script Date: 5/25/2016 5:18:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[spUpdateCommunityPartnerInfo]
 
@CPID int,
@OrganizationName nvarchar (150),
@Address nvarchar (500),
@Website nvarchar(500),
@MainPhone nvarchar (10),
@MissionStatement nvarchar (50),
@WorkDescription nvarchar (500)
As
Begin
Update communitypartners Set OrganizationName = @OrganizationName,
Address = @Address,
Website = @Website,
MainPhone = @MainPhone,
MissionStatement = @MissionStatement,
WorkDescription = @WorkDescription
where @CPID = CPID
END



GO
/****** Object:  StoredProcedure [dbo].[spUpdateCommunityPartnersPeople]    Script Date: 5/25/2016 5:18:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[spUpdateCommunityPartnersPeople]
@CPPID int,
@FirstName nvarchar (15),
@LastName nvarchar (15),
@Title nvarchar (15),
@Phone nvarchar (15),
@EmailID nvarchar (50)
As
Begin
Update CommunityPartnersPeople Set FirstName = @FirstName,
LastName = @LastName,
Title = @Title,
Phone = @Phone,
EmailID = @EmailID
where 
CPPID = @CPPID
End



GO
/****** Object:  StoredProcedure [dbo].[spUpdateOpportunity]    Script Date: 5/25/2016 5:18:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spUpdateOpportunity]	
	@OpportunityId int
   ,@Name varchar(50)
   ,@Location varchar(50)
   ,@JobDescription varchar(max)	
   ,@Requirements varchar(1000)
   ,@TimeCommittment varchar(9)
   ,@TotalNumberOfSlots int
   ,@OrientationDate datetime
   ,@ResumeRequired varchar(3)   
   ,@MinimumAge varchar(8)
   ,@CRCRequiredByPartner varchar(3)
   ,@CRCRequiredBySU varchar(3)
   ,@LinkToOnlineApp varchar(200)
   ,@TypeID int
   ,@CPID int
   ,@CPPID int
   ,@QuarterID int
   ,@JobHours varchar(20)
   ,@DistanceFromSU varchar(8)   
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Update Opportunity
	Set Name = @Name
		, Location = @Location
		, JobDescription = @JobDescription
		, Requirements = @Requirements
		, TimeCommittment = @TimeCommittment
		, TotalNumberOfSlots = @TotalNumberOfSlots
		, OrientatioNDate = @OrientatioNDate
		, ResumeRequired = @ResumeRequired		
		, MinimumAge = @MinimumAge
		, CRCRequiredByPartner = @CRCRequiredByPartner
		, CRCRequiredBySU = @CRCRequiredBySU
		, LinkToOnlineApp = @LinkToOnlineApp
		, TypeID = @TypeID
		, CPID = @CPID
		, CPPID = @CPPID
		, QuarterID = @QuarterID
		, JobHours = @JobHours
		, DistanceFromSU = @DistanceFromSU
	WHERE 
		OpportunityId = @OpportunityId	
END







GO
/****** Object:  StoredProcedure [dbo].[spUpdateSectionInfo]    Script Date: 5/25/2016 5:18:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[spUpdateSectionInfo]
   @SectionID int,
   @NumberOfSlots int
   as
   Begin
   Update Section Set NumberOfSlots = @NumberOfSlots
   where SectionID = @SectionID

   End



GO
/****** Object:  StoredProcedure [dbo].[spUpdateSignUpFor]    Script Date: 5/25/2016 5:18:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[spUpdateSignUpFor]
   @StudentID int,
   @OpportunityID int,
   @CPPID int,
   @SignUpStatus nvarchar (8),
   @StudentReflection nvarchar (max),
   @PartnerEvaluation nvarchar (max),
   @StudentEvaluation nvarchar (max)
   as
   Begin
   Update SignUpFor Set SignUpStatus = @SignUpStatus,
     StudentReflection = @StudentReflection,
	 PartnerEvaluation = @PartnerEvaluation,
	 StudentEvaluation = @StudentEvaluation
   where StudentID = @StudentID 
   and 
         OpportunityID = @OpportunityID
  and 
         CPPID = @CPPID

   End



GO
EXEC sys.sp_addextendedproperty @name=N'microsoft_database_tools_support', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'PROCEDURE',@level1name=N'sp_alterdiagram'
GO
EXEC sys.sp_addextendedproperty @name=N'microsoft_database_tools_support', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'PROCEDURE',@level1name=N'sp_creatediagram'
GO
EXEC sys.sp_addextendedproperty @name=N'microsoft_database_tools_support', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'PROCEDURE',@level1name=N'sp_dropdiagram'
GO
EXEC sys.sp_addextendedproperty @name=N'microsoft_database_tools_support', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'PROCEDURE',@level1name=N'sp_helpdiagramdefinition'
GO
EXEC sys.sp_addextendedproperty @name=N'microsoft_database_tools_support', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'PROCEDURE',@level1name=N'sp_helpdiagrams'
GO
EXEC sys.sp_addextendedproperty @name=N'microsoft_database_tools_support', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'PROCEDURE',@level1name=N'sp_renamediagram'
GO
EXEC sys.sp_addextendedproperty @name=N'microsoft_database_tools_support', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'PROCEDURE',@level1name=N'sp_upgraddiagrams'
GO
