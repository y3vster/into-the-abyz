Html Pages
==========
## Admin
 - List of new profiles
   - Display summary in table format
   - Modal window for displaying info
   - Approve / Reject buttons in modal window
 - Navigation Menu:
   - Show a badge icon with the number of needed profile actions
```css
.badge-white {
    background-color: #ffffff;
}
```
   - Hide the approval menu if there are no profile actions

## Partner Menu
- Use an exclamation point on the *profile* link if an action is needed
```Html
<a href="/Partner/Opportunity">My Opportunities <span class="glyphicon glyphicon-exclamation-sign text-danger" aria-hidden="true"></span></a>
```

- Hide the Opportunities menu:
  - if the profile is NOT created
  - If Admin has not approved the profile
  - If Admin rejected the profile

Admin
=====

	### No pending approvals
 - Do not display the approval page link

	### approvals pending
 - Display the link to the approval page
 - Display badge for number of pending approvals


New Database Table / Entity Class
=================================

- New class: PendingPartnerProfiles
  - AspNetUser_ID : string (community partner's unique ID)

Controller Logic
================

#PARTNER

### Partner does not have a profile
- (done) Redirect to the profile page
 - Display message: please fille out a profile before creating an opportunity

### Partner creates a new profile 
- Add AspNetUser_ID to the table `PendingPartnerProfiles`

## Partner is pending approval

(nice to have)
--------------
When a new pending profile is added, an alert is displayed for admin

Partner alerts
--------------

```html
<div class="alert alert-danger alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  Admin has rejected your profile.
</div>  
  
<div class="alert alert-warning alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong></strong> Your organization profile is being reviewed by admin, please check back later.
</div>  
```

